package nl.munlock.irods;

import io.kubernetes.client.openapi.ApiException;
import nl.munlock.Generic;
import nl.munlock.kubernetes.Kubernetes;
import nl.munlock.ontology.domain.DNASeqAssay;
import nl.munlock.ontology.domain.RNASeqAssay;
import nl.munlock.options.irods.CommandOptionsIRODS;
import nl.munlock.options.kubernetes.CommandOptionsKubernetes;
import nl.munlock.options.workflow.CommandOptionsHDT;
import nl.munlock.options.workflow.CommandOptionsYAML;
import nl.munlock.yaml.Yaml;
import nl.wur.ssb.RDFSimpleCon.ResultLine;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.commons.io.IOUtils;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.log4j.Logger;
import org.irods.jargon.core.exception.JargonException;
import org.irods.jargon.core.pub.DataObjectAO;
import org.irods.jargon.core.pub.IRODSGenQueryExecutor;
import org.irods.jargon.core.pub.domain.AvuData;
import org.irods.jargon.core.pub.io.IRODSFile;
import org.irods.jargon.core.pub.io.IRODSFileInputStream;
import org.irods.jargon.core.query.*;
import org.jermontology.ontology.JERMOntology.domain.Assay;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

public class Search {
    private static final Logger log = Generic.getLogger(Search.class, Yaml.debug);
    private static HashSet<String> processed = new HashSet<>();
    // static Domain domain;

    /**
     * @param commandOptions command options object
     * @param connection     irods connection object
     * @return list of assays from an rdf file
     * @throws Exception general exceptions
     */
    public static ArrayList<Assay> getAssaysFromRDF(CommandOptionsIRODS commandOptions, Connection connection, Domain domain) throws Exception {
        log.info("Obtaining assay files");

        // Get unprocessed files
        IRODSGenQueryBuilder queryBuilder = new IRODSGenQueryBuilder(true, null);
        // Obtain all TTL files in /Unprocessed folder
        String folderQuery = makePath(commandOptions.project, commandOptions.investigation, commandOptions.study, commandOptions.observationUnit, commandOptions.sample, commandOptions.assay, connection);

        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_COLL_NAME, QueryConditionOperators.LIKE, folderQuery);
        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_META_COLL_ATTR_NAME, QueryConditionOperators.LIKE, "type");
        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_META_COLL_ATTR_VALUE, QueryConditionOperators.LIKE, "Assay");
//        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_COLL_NAME, QueryConditionOperators.NOT_LIKE, "%PROVENANCE%");
//        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_COLL_NAME, QueryConditionOperators.NOT_LIKE, "%/Step_%");
        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_DATA_NAME, QueryConditionOperators.LIKE, "%ttl");
        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_DATA_NAME, QueryConditionOperators.NOT_LIKE, ".%");

        queryBuilder.addSelectAsGenQueryValue(RodsGenQueryEnum.COL_COLL_NAME);
        queryBuilder.addSelectAsGenQueryValue(RodsGenQueryEnum.COL_DATA_NAME);

        // Set limit?
        IRODSGenQueryFromBuilder query = queryBuilder.exportIRODSQueryFromBuilder(100000);
        IRODSGenQueryExecutor irodsGenQueryExecutor = connection.accessObjectFactory.getIRODSGenQueryExecutor(connection.irodsAccount);
        IRODSQueryResultSet irodsQueryResultSet = irodsGenQueryExecutor.executeIRODSQuery(query, 0);
        List<IRODSQueryResultRow> irodsQueryResultSetResults = irodsQueryResultSet.getResults();

        log.info("Crawling: " + folderQuery);

        if (irodsQueryResultSetResults.size() == 0) {
            throw new JargonQueryException("No results found with " + folderQuery);
        } else {
            log.info("Found " + irodsQueryResultSetResults.size() + " things...");
        }

        ArrayList<String> paths = new ArrayList<>();
        for (IRODSQueryResultRow irodsQueryResultSetResult : irodsQueryResultSetResults) {
            String path = irodsQueryResultSetResult.getColumn(0) + "/" + irodsQueryResultSetResult.getColumn(1);
            if (path.toLowerCase().contains("provenance")) {
                // ignore all PROVENANCE objects
            } else {
                paths.add(path);
                File parentFile = new File("." + new File(path).getParent());
                // If file does not exists...
                if (!new File("." + path).exists()) {
                    IRODSFileInputStream irodsFileInputStream = connection.fileFactory.instanceIRODSFileInputStream(path);
                    // Create parent folders to preserve structure
                    parentFile.mkdirs();
                    FileOutputStream fileOutputStream = new FileOutputStream(new File("." + path));
                    IOUtils.copy(irodsFileInputStream, fileOutputStream);
                    irodsFileInputStream.close();
                    fileOutputStream.close();
                    log.info("Obtained ." + path);
                }
            }
        }

        // Load...
        log.info("Loading files");
        int filePathNumber = 0;
        for (String path : paths) {
            filePathNumber = filePathNumber + 1;
            if (filePathNumber % 100 == 0) {
                log.info("Parsed " + filePathNumber + " files " + domain.getRDFSimpleCon().getModel().size() + " triples");
            }

            if (!new File(path).isHidden()) {
//                Domain domainTemp = new Domain("file://" + "." + path);
                RDFDataMgr.read(domain.getRDFSimpleCon().getModel(), "." + path);
//                domain.getRDFSimpleCon().getModel().add(domainTemp.getRDFSimpleCon().getModel());
//                domainTemp.close();
            }
        }

        Iterable<ResultLine> resultLines = domain.getRDFSimpleCon().runQuery("getSubjectsAndTypes.txt", true);
        ArrayList<Assay> assays = new ArrayList<>();
        resultLines.forEach(resultLine -> {
            String assayIRI = resultLine.getIRI("assay");
            String assayType = resultLine.getIRI("type").replaceAll(".*/", "");

            switch (assayType) {
                case "Amplicon":
                    assays.add(domain.make(Assay.class, assayIRI));
                    break;
                case "RNASeqAssay":
                    assays.add(domain.make(RNASeqAssay.class, assayIRI));
                    break;
                case "DNASeqAssay":
                    assays.add(domain.make(DNASeqAssay.class, assayIRI));
                    break;
                case "Illumina":
                    assays.add(domain.make(Assay.class, assayIRI));
                case "PacBio":
                    assays.add(domain.make(Assay.class, assayIRI));
                case "Nanopore":
                    assays.add(domain.make(Assay.class, assayIRI));
                case "JERMOntology#Sample":
                case "PairedSequenceDataSet":
                case "AmpliconLibraryAssay":
                case "SingleSequenceDataSet":
                    break;
                default:
                    log.info("Unknown assay type " + assayType);
            }
        });
        return assays;
    }

    public static String makePath(String project, String investigation, String study, String observationUnit, String sample, String assay, Connection connection) {
        String folderQuery = "/" + connection.irodsAccount.getZone() + "/projects";
        if (project != null) {
            folderQuery += "/" + project;
        }
        if (investigation != null) {
            folderQuery += "/" + investigation;
        } else {
            folderQuery += "/%";
        }
        if (study != null) {
            folderQuery += "/" + study;
        } else {
            folderQuery += "/%";
        }
        if (observationUnit != null) {
            folderQuery += "/" + observationUnit;
        } else {
            folderQuery += "/%";
        }
        if (sample != null) {
            folderQuery += "/" + sample;
        } else {
            folderQuery += "/%";
        }
        if (assay != null) {
            folderQuery += "/" + assay;
        } else {
            folderQuery += "/%";
        }
        // Replace multiple %%% by one %
        // folderQuery += "/%";
        folderQuery = folderQuery.replaceAll("(/%)+", "/%").replaceAll("%+", "%");
        folderQuery = folderQuery.replaceAll("/%$", "%");
        return folderQuery;
    }

    public static void resetFailures(Connection connection, String search) throws GenQueryBuilderException, JargonException, JargonQueryException {
        log.debug("Resetting jobs found in " + search);
        IRODSGenQueryBuilder queryBuilder = new IRODSGenQueryBuilder(true, null);

        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_COLL_NAME, QueryConditionOperators.LIKE, search);
        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_DATA_NAME, QueryConditionOperators.LIKE, "%.yaml");
        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_META_DATA_ATTR_NAME, QueryConditionOperators.LIKE, "cwl");
        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_META_DATA_ATTR_UNITS, QueryConditionOperators.LIKE, "failed");

        queryBuilder.addSelectAsGenQueryValue(RodsGenQueryEnum.COL_COLL_NAME);
        queryBuilder.addSelectAsGenQueryValue(RodsGenQueryEnum.COL_DATA_NAME);

        // Set limit?
        IRODSGenQueryFromBuilder query = queryBuilder.exportIRODSQueryFromBuilder(999999);

        IRODSGenQueryExecutor irodsGenQueryExecutor = connection.accessObjectFactory.getIRODSGenQueryExecutor(connection.irodsAccount);
        IRODSQueryResultSet irodsQueryResultSet = irodsGenQueryExecutor.executeIRODSQuery(query, 0);

        List<IRODSQueryResultRow> irodsQueryResultSetResults = irodsQueryResultSet.getResults();

        int index = 0;
        for (IRODSQueryResultRow irodsQueryResultSetResult : irodsQueryResultSetResults) {
            index = index + 1;
            // Yaml file... obtain, parse, check destination... if already exists don't do it?
            String yaml = irodsQueryResultSetResult.getColumn(0) + "/" + irodsQueryResultSetResult.getColumn(1);
            failureAVU(connection, yaml);
        }
    }

    public static HashSet<String> getAllFiles(Connection connection, String path, String file) throws GenQueryBuilderException, JargonException, JargonQueryException {
        log.debug("Searching in... " + path + " for " + file);

        IRODSGenQueryBuilder queryBuilder = new IRODSGenQueryBuilder(true, null);

        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_COLL_NAME, QueryConditionOperators.LIKE, path);
        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_DATA_NAME, QueryConditionOperators.LIKE, file);

        queryBuilder.addSelectAsGenQueryValue(RodsGenQueryEnum.COL_COLL_NAME);
        queryBuilder.addSelectAsGenQueryValue(RodsGenQueryEnum.COL_DATA_NAME);
        if (file.endsWith("hdt.gz"))
            queryBuilder.addSelectAsGenQueryValue(RodsGenQueryEnum.COL_D_CREATE_TIME);

        // Set limit?
        IRODSGenQueryFromBuilder query = queryBuilder.exportIRODSQueryFromBuilder(999999);

        IRODSGenQueryExecutor irodsGenQueryExecutor = connection.accessObjectFactory.getIRODSGenQueryExecutor(connection.irodsAccount);
        IRODSQueryResultSet irodsQueryResultSet = irodsGenQueryExecutor.executeIRODSQuery(query, 0);

        List<IRODSQueryResultRow> irodsQueryResultSetResults = irodsQueryResultSet.getResults();

        HashSet<String> files = new HashSet<>();
        for (IRODSQueryResultRow irodsQueryResultSetResult : irodsQueryResultSetResults) {
            // Yaml file... obtain, parse, check destination... if already exists don't do it?
            if (irodsQueryResultSetResult.getColumnsAsList().size() == 3) {
                LocalDateTime irodsDateTime = irodsQueryResultSetResult.getColumnAsDateOrNull(2).toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
                LocalDateTime localDate = LocalDateTime.of(2022, 3, 2, 1, 1, 1);
                String filePath = irodsQueryResultSetResult.getColumn(0) + "/" + irodsQueryResultSetResult.getColumn(1);
                if (irodsDateTime.isAfter(localDate)) {
                    files.add(filePath);
                } else {
                    // Delete file to ensure safe upload after renewed annotation
                    connection.fileFactory.instanceIRODSFile(filePath).delete();
                }
            } else {
                files.add(irodsQueryResultSetResult.getColumn(0) + "/" + irodsQueryResultSetResult.getColumn(1));
            }
        }
        return files;
    }

    public static void getAllUnprocessedReferences(CommandOptionsKubernetes commandOptionsKubernetes, Connection connection, String search) throws IOException, JargonException, JargonQueryException, GenQueryBuilderException, InterruptedException, ApiException {
        // Obtain all EMBL files already obtained
        HashSet<String> embls = getAllProcessedReferences(commandOptionsKubernetes, connection, search);

        // Perform yaml search
        log.debug("Searching in... " + search + " for " + commandOptionsKubernetes.yaml);

        IRODSGenQueryBuilder queryBuilder = new IRODSGenQueryBuilder(true, null);
        // Obtains all YAML files without the CWL ATTR_NAME
        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_COLL_NAME, QueryConditionOperators.LIKE, search);
        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_DATA_NAME, QueryConditionOperators.LIKE, commandOptionsKubernetes.yaml);

        queryBuilder.addSelectAsGenQueryValue(RodsGenQueryEnum.COL_COLL_NAME);
        queryBuilder.addSelectAsGenQueryValue(RodsGenQueryEnum.COL_DATA_NAME);

        // Set limit
        IRODSGenQueryFromBuilder query = queryBuilder.exportIRODSQueryFromBuilder(999999);

        IRODSGenQueryExecutor irodsGenQueryExecutor = connection.accessObjectFactory.getIRODSGenQueryExecutor(connection.irodsAccount);
        IRODSQueryResultSet irodsQueryResultSet = irodsGenQueryExecutor.executeIRODSQuery(query, 0);

        List<IRODSQueryResultRow> irodsQueryResultSetResults = irodsQueryResultSet.getResults();
        log.info("Detected " + irodsQueryResultSetResults.size() + " yaml files and " + embls.size() + " embl files for " + search);
        int index = 0;
        for (IRODSQueryResultRow irodsQueryResultSetResult : irodsQueryResultSetResults) {
            // Yaml file... obtain, parse, check destination... if already exists don't do it?
            String yaml = irodsQueryResultSetResult.getColumn(0) + "/" + irodsQueryResultSetResult.getColumn(1);
            String yamlName = irodsQueryResultSetResult.getColumn(1);
            // Skips already processed yaml files from previous runs
            if (processed.contains(yaml)) {
                System.err.println("SKIPPING " + yaml);
                continue;
            }

            index = index + 1;
            String embl = yamlName.replaceAll(".yaml", ".embl.gz");

            if (embls.contains(embl)) {
                continue;
            }

            if (index % 10 == 0) {
                log.info("Checking yaml " + index + " of " + irodsQueryResultSetResults.size() + " with path " + yaml);
            }

            Generic.downloadFile(connection, yaml);
            Kubernetes.yamls.add(yaml);
            Kubernetes.createJobs(commandOptionsKubernetes, connection);
            Kubernetes.yamls = new HashSet<>();

            // Temp download the ENA genome myself
//            PrintWriter printWriter = new PrintWriter("command.sh");
//            String gcaID = yamlName.replaceAll(".yaml","");
//            printWriter.println("wget https://www.ebi.ac.uk/ena/browser/api/embl/"+gcaID+"?download=true&gzip=true");
//            printWriter.close();
//            System.err.println(">>> " + gcaID);
//            ExecCommand execCommand = new ExecCommand("wget -nc -O "+gcaID+".embl.gz https://www.ebi.ac.uk/ena/browser/api/embl/"+gcaID+"?download=true&gzip=true");
//            System.err.println(execCommand.getOutput() + " " + execCommand.getError() + " " + execCommand.getExit());
//
//            DataTransferOperations dataTransferOperationsAO = connection.irodsFileSystem.getIRODSAccessObjectFactory().getDataTransferOperations(connection.irodsAccount);
//            IRODSFile destFile = connection.fileFactory.instanceIRODSFile(yaml.replaceAll(".yaml", ".embl.gz"));
//            System.err.println(destFile);
//            dataTransferOperationsAO.putOperation(new File(gcaID + ".embl.gz"), destFile, null, null);
        }
    }

    public static HashSet<String> getAllProcessedReferences(CommandOptionsKubernetes commandOptionsKubernetes, Connection connection, String search) throws JargonException, JargonQueryException, GenQueryBuilderException {
        log.debug("Searching in... " + search + " for GCA_%embl.gz");
        HashSet<String> embls = new HashSet<>();
        IRODSGenQueryBuilder queryBuilder = new IRODSGenQueryBuilder(true, null);
        // Obtains all YAML files without the CWL ATTR_NAME
        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_COLL_NAME, QueryConditionOperators.LIKE, search);
        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_DATA_NAME, QueryConditionOperators.LIKE, "%embl.gz");

        queryBuilder.addSelectAsGenQueryValue(RodsGenQueryEnum.COL_DATA_NAME);

        // Set limit
        IRODSGenQueryFromBuilder query = queryBuilder.exportIRODSQueryFromBuilder(999999);

        IRODSGenQueryExecutor irodsGenQueryExecutor = connection.accessObjectFactory.getIRODSGenQueryExecutor(connection.irodsAccount);
        IRODSQueryResultSet irodsQueryResultSet = irodsGenQueryExecutor.executeIRODSQuery(query, 0);

        List<IRODSQueryResultRow> irodsQueryResultSetResults = irodsQueryResultSet.getResults();
        // log.info("Detected " + irodsQueryResultSetResults.size() + " embl files");
        for (IRODSQueryResultRow irodsQueryResultSetResult : irodsQueryResultSetResults) {
            // Yaml file... obtain, parse, check destination... if already exists don't do it?
            embls.add(irodsQueryResultSetResult.getColumn(0));
        }
        return embls;
    }

    public static HashSet<String> getAllFailedUnprocessed(CommandOptionsKubernetes commandOptionsKubernetes, Connection connection) throws GenQueryBuilderException, JargonException, JargonQueryException, IOException, InterruptedException, ApiException {

        // Creating search pattern based on PISOSA
        String search = makePath(commandOptionsKubernetes.project, commandOptionsKubernetes.investigation, commandOptionsKubernetes.study, commandOptionsKubernetes.observationUnit, commandOptionsKubernetes.sample, commandOptionsKubernetes.assay, connection);

        IRODSGenQueryBuilder queryBuilder = new IRODSGenQueryBuilder(true, null);
        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_COLL_NAME, QueryConditionOperators.LIKE, search);
        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_DATA_NAME, QueryConditionOperators.LIKE, commandOptionsKubernetes.yaml);
        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_META_DATA_ATTR_NAME, QueryConditionOperators.LIKE, "cwl");
        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_META_DATA_ATTR_UNITS, QueryConditionOperators.LIKE, "failed");

        queryBuilder.addSelectAsGenQueryValue(RodsGenQueryEnum.COL_COLL_NAME);
        queryBuilder.addSelectAsGenQueryValue(RodsGenQueryEnum.COL_DATA_NAME);

        // Set limit?
        IRODSGenQueryFromBuilder query = queryBuilder.exportIRODSQueryFromBuilder(50000);

        IRODSGenQueryExecutor irodsGenQueryExecutor = connection.accessObjectFactory.getIRODSGenQueryExecutor(connection.irodsAccount);
        IRODSQueryResultSet irodsQueryResultSet = irodsGenQueryExecutor.executeIRODSQuery(query, 0);

        List<IRODSQueryResultRow> irodsQueryResultSetResults = irodsQueryResultSet.getResults();

        HashSet<String> yamlFiles = new HashSet<>();
        for (IRODSQueryResultRow irodsQueryResultSetResult : irodsQueryResultSetResults) {
            // Yaml file... obtain, parse, check destination... if already exists don't do it?
            String yaml = irodsQueryResultSetResult.getColumn(0) + "/" + irodsQueryResultSetResult.getColumn(1);
            yamlFiles.add(yaml);
        }
        return yamlFiles;
    }

    public static HashSet<String> getAllUnprocessedQuery(CommandOptionsKubernetes commandOptionsKubernetes, Connection connection, String search) throws GenQueryBuilderException, JargonException, JargonQueryException, IOException, InterruptedException, ApiException {
        // Creating search pattern based on PISOSA
        if (search == null) {
            search = makePath(commandOptionsKubernetes.project, commandOptionsKubernetes.investigation, commandOptionsKubernetes.study, commandOptionsKubernetes.observationUnit, commandOptionsKubernetes.sample, commandOptionsKubernetes.assay, connection);
        }

        log.info("Searching for failed jobs in " + search);

        HashSet<String> yamlFiles = getAllFailedUnprocessed(commandOptionsKubernetes, connection);

        log.info("Searching for jobs in " + search + " with " + "%" + commandOptionsKubernetes.yaml);

        IRODSGenQueryBuilder queryBuilder = new IRODSGenQueryBuilder(true, null);
        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_COLL_NAME, QueryConditionOperators.LIKE, search);
        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_DATA_NAME, QueryConditionOperators.LIKE, "%" + commandOptionsKubernetes.yaml);
        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_META_DATA_ATTR_NAME, QueryConditionOperators.LIKE, "cwl");
        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_META_DATA_ATTR_UNITS, QueryConditionOperators.LIKE, "waiting");

        queryBuilder.addSelectAsGenQueryValue(RodsGenQueryEnum.COL_COLL_NAME);
        queryBuilder.addSelectAsGenQueryValue(RodsGenQueryEnum.COL_DATA_NAME);

        // Set limit?
        IRODSGenQueryFromBuilder query = queryBuilder.exportIRODSQueryFromBuilder(50000);

        IRODSGenQueryExecutor irodsGenQueryExecutor = connection.accessObjectFactory.getIRODSGenQueryExecutor(connection.irodsAccount);
        IRODSQueryResultSet irodsQueryResultSet = irodsGenQueryExecutor.executeIRODSQuery(query, 0);

        List<IRODSQueryResultRow> irodsQueryResultSetResults = irodsQueryResultSet.getResults();

        for (IRODSQueryResultRow irodsQueryResultSetResult : irodsQueryResultSetResults) {
            // Yaml file... obtain, parse, check destination... if already exists don't do it?
            String yaml = irodsQueryResultSetResult.getColumn(0) + "/" + irodsQueryResultSetResult.getColumn(1);
            yamlFiles.add(yaml);
        }

        log.info(yamlFiles.size() + " yaml files detected");
        return yamlFiles;
    }

    public static void getAllUnprocessed(CommandOptionsKubernetes commandOptionsKubernetes, Connection connection) throws GenQueryBuilderException, JargonException, JargonQueryException, IOException, InterruptedException, ApiException {
        getAllUnprocessed(commandOptionsKubernetes, connection, null);
    }

    public static void getAllUnprocessed(CommandOptionsKubernetes commandOptionsKubernetes, Connection connection, String search) throws GenQueryBuilderException, JargonException, JargonQueryException, IOException, InterruptedException, ApiException {
        HashSet<String> yamlFiles;
        if (search == null)
            yamlFiles = getAllUnprocessedQuery(commandOptionsKubernetes, connection);
        else
            yamlFiles = getAllUnprocessedQuery(commandOptionsKubernetes, connection, search);

        // get All processed folders not sure if needed now metadata is being updated
        Set<String> processed = getAllProcessed(commandOptionsKubernetes, connection);

        int count = 0;

        for (String yaml : yamlFiles) {
            count = count + 1;
            log.info("Processing " + count + " " + yaml);
            Generic.downloadFile(connection, yaml);
            Scanner scanner = new Scanner(new File("." + yaml));
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                if (line.startsWith("destination: ")) {
                    String destination = line.split(" ")[1];
                    if (processed.contains(destination)) {
                        log.debug("Skipping analysis of " + new File(yaml).getName() + " as destination folder already exists");
                        fixAVU(connection, yaml);
                    } else {
                        // When not in processed, will do a final check again to see if it exists if not assign to kube
                        if (!connection.fileFactory.instanceIRODSFile(destination).exists()) {
                            log.debug("Processing " + line);
                            Kubernetes.yamls.add(yaml);
                            log.debug("Starting kubernetes");
                            Kubernetes.createJobs(commandOptionsKubernetes, connection);
                            // Reset references...
                            Kubernetes.yamls = new HashSet<>();
                        } else {
                            fixAVU(connection, yaml);
                        }
                        break;
                    }
                }
            }
            scanner.close();
        }
    }

    private static void fixAVU(Connection connection, String yaml) throws JargonException {
        log.debug("Skipping analysis of " + new File(yaml).getName() + " as destination folder already exists");
        DataObjectAO dataObjectAO = connection.irodsFileSystem.getIRODSAccessObjectFactory().getDataObjectAO(connection.irodsAccount);
        List<MetaDataAndDomainData> metaDataAndDomainDataList = dataObjectAO.findMetadataValuesForDataObject(yaml);
        AvuData avu = null;
        for (MetaDataAndDomainData metaDataAndDomainData : metaDataAndDomainDataList) {
            avu = metaDataAndDomainData.asAvu();
            if (metaDataAndDomainData.getAvuAttribute().contains("cwl") &&
                    metaDataAndDomainData.getAvuUnit().contains("waiting")) {
                break;
            }
        }
        if (avu != null) {
            log.debug("Updating metadata field to finished");
            dataObjectAO.deleteAVUMetadata(yaml, avu);
            avu.setUnit("finished");
            dataObjectAO.addAVUMetadata(yaml, avu);
        }
    }

    public static void queueAVU(Connection connection, String yaml) throws JargonException {
        log.debug("Setting AVU to queue");
        DataObjectAO dataObjectAO = connection.irodsFileSystem.getIRODSAccessObjectFactory().getDataObjectAO(connection.irodsAccount);
        List<MetaDataAndDomainData> metaDataAndDomainDataList = dataObjectAO.findMetadataValuesForDataObject(yaml);
        AvuData avu = null;
        for (MetaDataAndDomainData metaDataAndDomainData : metaDataAndDomainDataList) {
            avu = metaDataAndDomainData.asAvu();
            if (metaDataAndDomainData.getAvuAttribute().contains("cwl") && metaDataAndDomainData.getAvuUnit().contains("waiting")) {
                break;
            }
        }
        if (avu != null) {
            log.debug("Updating metadata field to queue");
            dataObjectAO.deleteAVUMetadata(yaml, avu);
            avu.setUnit("queue");
            dataObjectAO.addAVUMetadata(yaml, avu);
        }
    }

    public static void failureAVU(Connection connection, String yaml) throws JargonException {
        log.debug("Setting AVU to queue");
        DataObjectAO dataObjectAO = connection.irodsFileSystem.getIRODSAccessObjectFactory().getDataObjectAO(connection.irodsAccount);
        List<MetaDataAndDomainData> metaDataAndDomainDataList = dataObjectAO.findMetadataValuesForDataObject(yaml);
        AvuData avu = null;
        for (MetaDataAndDomainData metaDataAndDomainData : metaDataAndDomainDataList) {
            avu = metaDataAndDomainData.asAvu();
            if (metaDataAndDomainData.getAvuAttribute().contains("cwl") && metaDataAndDomainData.getAvuUnit().contains("failure")) {
                break;
            }
        }
        if (avu != null) {
            log.debug("Updating metadata field to waiting " + yaml);
            dataObjectAO.deleteAVUMetadata(yaml, avu);
            avu.setUnit("waiting");
            dataObjectAO.addAVUMetadata(yaml, avu);
        }
    }

    private static Set<String> getAllProcessed(CommandOptionsKubernetes commandOptionsKubernetes, Connection connection) throws GenQueryBuilderException, JargonException, JargonQueryException {
        // Obtains unprocessed folders...
        String search = "/" + commandOptionsKubernetes.zone + "/projects/" + commandOptionsKubernetes.project + "/%/processed/%";

        IRODSGenQueryBuilder queryBuilder = new IRODSGenQueryBuilder(true, null);

        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_COLL_NAME, QueryConditionOperators.LIKE, search);
//        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_DATA_NAME, QueryConditionOperators.LIKE, "%.yaml");
//        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_META_DATA_ATTR_NAME, QueryConditionOperators.LIKE, "cwl");
//        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_META_DATA_ATTR_UNITS, QueryConditionOperators.LIKE, "waiting");

        queryBuilder.addSelectAsGenQueryValue(RodsGenQueryEnum.COL_COLL_NAME);
//        queryBuilder.addSelectAsGenQueryValue(RodsGenQueryEnum.COL_DATA_NAME);

        // Set limit?
        IRODSGenQueryFromBuilder query = queryBuilder.exportIRODSQueryFromBuilder(50000);

        IRODSGenQueryExecutor irodsGenQueryExecutor = connection.accessObjectFactory.getIRODSGenQueryExecutor(connection.irodsAccount);
        IRODSQueryResultSet irodsQueryResultSet = irodsGenQueryExecutor.executeIRODSQuery(query, 0);

        List<IRODSQueryResultRow> irodsQueryResultSetResults = irodsQueryResultSet.getResults();

        Set<String> processed = new HashSet<>();
        for (IRODSQueryResultRow irodsQueryResultSetResult : irodsQueryResultSetResults) {
            String folder = irodsQueryResultSetResult.getColumn(0);
            processed.add(folder);
        }
        return processed;
    }

    public static Set<String> getAllHDT(String search, Connection connection) throws GenQueryBuilderException, JargonException, JargonQueryException {
        log.info("Obtain all HDTs");

        IRODSGenQueryBuilder queryBuilder = new IRODSGenQueryBuilder(true, null);

        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_COLL_NAME, QueryConditionOperators.LIKE, search + "%/hdt%");
        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_DATA_NAME, QueryConditionOperators.LIKE, "%.hdt");

        queryBuilder.addSelectAsGenQueryValue(RodsGenQueryEnum.COL_COLL_NAME);

        // Set limit?
        IRODSGenQueryFromBuilder query = queryBuilder.exportIRODSQueryFromBuilder(50000);

        IRODSGenQueryExecutor irodsGenQueryExecutor = connection.accessObjectFactory.getIRODSGenQueryExecutor(connection.irodsAccount);
        IRODSQueryResultSet irodsQueryResultSet = irodsGenQueryExecutor.executeIRODSQuery(query, 0);

        List<IRODSQueryResultRow> irodsQueryResultSetResults = irodsQueryResultSet.getResults();

        Set<String> processed = new HashSet<>();
        for (IRODSQueryResultRow irodsQueryResultSetResult : irodsQueryResultSetResults) {
            String folder = irodsQueryResultSetResult.getColumn(0);
            processed.add(folder);
        }
        return processed;
    }

    /**
     * Obtains the project RDF file
     *
     * @param commandOptions
     * @param connection
     * @return
     */
    public static ArrayList<Domain> getProjectFromRDF(CommandOptionsYAML commandOptions, Connection connection) throws Exception {
        // Using default project folder
        String folderQuery = "/" + connection.irodsAccount.getZone() + "/projects/" + commandOptions.project + "/" + commandOptions.investigation + "%";
        return getProjectFromRDF(connection, folderQuery);
    }

    public static ArrayList<Domain> getProjectFromRDF(Connection connection, String folderQuery) throws Exception {
        log.info("Obtaining project file from " + folderQuery);
        ArrayList<Domain> domains = new ArrayList<>();

        // Get unprocessed files
        IRODSGenQueryBuilder queryBuilder = new IRODSGenQueryBuilder(true, null);
        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_COLL_NAME, QueryConditionOperators.LIKE, folderQuery);
        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_DATA_NAME, QueryConditionOperators.LIKE, "%ttl");
        queryBuilder.addSelectAsGenQueryValue(RodsGenQueryEnum.COL_COLL_NAME);
        queryBuilder.addSelectAsGenQueryValue(RodsGenQueryEnum.COL_DATA_NAME);

        // Set limit?
        IRODSGenQueryFromBuilder query = queryBuilder.exportIRODSQueryFromBuilder(99999);
        IRODSGenQueryExecutor irodsGenQueryExecutor = connection.accessObjectFactory.getIRODSGenQueryExecutor(connection.irodsAccount);
        IRODSQueryResultSet irodsQueryResultSet = irodsGenQueryExecutor.executeIRODSQuery(query, 0);
        List<IRODSQueryResultRow> irodsQueryResultSetResults = irodsQueryResultSet.getResults();

        log.info("Crawling project: " + folderQuery);

        if (irodsQueryResultSetResults.size() == 0) {
            throw new JargonQueryException("No results found with " + folderQuery);
        } else {
            log.info("Found " + irodsQueryResultSetResults.size() + " things...");
        }

        for (IRODSQueryResultRow irodsQueryResultSetResult : irodsQueryResultSetResults) {
            String path = irodsQueryResultSetResult.getColumn(0) + "/" + irodsQueryResultSetResult.getColumn(1);
            if (new File(path).getName().startsWith(".") || path.endsWith("provenance.ttl")) {
                continue;
            } else if (!new File("." + path).exists()) {
                log.info("Obtaining " + new File(path).getName());
                IRODSFileInputStream irodsFileInputStream = connection.fileFactory.instanceIRODSFileInputStream(path);
                new File("." + new File(path).getParent()).mkdirs();
                FileOutputStream fileOutputStream = new FileOutputStream(new File("." + path));
                IOUtils.copy(irodsFileInputStream, fileOutputStream);
                irodsFileInputStream.close();
                fileOutputStream.close();
            }

            // Load...
            String config = "file://" + "." + path;
            Domain domain = new Domain(config);
            domains.add(domain);
        }
        return domains;
    }

    public static Set<String> getOUFromiRODS(Connection connection, CommandOptionsHDT commandOptionsHDT) throws GenQueryBuilderException, JargonException, JargonQueryException {
        // Obtains OU folders...
        String folderQuery = makePath(commandOptionsHDT.project, commandOptionsHDT.investigation, commandOptionsHDT.study, null, null, null, connection);

        log.info("Searching for OU in " + folderQuery);

        IRODSGenQueryBuilder queryBuilder = new IRODSGenQueryBuilder(true, null);

        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_COLL_NAME, QueryConditionOperators.LIKE, folderQuery);
        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_META_COLL_ATTR_NAME, QueryConditionOperators.EQUAL, "type");
        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_META_COLL_ATTR_VALUE, QueryConditionOperators.EQUAL, "ObservationUnit");
        queryBuilder.addSelectAsGenQueryValue(RodsGenQueryEnum.COL_COLL_NAME);

        // Set limit?
        IRODSGenQueryFromBuilder query = queryBuilder.exportIRODSQueryFromBuilder(500000);

        IRODSGenQueryExecutor irodsGenQueryExecutor = connection.accessObjectFactory.getIRODSGenQueryExecutor(connection.irodsAccount);
        IRODSQueryResultSet irodsQueryResultSet = irodsGenQueryExecutor.executeIRODSQuery(query, 0);

        List<IRODSQueryResultRow> irodsQueryResultSetResults = irodsQueryResultSet.getResults();

        if (irodsQueryResultSetResults.size() == 0)
            throw new JargonException("No results found");

        Set<String> folders = new HashSet<>();
        for (IRODSQueryResultRow irodsQueryResultSetResult : irodsQueryResultSetResults) {
            String path = irodsQueryResultSetResult.getColumn(0);
            if (commandOptionsHDT.observationUnit != null) {
                if (path.contains(commandOptionsHDT.observationUnit))
                    folders.add(path);
            } else {
                folders.add(path);
            }
        }
        log.info("Detected " + folders.size());
        return folders;
    }

    /**
     * Time stamp check for hdt file and turtle files
     *
     * @param connection        irods connection object
     * @param commandOptionsHDT command options object for hdt files
     * @param folder            folder containing the hdt files and turtle files
     * @return
     */
    public static boolean checkTimeStampsHDT(Connection connection, CommandOptionsHDT commandOptionsHDT, String folder) throws GenQueryBuilderException, JargonException, JargonQueryException {
        // Obtain time stamp of latest TURTLE file
        // Obtains OU folders...
        log.info("Searching for turtle timestamps in " + folder);

        // Get creation time
        IRODSGenQueryBuilder queryBuilder = new IRODSGenQueryBuilder(true, null);
        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_DATA_NAME, QueryConditionOperators.LIKE, "%.ttl");
        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_COLL_NAME, QueryConditionOperators.LIKE, folder + "%");

        queryBuilder.addSelectAsGenQueryValue(RodsGenQueryEnum.COL_D_MODIFY_TIME);
        queryBuilder.addOrderByGenQueryField(RodsGenQueryEnum.COL_D_MODIFY_TIME, GenQueryOrderByField.OrderByType.DESC);

        // Set limit?
        IRODSGenQueryFromBuilder query = queryBuilder.exportIRODSQueryFromBuilder(1);

        IRODSGenQueryExecutor irodsGenQueryExecutor = connection.accessObjectFactory.getIRODSGenQueryExecutor(connection.irodsAccount);
        IRODSQueryResultSet irodsQueryResultSet = irodsGenQueryExecutor.executeIRODSQuery(query, 0);

        List<IRODSQueryResultRow> irodsQueryResultSetResults = irodsQueryResultSet.getResults();

        if (irodsQueryResultSetResults.size() == 0) {
            // throw new JargonException("No results found");
            log.error("No results found");
        }

        Set<String> folders = new HashSet<>();

        if (irodsQueryResultSetResults.size() > 0) {
            Date dateTTL = irodsQueryResultSetResults.get(0).getColumnAsDateOrNull(0);

            // Get timestamp HDT file
            IRODSFile irodsFolder = connection.fileFactory.instanceIRODSFile(folder + "/hdt/");
            if (!irodsFolder.exists()) {
                return true;
            } else {
                boolean hdtFound = false;
                for (File file : irodsFolder.listFiles()) {
                    if (file.getName().endsWith(".hdt")) {
                        hdtFound = true;
                        IRODSFile irodsHDTFile = connection.fileFactory.instanceIRODSFile(file.getAbsolutePath());

                        Date dateHDT = new Date(irodsHDTFile.lastModified());

                        if (dateHDT.compareTo(dateTTL) == -1) {
                            log.info("TTL date: " + dateTTL + " HDT date: " + dateHDT);
                            // This is a job to be executed as TTL files that are newer has been identified
                            return true;
                        }
                    }
                }
                // There is no HDT file present so obviously should return true
                if (!hdtFound) {
                    return true;
                }
            }
        }
        return false;
    }

    public static long getFolderSize(Connection connection, CommandOptionsHDT commandOptionsHDT, String folder, String filter) throws GenQueryBuilderException, JargonException, JargonQueryException {
        IRODSGenQueryBuilder queryBuilder = new IRODSGenQueryBuilder(true, null);
        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_COLL_NAME, QueryConditionOperators.LIKE, folder + "%");
        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_DATA_NAME, QueryConditionOperators.LIKE, filter);

        queryBuilder.addSelectAsAgregateGenQueryValue(RodsGenQueryEnum.COL_DATA_SIZE, GenQueryField.SelectFieldTypes.SUM);

        IRODSGenQueryFromBuilder query = queryBuilder.exportIRODSQueryFromBuilder(30000);

        IRODSGenQueryExecutor irodsGenQueryExecutor = connection.accessObjectFactory.getIRODSGenQueryExecutor(connection.irodsAccount);
        IRODSQueryResultSet irodsQueryResultSet = irodsGenQueryExecutor.executeIRODSQuery(query, 0);

        List<IRODSQueryResultRow> irodsQueryResultSetResults = irodsQueryResultSet.getResults();

        if (irodsQueryResultSetResults.size() == 0)
            throw new JargonException("No results found");

        for (IRODSQueryResultRow irodsQueryResultSetResult : irodsQueryResultSetResults) {
            long size = Long.parseLong(irodsQueryResultSetResult.getColumn(0)) / 1024 / 1024 * 10;
            System.err.println("SIZE: " + size);
            return size;
        }
        return -1;
    }

    public static HashSet<String> getAllUnprocessedQuery(CommandOptionsKubernetes commandOptionsKubernetes, Connection connection) throws JargonQueryException, JargonException, IOException, InterruptedException, GenQueryBuilderException, ApiException {
        return getAllUnprocessedQuery(commandOptionsKubernetes, connection, null);
    }
}
