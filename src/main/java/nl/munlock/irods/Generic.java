package nl.munlock.irods;

import nl.munlock.yaml.Yaml;
import nl.wur.ssb.RDFSimpleCon.ExecCommand;
import org.apache.log4j.Logger;
import org.irods.jargon.core.exception.JargonException;
import org.irods.jargon.core.packinstr.TransferOptions;
import org.irods.jargon.core.pub.DataTransferOperations;
import org.irods.jargon.core.pub.io.IRODSFile;
import org.irods.jargon.core.transfer.DefaultTransferControlBlock;
import org.irods.jargon.core.transfer.TransferControlBlock;

import java.io.File;
import java.util.ArrayList;

public class Generic {
    private static final Logger log = nl.munlock.Generic.getLogger(Generic.class, Yaml.debug);

    public static ArrayList<String> prepareFilter(Connection connection, String filter_genomes) throws JargonException {
        String[] genomes = filter_genomes.split(",");
        ArrayList<String> paths = new ArrayList<>();
        for (String genome : genomes) {
            log.info("Processing " + genome);
            if (genome.matches("^GCA_[0-9]+\\.[0-9]+$")) {
                // Obtain fasta file if it does not yet exists
                String directory = createDirectory(genome);

                IRODSFile irodsFile = connection.fileFactory.instanceIRODSFile("/" + connection.irodsAccount.getZone() + "/references/genomes" + directory + "/" + genome + "/" + genome + ".fasta.gz");
                if (irodsFile.exists()) {
                    log.info("Genome " + irodsFile.getName() + " already present");
                    paths.add(irodsFile.getAbsolutePath());
                } else {
                    // Create directory
                    connection.fileFactory.instanceIRODSFile(irodsFile.getParentFile().getAbsolutePath()).mkdirs();

                    // Download the file if it does not exists
                    File fasta = new File(genome + ".fasta.gz");
                    if (!fasta.exists()) {
                        String command = "wget https://www.ebi.ac.uk/ena/browser/api/fasta/" + genome + "?download=true&gzip=true -O " + fasta;
                        ExecCommand execCommand = new ExecCommand(command);
                        System.err.println(execCommand.getOutput());
                    }

                    log.info("Transferring " + fasta.getName() + " to " + irodsFile.getAbsolutePath());

                    if (fasta.length() < 1000) {
                        System.err.println(fasta.length());
                        throw new JargonException("File size of " + fasta.getName() + " below 1000");
                    }

                    DataTransferOperations dataTransferOperationsAO = connection.irodsFileSystem.getIRODSAccessObjectFactory().getDataTransferOperations(connection.irodsAccount);
                    StatusCallbackListener statusCallbackListener = new StatusCallbackListener();

                    TransferControlBlock defaultTransferControlBlock = DefaultTransferControlBlock.instance();
                    TransferOptions transferOptions = dataTransferOperationsAO.buildTransferOptionsBasedOnJargonProperties();
                    transferOptions.setIntraFileStatusCallbacks(true);
                    transferOptions.setIntraFileStatusCallbacksNumberCallsInterval(1);
                    transferOptions.setIntraFileStatusCallbacksTotalBytesInterval(2);
                    System.err.println("isComputeChecksumAfterTransfer " + transferOptions.isComputeChecksumAfterTransfer());
                    System.err.println("isComputeAndVerifyChecksumAfterTransfer " + transferOptions.isComputeAndVerifyChecksumAfterTransfer());
                    defaultTransferControlBlock.setTransferOptions(transferOptions);
                    dataTransferOperationsAO.putOperation(fasta, irodsFile, statusCallbackListener, defaultTransferControlBlock);
                    log.info("Transfer completed");
                    paths.add(irodsFile.getAbsolutePath());
                }
            } else if (genome.matches("^GCA_[0-9]+$")) {
                throw new JargonException("No version number available for " + genome);
            }else {
                // Check if path exists in irods
                IRODSFile irodsGenome = connection.fileFactory.instanceIRODSFile(genome);
                if (irodsGenome.exists()) {
                    paths.add(irodsGenome.getAbsolutePath());
                } else {
                    throw new JargonException("No genome found with " + genome);
                }
            }
        }
        return paths;
    }

    private static String createDirectory(String identifier) {
        String id = identifier.split("_")[0];
        String numeric = identifier.split("_")[1];
        String part = "";
        ArrayList<String> parts = new ArrayList<>();
        for (int i = 0; i < 11; i++) {
            if (part.length() < 3)
                part += numeric.toCharArray()[i];
            if (part.length() == 3) {
                parts.add(part);
                part = "";
            }
        }

        String path = "";
        for (int i = 0; i < parts.size(); i++) {
            if (i > 0)
                path = path + "/" + id + "_";
            for (int i1 = 0; i1 < i; i1++) {
                path = path + parts.get(i1);
            }
        }
        return path;
    }
}
