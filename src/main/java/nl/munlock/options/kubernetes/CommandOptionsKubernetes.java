package nl.munlock.options.kubernetes;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import nl.munlock.options.irods.CommandOptionsIRODS;

import java.lang.reflect.Field;

public class CommandOptionsKubernetes extends CommandOptionsIRODS {

    @Parameter(names = {"-k", "-kubernetes"})
    public boolean kubernetes = false;

    @Parameter(names = {"--help", "-h", "-help"})
    public boolean help = false;

    @Parameter(names = {"-limit"}, description = "Total number of items in nl.wur.ssb.kubernetes")
    public int limit = 10;

    @Parameter(names = {"-sleep"}, description = "Total number of seconds to sleep after kubernetes limit has been reached")
    public int sleep = 60;

//    @Parameter(names = {"-priority"}, description = "Kubernetes priority value")
//    public int priority = 0;

    @Parameter(names = {"-delay"}, description = "Kubernetes job submission delay (seconds)")
    public int delay = 1;

    @Parameter(names = {"-disableProv"}, description = "Disable provenance output")
    public boolean disableProvenance = false;

    @Parameter(names = {"-reset"}, description = "Reset jobs to waiting")
    public boolean reset;

    @Parameter(names = {"-yaml"}, description = "Workflow yaml name to filter for job types")
    public String yaml = "%.yaml";

    @Parameter(names = {"-timeout"}, description = "Set a job timeout (in seconds) for kubernetes pod")
    public long timeout = 0;

    @Parameter(names = {"-memory"}, description = "Set default memory value (overridden by yaml content)")
    public int memory = 5000;

    @Parameter(names = {"-threads"}, description = "Set default number of threads (overridden by yaml content)")
    public int threads = 2;

//    @Parameter(names = {"-label"}, description = "Set of labels for submission")
//    public ArrayList<String> labels = new ArrayList<>();

    @Parameter(names = {"-node"}, description = "Node selector by name")
    public String node = "";


    public CommandOptionsKubernetes(String args[]) {
        try {
            JCommander jc = new JCommander(this);
            jc.parse(args);

            // if any of the nl.wur.ssb.options is null...
            boolean failed = false;
            for (Field f : getClass().getDeclaredFields())
                if (f.get(this) == null) {
                    System.err.println(f + " system variable not found, see help for more information");
                    failed = true;
                }

            if (this.help || failed) {
                throw new ParameterException("something failed");
            }
        } catch (ParameterException | IllegalAccessException pe) {
            int exitCode = 64;
            if (this.help) {
                exitCode = 0;
            }
            System.out.println(pe.getMessage());
            new JCommander(this).usage();
            System.out.println("  * required parameter");
            System.exit(exitCode);
        }
    }

    public CommandOptionsKubernetes() {
    }
}
