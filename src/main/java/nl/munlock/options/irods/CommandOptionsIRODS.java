package nl.munlock.options.irods;

import com.beust.jcommander.Parameter;

public class CommandOptionsIRODS {

    // Default nl.wur.ssb.irods settings
    @Parameter(names = {"--username", "-username"}, description = "Username for iRODS authentication (System variable: irodsUserName)")
    public String username = System.getenv("irodsUserName");

    @Parameter(names = {"--password","-password"}, description = "Password for iRODS authentication (System variable: irodsPassword)")
    public String password = System.getenv("irodsPassword");

    @Parameter(names = {"--host","-host"}, description = "Host for iRODS authentication (System variable: irodsHost)")
    public String host = System.getenv("irodsHost");

    @Parameter(names = {"--port","-port"}, description = "Port for iRODS authentication (System variable: irodsPort)")
    public String port = System.getenv("irodsPort");

    @Parameter(names = {"--zone","-zone"}, description = "Zone for iRODS authentication (System variable: irodsZone)")
    public String zone = System.getenv("irodsZone");

    @Parameter(names = {"--authentication","-authentication"}, description = "Authentication scheme for iRODS authentication, e.g. \"password\" (System variable: irodsAuthScheme)")
    public String authentication = System.getenv("irodsAuthScheme");

    @Parameter(names = {"--sslpolicy", "-sslpolicy"}, description = "SSL Negotiation policy e.g. CS_NEG_REQUIRE, CS_NEG_REFUSE (System variable: irodsSsl)")
    public String sslPolicyString = System.getenv("irodsSSL");

    // ISA
    @Parameter(names = {"-project"}, description = "Specify for which project this nl.wur.ssb.workflow should be executed")
    public String project = "%";

    @Parameter(names = {"-investigation"}, description = "Specify for which investigation this nl.wur.ssb.workflow should be executed")
    public String investigation = "%";

    @Parameter(names = {"-study"}, description = "Specify for which study this nl.wur.ssb.workflow should be executed")
    public String study = "%";

    @Parameter(names = {"-observationUnit"}, description = "Specify for which observational unit this nl.wur.ssb.workflow should be executed")
    public String observationUnit = "%";

    @Parameter(names = {"-sample"}, description = "Specify for which sample this nl.wur.ssb.workflow should be executed")
    public String sample = "%";

    @Parameter(names = {"-assay"}, description = "Specify for which assay this nl.wur.ssb.workflow should be executed")
    public String assay = "%";

    @Parameter(names = {"-debug"}, description = "Enables debug mode")
    public Boolean debug = false;

}
