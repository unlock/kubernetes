package nl.munlock.options.workflow;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Field;

public class CommandOptionsNanopore extends CommandOptionsYAML {

    @Parameter(names = {"-kraken_database"}, description = "The path to the kraken database")
    public String kraken_database = "/unlock/references/databases/Kraken2/K2_PlusPF_20210517";

    @Parameter(names = {"-basecall_model"}, description = "The basecall model to be used")
    public String basecall_model = "r941_min_hac_g507";

    @Parameter(names = {"-genome_size"}, description = "Genome size when known")
    public String genome_size = "";

    @Parameter(names = {"-configuration_command"}, description = "Configuration command to be used for guppy")
    public String configuration_command = "dna_r9.4.1_450bps_hac.cfg";

    @Parameter(names = {"-filter_references"}, description = "List of GCA numbers separated by a comma (GCA_000000001.2,GCA_000000002.1")
    public String filter_references = "GCA_000001405.28";

    @Parameter(names = {"-binning"}, description = "Run binning workflow using Illumina data")
    public boolean binning;

    @Parameter(names = {"-deduplicate"}, description = "Deduplicate illumina reads (exact matches)")
    public boolean deduplicate;

    @Parameter(names = {"-use_reference_mapped_reads"}, description = "Use the reads mapped to the given reference(s)")
    public boolean use_reference_mapped;

    @Parameter(names = {"-level"}, description = "Level of analysis (OU, Assay, Sample)", required = true)
    public String level = "OU";


    public CommandOptionsNanopore(String args[]) {
        try {
            JCommander jc = new JCommander(this);
            jc.parse(args);

            // Increase default memory
            if (!StringUtils.join(args, " ").contains("-memory")) {
                this.memory = 40000;
            }
            if (!StringUtils.join(args, " ").contains("-threads")) {
                this.threads = 30;
            }

            // if any of the nl.wur.ssb.options is null...
            boolean failed = false;
            for (Field f : getClass().getDeclaredFields())
                if (f.get(this) == null) {
                    System.err.println(f + " system variable not found, see help for more information");
                    failed = true;
                }

            if (this.help || failed) {
                throw new ParameterException("something failed");
            }
        } catch (ParameterException | IllegalAccessException pe) {
            int exitCode = 64;
            if (this.help) {
                exitCode = 0;
            }
            System.out.println(pe.getMessage());
            new JCommander(this).usage();
            System.out.println("  * required parameter");
            System.exit(exitCode);
        }
    }
}
