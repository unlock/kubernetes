package nl.munlock.options.workflow;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Field;

public class CommandOptionsRNASeq extends CommandOptionsYAML {

    @Parameter(names = {"-star"}, description = "Run RNA seq workflow using STAR")
    public boolean star = false;

    @Parameter(names = {"-bowtie2"}, description = "Run RNA seq workflow using BOWTIE2")
    public boolean bowtie2 = false;

    public CommandOptionsRNASeq(String args[]) {
        try {
            JCommander jc = new JCommander(this);
            jc.parse(args);

            // Increase default memory
            if (!StringUtils.join(args, " ").contains("-memory")) {
                this.memory = 40000;
            }
            if (!StringUtils.join(args, " ").contains("-threads")) {
                this.threads = 30;
            }

            // if any of the nl.wur.ssb.options is null...
            boolean failed = false;
            for (Field f : getClass().getDeclaredFields())
                if (f.get(this) == null) {
                    System.err.println(f + " system variable not found, see help for more information");
                    failed = true;
                }

            if (this.help || failed) {
                throw new ParameterException("something failed");
            }
        } catch (ParameterException | IllegalAccessException pe) {
            int exitCode = 64;
            if (this.help) {
                exitCode = 0;
            }
            System.out.println(pe.getMessage());
            new JCommander(this).usage();
            System.out.println("  * required parameter");
            System.exit(exitCode);
        }
    }
}
