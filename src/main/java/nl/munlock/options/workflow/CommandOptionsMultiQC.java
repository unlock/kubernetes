package nl.munlock.options.workflow;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;

import java.lang.reflect.Field;

public class CommandOptionsMultiQC extends CommandOptionsYAML {

    @Parameter(names = {"-kraken_database"}, description = "The path to the kraken database")
    public String kraken_database = "/unlock/references/databases/Kraken2/K2_PlusPF_20210517";

    @Parameter(names = {"-level"}, description = "Level of analysis (Study, Investigation)", hidden = true)
    public String level = "Study";


    public CommandOptionsMultiQC(String args[]) {
        try {
            JCommander jc = new JCommander(this);
            jc.parse(args);

            // if any of the nl.wur.ssb.options is null...
            boolean failed = false;
            for (Field f : getClass().getDeclaredFields())
                if (f.get(this) == null) {
                    System.err.println(f + " system variable not found, see help for more information");
                    failed = true;
                }

            if (this.help || failed) {
                throw new ParameterException("something failed");
            }
        } catch (ParameterException | IllegalAccessException pe) {
            int exitCode = 64;
            if (this.help) {
                exitCode = 0;
            }
            System.out.println(pe.getMessage());
            new JCommander(this).usage();
            System.out.println("  * required parameter");
            System.exit(exitCode);
        }
    }
}
