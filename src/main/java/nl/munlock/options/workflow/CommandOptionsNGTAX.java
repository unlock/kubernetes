package nl.munlock.options.workflow;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;

import java.lang.reflect.Field;

public class CommandOptionsNGTAX extends CommandOptionsYAML {

    @Parameter(names = {"-length"}, description = "Read length used in NGTAX (default maximum length)")
    public int read_len = 0;

    @Parameter(names = {"-minimumThreshold"}, description = "Minimum threshold detectable, expressed in percentage")
    public double minimumThreshold = 0.1;

    @Parameter(names = {"-referenceDB"}, description = "Reference database to disable provide empty string -referenceDB ''")
    public String reference_db = "/unlock/references/databases/Silva/SILVA_132_SSURef_tax_silva.fasta.gz";

    @Parameter(names = {"-mock"}, description = "Perform mock creation (e.g., when using public datasets)")
    public boolean mock;

    @Parameter(names = {"-primersRemoved"}, description = "If the primers are removed or not")
    public boolean primersRemoved;


    public CommandOptionsNGTAX(String args[]) {
        try {
            JCommander jc = new JCommander(this);
            jc.parse(args);

            // if any of the nl.wur.ssb.options is null...
            boolean failed = false;
            for (Field f : getClass().getDeclaredFields())
                if (f.get(this) == null) {
                    System.err.println(f + " system variable not found, see help for more information");
                    failed = true;
                }

            if (this.help || failed) {
                throw new ParameterException("something failed");
            }
        } catch (ParameterException | IllegalAccessException pe) {
            int exitCode = 64;
            if (this.help) {
                exitCode = 0;
            }
            System.out.println(pe.getMessage());
            new JCommander(this).usage();
            System.out.println("  * required parameter");
            System.exit(exitCode);
        }
    }
}
