package nl.munlock.options.workflow;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import nl.munlock.options.irods.CommandOptionsIRODS;
import nl.munlock.options.kubernetes.CommandOptionsKubernetes;

import java.lang.reflect.Field;

public class CommandOptionsYAML extends CommandOptionsIRODS {
    @Parameter(names = {"--help", "-h", "-help"})
    public boolean help = false;

    @Parameter(names = {"-cwl"}, description = "CWL file stored on iRODS")
    public String cwl;

    // This is done at nl.wur.ssb.yaml level
    @Parameter(names = {"-wid", "-workflowid"}, description = "Identifier used for the nl.wur.ssb.workflow (could be the nl.wur.ssb.workflow name + hash)", required = true)
    public String wid;

//    @Parameter(names = {"-yaml"}, description = "Yaml irods file name")
//    public String yaml = "";

    @Parameter(names = {"-overwrite"}, description = "Overwrites already existing yaml files")
    public boolean overwrite = false;

    @Parameter(names = {"-threads"}, description = "Number of threads to use")
    public int threads = 2;

    @Parameter(names = {"-memory"}, description = "Amount of memory needed (in megabytes)")
    public int memory = 5000;

    @Parameter(names = {"-conda"}, description = "Conda environment to use.")
    public String conda = "";

    public CommandOptionsYAML(String args[]) {
            try {
                JCommander jc = new JCommander(this);
                jc.setAcceptUnknownOptions(true);
                jc.parse(args);

                // if any of the options is null...
                boolean failed = false;
                for (Field f : getClass().getDeclaredFields())
                    if (f.get(this) == null) {
                        System.err.println(f + " system variable not found, see help for more information");
                        failed = true;
                    }

                if (this.help || failed) {
                    throw new ParameterException("something failed");
                }
            } catch (ParameterException | IllegalAccessException pe) {
                int exitCode = 64;
                if (this.help) {
                    exitCode = 0;
                }
                System.out.println(pe.getMessage());
                new JCommander(this).usage();
                System.out.println("  * required parameter");
                System.exit(exitCode);
            }
        }

    public CommandOptionsYAML() {
        }
    }