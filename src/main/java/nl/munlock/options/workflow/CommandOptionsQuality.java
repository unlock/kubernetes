package nl.munlock.options.workflow;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;

import java.lang.reflect.Field;

public class CommandOptionsQuality extends CommandOptionsYAML {

    @Parameter(names = {"-kraken_database"}, description = "The path to the kraken database")
    public String kraken_database = "/unlock/references/databases/Kraken2/K2_PlusPF_20210517";

    @Parameter(names = {"-filter_references"}, description = "List of GCA numbers separated by a comma (GCA_000000001.2,GCA_000000002.1")
    public String filter_references = "GCA_000001405.28";

    // Hidden for now not sure if needed
    @Parameter(names = {"-level"}, description = "Level of analysis (OU, Assay, Sample)", hidden = true)
    public String level = "OU";
    @Parameter(names = {"-rrna"}, description = "rRNA filtering")
    public boolean rrna;
    @Parameter(names = {"-run_filter_references"}, description = "run filter references")
    public boolean run_filter_references;


    public CommandOptionsQuality(String args[]) {
        try {
            JCommander jc = new JCommander(this);
            jc.parse(args);

            // if any of the nl.wur.ssb.options is null...
            boolean failed = false;
            for (Field f : getClass().getDeclaredFields())
                if (f.get(this) == null) {
                    System.err.println(f + " system variable not found, see help for more information");
                    failed = true;
                }

            if (this.help || failed) {
                throw new ParameterException("something failed");
            }
        } catch (ParameterException | IllegalAccessException pe) {
            int exitCode = 64;
            if (this.help) {
                exitCode = 0;
            }
            System.out.println(pe.getMessage());
            new JCommander(this).usage();
            System.out.println("  * required parameter");
            System.exit(exitCode);
        }
    }
}
