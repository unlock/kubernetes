package nl.munlock.kubernetes;

import nl.munlock.irods.Connection;
import nl.munlock.options.kubernetes.CommandOptionsKubernetes;
import org.apache.log4j.Level;

public class App {
    public static void main(String[] args) throws Exception {
        org.apache.log4j.Logger.getLogger("org").setLevel(Level.OFF);
        CommandOptionsKubernetes commandOptions = new CommandOptionsKubernetes(args);
        Connection connection = new Connection(commandOptions);
        Kubernetes.main(commandOptions, connection);
    }
}
