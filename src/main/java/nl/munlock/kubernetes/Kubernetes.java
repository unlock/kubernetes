package nl.munlock.kubernetes;

import com.esotericsoftware.yamlbeans.YamlConfig;
import com.esotericsoftware.yamlbeans.YamlReader;
import com.google.gson.JsonSyntaxException;
import io.kubernetes.client.custom.Quantity;
import io.kubernetes.client.openapi.ApiClient;
import io.kubernetes.client.openapi.ApiException;
import io.kubernetes.client.openapi.Configuration;
import io.kubernetes.client.openapi.apis.BatchV1Api;
import io.kubernetes.client.openapi.apis.CoreV1Api;
import io.kubernetes.client.openapi.models.*;
import io.kubernetes.client.util.Config;
import nl.munlock.Generic;
import nl.munlock.irods.Connection;
import nl.munlock.irods.Search;
import nl.munlock.objects.Workflow;
import nl.munlock.options.kubernetes.CommandOptionsKubernetes;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.irods.jargon.core.exception.JargonException;
import org.irods.jargon.core.query.*;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.TimeUnit;

import static nl.munlock.irods.Search.queueAVU;
import static nl.munlock.irods.Search.resetFailures;
import static nl.munlock.yaml.Workflow.fixClass;

public class Kubernetes {
    private static final Logger log = Generic.getLogger(Kubernetes.class, false);
    public static HashSet<String> yamls = new HashSet<>();

    public static void main(CommandOptionsKubernetes commandOptionsKubernetes, Connection connection) throws JargonException, JargonQueryException, IOException, ApiException, GenQueryBuilderException, InterruptedException {
        getYamls(commandOptionsKubernetes, connection);
        createJobs(commandOptionsKubernetes, connection);
    }

    public static void createJobs(CommandOptionsKubernetes commandOptionsKubernetes, Connection connection) throws IOException, JargonQueryException, JargonException, InterruptedException, ApiException {
        // Requires the kube config file
        ApiClient client = Config.defaultClient();

        if (commandOptionsKubernetes.debug) {
            client.setDebugging(true);
        } else {
            client.setDebugging(false);
        }

        Configuration.setDefaultApiClient(client);

        int assayCount = 0;
        // Check nl.wur.ssb.kubernetes if it has room for more jobs...
        int totalItems = checkKubernetesJobs(client);

        // Obtain all jobs
        BatchV1Api batchV1Api = new BatchV1Api();
        List<V1Job> v1Jobs = batchV1Api.listNamespacedJob("unlock", null, null, null, null, null, null, null, null, null, null).getItems();
        HashSet<String> jobNames = new HashSet<>();
        for (V1Job v1Job : v1Jobs) {
            String name = v1Job.getMetadata().getName();
            System.err.println(name);
            name = name.substring(0,name.lastIndexOf("-"));
            System.err.println(name);
            jobNames.add(name);
        }

        for (String yaml : yamls) {

            // Make sure that yaml string is using the unix file path
            yaml = FilenameUtils.separatorsToUnix(yaml);

            assayCount = assayCount + 1;

            V1Job v1Job = generateKubernetesJobObject(commandOptionsKubernetes, yaml, connection);

            try {
                // Sleeping for x seconds...
                while (totalItems >= commandOptionsKubernetes.limit) {
                    log.info("Sleeping for " + commandOptionsKubernetes.sleep + " seconds as there are " + totalItems + " jobs running");
                    // Count down?
                    for (int i = commandOptionsKubernetes.sleep; i >= 0; i--) {
                        System.out.print("Sleeping for " + i + " seconds\r");
                        TimeUnit.SECONDS.sleep(1);
                    }
                    totalItems = checkKubernetesJobs(client);
                }

                batchV1Api.createNamespacedJob("unlock", v1Job, null, null, null);
                batchV1Api.createNamespacedJobCall("unlock", v1Job, null, null, null, null);
            } catch (ApiException e) {
                System.err.println(e.getMessage());
                System.err.println(e.getResponseBody());
                throw new IOException();
            }
            // Changing waiting to queue
            queueAVU(connection, yaml);

            log.debug("Delay implemented for " + commandOptionsKubernetes.delay + " seconds");
            TimeUnit.SECONDS.sleep(commandOptionsKubernetes.delay);

            totalItems = totalItems + 1;

            log.debug("Jobs currently running " + totalItems);
        }
        yamls = new HashSet<>();
    }

    private static void getYamls(CommandOptionsKubernetes commandOptionsKubernetes, Connection connection) throws JargonException, GenQueryBuilderException, JargonQueryException, IOException, ApiException, InterruptedException {

        if (commandOptionsKubernetes.project.contains("references")) {
            for (File level1 : connection.fileFactory.instanceIRODSFile("/unlock/references/genomes/").listFiles()) {
                if (!level1.getName().startsWith("GCA_")) continue;
                if (level1.isDirectory()) {
                    String path = level1.getAbsolutePath() + "/%";
                    log.debug("Analyzing " + path);
                    if (commandOptionsKubernetes.reset) {
                        resetFailures(connection, path);
                    }
                    Search.getAllUnprocessedReferences(commandOptionsKubernetes, connection, path);
                }
            }
        } else if (commandOptionsKubernetes.project.contains("ampliconlibraries")) {
            String search = Search.makePath(commandOptionsKubernetes.project, commandOptionsKubernetes.investigation, commandOptionsKubernetes.study, commandOptionsKubernetes.observationUnit, commandOptionsKubernetes.sample, commandOptionsKubernetes.assay, connection);
            // /unlock/landingzone/projects/P_???/I_???/demultiplexed/jobs/mapping_file_???.yaml
            search = search.replaceAll("/unlock/projects/ampliconlibraries/", "/" + commandOptionsKubernetes.zone + "/landingzone/projects/%");
            Search.getAllUnprocessed(commandOptionsKubernetes, connection, search);
//                Kubernetes.createJobs(commandOptionsKubernetes, connection);
        } else {
            Search.getAllUnprocessed(commandOptionsKubernetes, connection);
            Kubernetes.createJobs(commandOptionsKubernetes, connection);
        }
    }


    /**
     * Only manages the irodsrunner- jobs... and cleans them up when needed...
     *
     * @param client
     * @return
     * @throws ApiException
     */
    public static int checkKubernetesJobs(ApiClient client) throws ApiException {

        BatchV1Api batchV1Api = new BatchV1Api(client);

        List<V1Job> v1Jobs = batchV1Api.listNamespacedJob("unlock", null, null, null, null, null, null, null, null, null, null).getItems();

        int totalJobs = v1Jobs.size();
        log.debug("Total jobs before removal " + totalJobs);
        Iterator<V1Job> v1JobIterator = v1Jobs.iterator();
        while (v1JobIterator.hasNext()) {
            V1Job v1Job = v1JobIterator.next();
            if (v1Job.getMetadata().getName().startsWith("cwl-")) {
                if (v1Job.getStatus().getActive() == null) {
                    log.debug("Deleting " + v1Job.getMetadata().getName() + " " + totalJobs);
                    try {
                        batchV1Api.deleteNamespacedJob(v1Job.getMetadata().getName(), v1Job.getMetadata().getNamespace(), null, null, null, null, null, null);
                    } catch (com.google.gson.JsonSyntaxException | ApiException e) {
                        if (e.getMessage().contains("Expected a string but was BEGIN_OBJECT")) {
                            // Exception thrown when item is being deleted, api is waiting for new implementation...
                        } else if (e.getMessage().contains("Not Found")) {
                            // Job was already on the way to deletion and we went to fast?
                        } else {
                            log.error(e.getMessage());
                            log.error(StringUtils.join(e.getStackTrace(), "\n"));
                            throw new JsonSyntaxException(e.getMessage());
                        }
                        totalJobs = totalJobs - 1;
                    }
                }
            }
        }

        // Remove pods as TTL is not always working when finished
        CoreV1Api api = new CoreV1Api();
        V1PodList v1PodList = api.listPodForAllNamespaces(null, null, null, null, null, null, null, null, null, null);
        Iterator<V1Pod> v1PodIterator = v1PodList.getItems().iterator();
        while (v1PodIterator.hasNext()) {
            V1Pod v1Pod = v1PodIterator.next();
            if (v1Pod.getMetadata().getName().startsWith("cwl-")) {
                if (v1Pod.getStatus().getPhase().matches("Succeeded")) {
                    log.debug("Deleting " + v1Pod.getMetadata().getName() + " " + totalJobs);
                    try {
                        api.deleteNamespacedPod(v1Pod.getMetadata().getName(), v1Pod.getMetadata().getNamespace(), null, null, null, null, null, null);
                    } catch (com.google.gson.JsonSyntaxException | ApiException e) {
                        if (e.getMessage().contains("Expected a string but was BEGIN_OBJECT")) {
                            // Exception thrown hen item is being deleted, api is waiting for new implementation...
                        } else if (e.getMessage().contains("Not Found")) {
                            // Job was already on the way to deletion and we went to fast?
                        } else {
                            System.exit(1);
                            throw new JsonSyntaxException(e.getMessage());

                        }
                        totalJobs = totalJobs - 1;
                    }
                }
            }
        }

        if (totalJobs < 0) {
            totalJobs = 0;
        }

        log.debug("Jobs currently running " + totalJobs);

        // Remove error GCA pods
        api = new CoreV1Api(client);
        List<V1Pod> v1Pods = api.listPodForAllNamespaces(null, null, "", null, null, null, null, null, null, null).getItems();
        for (V1Pod v1Pod : v1Pods) {
            if (v1Pod.getMetadata().getName().startsWith("cwl-gc")) {
                if (v1Pod.getStatus().getPhase().matches("Failed")) {
                    try {
                        api.deleteNamespacedPod(v1Pod.getMetadata().getName(), v1Pod.getMetadata().getNamespace(), null, null, null, null, null, null);
                        log.debug("Removing GCA pod " + v1Pod.getMetadata().getName());
                    } catch (com.google.gson.JsonSyntaxException | ApiException e) {
                        if (e.getMessage().contains("Expected a string but was BEGIN_OBJECT")) {
                            // Exception thrown when item is being deleted, api is waiting for new implementation...
                        } else if (e.getMessage().contains("Not Found")) {
                            // Job was already on the way to deletion and we went too fast?
                        } else {
                            throw new JsonSyntaxException(e.getMessage());
                        }
                    }
                }
            }
        }
        return totalJobs;
    }

    private static V1Job generateKubernetesJobObject(CommandOptionsKubernetes commandOptions, String yamlFile, Connection connection) throws JargonException, JargonQueryException, IOException, IllegalFormatException {
        // Load yaml file for cores and memory restrictions?
        log.info("Loading ." + yamlFile);
        fixClass("." + yamlFile);

        // ! check for yaml file
        Path filePath = new File("." + yamlFile).toPath();
        Charset charset = Charset.defaultCharset();
        List<String> stringList = Files.readAllLines(filePath, charset);
        if (stringList.size() == 0) {
            System.err.println("STRINGLIST EMPTY " + filePath);
        } else if (stringList.get(0).startsWith("!")) {
            stringList.remove(0);
        }

        String content = StringUtils.join(stringList, "\n");
        //
        YamlConfig yamlConfig = new YamlConfig();
        yamlConfig.readConfig.setIgnoreUnknownProperties(true);
        YamlReader reader = new YamlReader(content, yamlConfig);
        Workflow workflow = reader.read(Workflow.class);

        if (workflow.threads == 0) {
            workflow.threads = commandOptions.threads;
        }

        if (workflow.memory == 0) {
            workflow.memory = commandOptions.memory;
        }

        log.debug("Memory: " + workflow.memory);
        log.debug("Threads: " + workflow.threads);

        List<AVUQueryElement> avuQueryElements = new ArrayList<>();
        avuQueryElements.add(AVUQueryElement.instanceForValueQuery(AVUQueryElement.AVUQueryPart.ATTRIBUTE, QueryConditionOperators.EQUAL, "cwl"));
        List<MetaDataAndDomainData> metaDataAndDomainData = connection.accessObjectFactory.getDataObjectAO(connection.irodsAccount).findMetadataValuesForDataObjectUsingAVUQuery(avuQueryElements, yamlFile);
        String cwlFile;
        if (metaDataAndDomainData.size() != 0)
            cwlFile = metaDataAndDomainData.get(0).asAvu().getValue();
        else {
            // Due to bulk upload no metadata was set
            cwlFile = "/unlock/infrastructure/cwl/ena/ena_wget.cwl";
            // throw new InvalidArgumentException("No CWL?");
        }

        log.debug("CWL: " + cwlFile);

        V1PodTemplateSpec template = new V1PodTemplateSpec();

        // Defines the docker container
        V1Container containerItem = new V1Container();
        containerItem.name(workflow.identifier.toLowerCase().replaceAll("[^A-Za-z0-9]", "-") + "-xxx");
        containerItem.image("docker-registry.wur.nl/unlock/docker:kubernetes");
        containerItem.setImagePullPolicy("Always");

        V1ResourceRequirements resource = new V1ResourceRequirements();
        // Set limits to the job
        Map<String, Quantity> limit = new HashMap<>();
        Quantity quantity_cpu = new Quantity(workflow.threads * 1000 + "m");
        Quantity quantity_mem = new Quantity(workflow.memory + "Mi");
        limit.put("cpu", quantity_cpu);
        limit.put("memory", quantity_mem);
        resource.limits(limit);

        // Set requests for the job
        Map<String, Quantity> request = new HashMap<>();
        quantity_mem = new Quantity(workflow.memory + "Mi");
        quantity_cpu = new Quantity(workflow.threads * 1000 + "m");
        request.put("memory", quantity_mem);
        request.put("cpu", quantity_cpu);
        resource.requests(request);

        containerItem.resources(resource);

        // List of arguments for the command during startup
        List<String> args = new ArrayList<>();

        // Execute.sh should sync the collection after the run
        if (commandOptions.debug) {
            args.add("-x");
        }

        args.add("/run.sh");

        // The cwl workflow file
        args.add("-c");
        args.add(cwlFile);

        // The yaml file
        args.add("-y");
        args.add(yamlFile);

        // Provenance
        args.add("-p");
        if (workflow.provenance) {
            args.add("true");
        } else {
            args.add("false");
        }

        containerItem.args(args);

        // Base command
        List<String> command = new ArrayList<>();
        command.add("bash");
        containerItem.command(command);

        // Unlock mount
        containerItem.addVolumeMountsItem(new V1VolumeMount().name("unlock").mountPath("/unlock"));

        // Set the name
        containerItem.name(workflow.identifier.toLowerCase().replaceAll("[^A-Za-z0-9]", "-") + "-xxx");

        // Setting secrets
        containerItem.addEnvItem(setSecret("irodsHost", "unlock-secret", "irodsHost"));
        containerItem.addEnvItem(setSecret("irodsPort", "unlock-secret", "irodsPort"));
        containerItem.addEnvItem(setSecret("irodsUserName", "unlock-secret", "irodsUserName"));
        containerItem.addEnvItem(setSecret("irodsZone", "unlock-secret", "irodsZone"));
        containerItem.addEnvItem(setSecret("irodsAuthScheme", "unlock-secret", "irodsAuthScheme"));
        containerItem.addEnvItem(setSecret("irodsHome", "unlock-secret", "irodsHome"));
        containerItem.addEnvItem(setSecret("irodsCwd", "unlock-secret", "irodsCwd"));
        containerItem.addEnvItem(setSecret("irodsPassword", "unlock-secret", "irodsPassword"));
        containerItem.addEnvItem(setSecret("irodsSSL", "unlock-secret", "irodsSSL"));

        // Job specifications
        V1JobSpec v1JobSpec = new V1JobSpec();
        v1JobSpec.ttlSecondsAfterFinished(1000);
        v1JobSpec.setTtlSecondsAfterFinished(1000);
        v1JobSpec.setBackoffLimit(1);
        // Set job timeout
        if (commandOptions.timeout > 0)
            v1JobSpec.activeDeadlineSeconds(commandOptions.timeout);

        // 60 min runtime, disabled as when queue is large jobs get killed when not even started
        // specs.activeDeadlineSeconds((long) 3600);
        v1JobSpec.template(template);

        V1PodSpec v1PodSpec = new V1PodSpec();
        // https://rancher.com/docs/k3s/latest/en/storage/
        v1PodSpec.addVolumesItem(new V1Volume().name("unlock").persistentVolumeClaim(new V1PersistentVolumeClaimVolumeSource().claimName("unlock")));
        v1PodSpec.addContainersItem(containerItem);
        v1PodSpec.restartPolicy("Never");
        if (commandOptions.node.length() > 0) {
            HashMap<String, String> nodeSelector = new HashMap<>();
            if (commandOptions.node.contains(",")) {
                ArrayList<String> nodes = new ArrayList<>(List.of(commandOptions.node.split(",")));
                Collections.shuffle(nodes);
                nodeSelector.put("kubernetes.io/hostname", nodes.get(0));
            } else {
                nodeSelector.put("kubernetes.io/hostname", commandOptions.node);
            }
            v1PodSpec.nodeSelector(nodeSelector);
        }
        template.spec(v1PodSpec);
        // Set priority level
        // v1PodSpec.setPriority(commandOptions.priority);

        V1ObjectMeta v1ObjectMeta = new V1ObjectMeta();
        String name = "cwl-" + workflow.identifier.toLowerCase().replaceAll("[^A-Za-z0-9]", "-");
        System.err.println(name);
        v1ObjectMeta.generateName(name);
        HashMap<String, String> metadata = new HashMap<>();
        // Project information (work in progress)
        if (!commandOptions.project.contains("%"))
            metadata.put("project", commandOptions.project);
        if (!commandOptions.investigation.contains("%"))
            metadata.put("investigation", commandOptions.investigation);
        if (!commandOptions.study.contains("%"))
            metadata.put("study", commandOptions.study);

        // ...
        v1ObjectMeta.annotations(metadata);
        V1Job v1Job = new V1Job();
        v1Job.apiVersion("batch/v1");
        v1Job.kind("Job");
        // v1Job.metadata(v1ObjectMeta);
        v1Job.spec(v1JobSpec);

        // Add all metadata components to the v1object
        v1Job.setMetadata(v1ObjectMeta);
        template.metadata(v1ObjectMeta);

        return v1Job;
    }

    static V1EnvVar setSecret(String name, String secretName, String secretKey) {
        // Container secrets? TODO validate...
        V1EnvVarSource v1EnvVarSource = new V1EnvVarSource();
        V1SecretKeySelector v1SecretKeySelector = new V1SecretKeySelector();
        v1SecretKeySelector.setName(secretName);
        v1SecretKeySelector.setKey(secretKey);
        v1EnvVarSource.secretKeyRef(v1SecretKeySelector);

        V1EnvVar v1EnvVar = new V1EnvVar();
        v1EnvVar.setValueFrom(v1EnvVarSource);
        v1EnvVar.setName(name);
        return v1EnvVar;
    }
}

