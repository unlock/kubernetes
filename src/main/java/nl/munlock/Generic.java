package nl.munlock;

import nl.munlock.irods.Connection;
import org.apache.commons.io.FilenameUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.NodeIterator;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.log4j.*;
import org.irods.jargon.core.checksum.ChecksumValue;
import org.irods.jargon.core.exception.JargonException;
import org.irods.jargon.core.pub.DataObjectChecksumUtilitiesAO;
import org.irods.jargon.core.pub.DataTransferOperations;
import org.irods.jargon.core.pub.io.IRODSFile;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Generic {
    private static final org.slf4j.Logger log = LoggerFactory.getLogger(Generic.class);
    static ArrayList<String> processed = new ArrayList<>();
    /**
     * Logger initialization with debug option
     * @param debug boolean if debug mode should be enabled
     *
     * @return
     */
    public static Logger getLogger(Class clazz, boolean debug) {
        ConsoleAppender console = new ConsoleAppender();
        String PATTERN = "%d %-5p [%c{1}] %m%n";
        console.setLayout(new PatternLayout(PATTERN));
        console.setThreshold(Level.DEBUG);
        console.activateOptions();

        Logger.getRootLogger().removeAllAppenders();
        Logger.getRootLogger().addAppender(console);

        Logger logger = Logger.getLogger(clazz);

        logger.setLevel(Level.INFO);
        if (debug)
            logger.setLevel(Level.DEBUG);


        FileAppender fa = new FileAppender();
        fa.setName("iRODS Logger");
        fa.setFile("runner.log");
        fa.setLayout(new PatternLayout("%d %-5p [%c{1}] %m%n"));
        fa.setThreshold(Level.INFO);
        if (debug) fa.setThreshold(Level.DEBUG);

        fa.setAppend(true);
        fa.activateOptions();
        org.apache.log4j.Logger.getRootLogger().addAppender(fa);
        return logger;
    }

    public static String getAssayPath(File collection) throws FileNotFoundException {
        String regex = "(.*/S_.*?/(Amplicon|RNA|DNA)/A_.*?)/";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(collection.getAbsolutePath() + "/");
        while (matcher.find()) {
            return matcher.group(1);
        }
        throw new FileNotFoundException("Assay path not found in " + collection);
    }

    public static void downloadFile(Connection connection, String download) throws JargonException {
        IRODSFile irodsFile = connection.fileFactory.instanceIRODSFile(download);
        if (!irodsFile.exists()) {
            throw new JargonException("File " + irodsFile + " does not exist");
        }
        org.apache.log4j.Logger.getLogger("org.nl.wur.ssb.irods.jargon.core.transfer").setLevel(Level.OFF);
        DataTransferOperations dataTransferOperationsAO = connection.irodsFileSystem.getIRODSAccessObjectFactory().getDataTransferOperations(connection.irodsAccount);

        // Create folder directory
        File directory = new File("." + download.replaceAll( new File(download).getName() + "$", ""));
        directory.mkdirs();

        File localFile = new File("." + download);

        while (localFile.exists()) {
            localFile.delete();
        }

        // Disables the logger for the transfer as it easily gives thousands of lines...
        org.apache.log4j.Logger.getLogger("org.nl.wur.ssb.irods.jargon.core.transfer").setLevel(Level.OFF);
        log.info("Downloading " + localFile.getName());

        dataTransferOperationsAO.getOperation(irodsFile, localFile, null, null);
    }

//    public static void destinationChecker(Connection connection, File file) {
//        Scanner scanner = new Scanner(file.getAbsolutePath());
//        while (scanner.hasNextLine()) {
//            String line = scanner.nextLine();
//            System.err.println(line);
//            if (line.startsWith("destination")) {
//                System.err.println(line);
//            }
//        }
//    }

    public static String getEdamFormat(String fileName) throws Exception {
        // Always remove compression end when gz
        fileName = fileName.replaceAll(".gz$","");

        String extension = FilenameUtils.getExtension(fileName);

        log.info("File extension " + extension + " detected");
        if (fileName.startsWith("http")) {
            return fileName;
        } else if (extension.matches("bam")) {
            return "http://edamontology.org/format_2572";
        } else if (extension.matches("(fq|fastq)")) {
            return "http://edamontology.org/format_1930";
        } else if (extension.matches("(fa|fasta)")) {
            return "http://edamontology.org/format_1929";
        } else if (extension.matches("ttl")) {
            return "http://edamontology.org/format_3255";
        } else if (extension.matches("embl")) {
            return "http://edamontology.org/format_1927";
        } else {
            log.error("Unknown file format " + extension);
            return null;
        }
    }

    public static String getSHA256(Connection connection, String irodsFilePath) throws JargonException, MalformedURLException {
        if (irodsFilePath.startsWith("http")) {
            irodsFilePath = new URL(irodsFilePath).getPath();
        }
        IRODSFile irodsFile = connection.fileFactory.instanceIRODSFile(irodsFilePath);
        return getSHA256(connection, irodsFile);
    }

    public static String getSHA256(Connection connection, IRODSFile irodsFile) throws JargonException {
        DataObjectChecksumUtilitiesAO dataObjectChecksumUtilitiesAO = connection.irodsFileSystem.getIRODSAccessObjectFactory().getDataObjectChecksumUtilitiesAO(connection.irodsAccount);
        // TODO there is an issue with too many checksum checks
        if (!irodsFile.exists()) {
            connection.close();
            connection.reconnect();
        }
        ChecksumValue checksumValue = dataObjectChecksumUtilitiesAO.retrieveExistingChecksumForDataObject(irodsFile.getAbsolutePath());
        // Todo not sure if this works and it will receive a null for checksum
        if (checksumValue == null) {
            checksumValue = dataObjectChecksumUtilitiesAO.computeChecksumOnDataObject(irodsFile);
        }
        return checksumValue.getHexChecksumValue();
    }
//
//    public static String getSHA256(String irodsFilePath) throws JargonException, MalformedURLException {
//        if (irodsFilePath.startsWith("http")) {
//            irodsFilePath = new URL(irodsFilePath).getPath();
//        }
//        IRODSFile irodsFile = connection.fileFactory.instanceIRODSFile(irodsFilePath);
//        return getSHA256(irodsFile);
//    }

    public static String getBase64(Connection connection, String irodsFilePath) throws JargonException, MalformedURLException {
        if (irodsFilePath.startsWith("http")) {
            irodsFilePath = new URL(irodsFilePath).getPath();
        }
        IRODSFile irodsFile = connection.fileFactory.instanceIRODSFile(irodsFilePath);
        return getBase64(connection, irodsFile);
    }

    public static String getBase64(Connection connection, IRODSFile irodsFile) throws JargonException {
        DataObjectChecksumUtilitiesAO dataObjectChecksumUtilitiesAO = connection.irodsFileSystem.getIRODSAccessObjectFactory().getDataObjectChecksumUtilitiesAO(connection.irodsAccount);
        ChecksumValue checksumValue = dataObjectChecksumUtilitiesAO.retrieveExistingChecksumForDataObject(irodsFile.getAbsolutePath());
        // Todo not sure if this works and it will receive a null for checksum
        if (checksumValue == null) {
            checksumValue = dataObjectChecksumUtilitiesAO.computeChecksumOnDataObject(irodsFile);
        }
        return checksumValue.getBase64ChecksumValue();
    }

    public static NodeIterator getProperty(Model model, String subj, String pred) {
        Resource subjR = null;
        if (subj != null) {
            subjR = model.createResource(subj);
        }

        Property predR = null;
        if (pred != null) {
            predR = model.createProperty(pred);
        }

        return model.listObjectsOfProperty(subjR, predR);
    }
}
