package nl.munlock;

import nl.munlock.yaml.Yaml;
import org.apache.log4j.Logger;

import java.util.Arrays;

public class App {
    /**
     * Main landing point for yaml or kubernetes runs...
     * @param args
     * @throws Exception
     */
    private static final Logger log = Generic.getLogger(Yaml.class, false);

    public static void main(String[] args) throws Exception {
        if (Arrays.asList(args).contains("-cwl")) {
            log.info("Generating YAML files");
            Yaml.main(args);
        } else if (Arrays.asList(args).contains("-kubernetes")) {
            log.info("Executing kubernetes workflows");
            nl.munlock.kubernetes.App.main(args);
        }
    }
}
