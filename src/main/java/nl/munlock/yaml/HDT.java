package nl.munlock.yaml;

import com.esotericsoftware.yamlbeans.YamlWriter;
import nl.munlock.Generic;
import nl.munlock.irods.Connection;
import nl.munlock.irods.Search;
import nl.munlock.objects.WorkflowHDT;
import nl.munlock.options.workflow.CommandOptionsHDT;
import org.apache.log4j.Logger;
import org.irods.jargon.core.pub.DataObjectAO;
import org.irods.jargon.core.pub.DataTransferOperations;
import org.irods.jargon.core.pub.domain.AvuData;
import org.irods.jargon.core.pub.io.IRODSFile;

import java.io.File;
import java.io.FileWriter;
import java.util.Set;

import static nl.munlock.yaml.Index.setTransferControlBlock;

public class HDT {
    private static final Logger log = Generic.getLogger(HDT.class, false);

    public static void generateHDTWorkflow(CommandOptionsHDT commandOptions, Connection connection, Set<String> folders) throws Exception {
        log.info("Generating HDT workflows for " + folders.size());
        DataTransferOperations dataTransferOperationsAO = connection.irodsFileSystem.getIRODSAccessObjectFactory().getDataTransferOperations(connection.irodsAccount);

        for (String folder : folders) {
            // Obtain rough estimate for memory consumption (TEST)
            long memory = Search.getFolderSize(connection, commandOptions, folder, "%ttl");
            if (memory > commandOptions.memory) {
                commandOptions.memory = Math.toIntExact(memory);
            }

            String yamlName = commandOptions.wid + ".yaml";

            // Save to iRODS
            IRODSFile destFile = connection.fileFactory.instanceIRODSFile(folder + "/hdt/" + yamlName);

            if (!destFile.exists()) {
                // If not exists start building
            } else if (commandOptions.overwrite && destFile.exists())
                // If exists and overwrite, delete
                destFile.delete();
            else {
                // Else skip this one
                continue;
            }

            log.info("Uploading " + new File(yamlName) + " to " + destFile);

            IRODSFile irodsFile = connection.fileFactory.instanceIRODSFile(folder + "/hdt/");
            // Prepare directory for YAML file
            irodsFile.mkdirs();

            WorkflowHDT workflowHDT = new WorkflowHDT();
            workflowHDT.setFolder(folder);
            workflowHDT.setDestination(folder + "/hdt/");
            workflowHDT.setMemory(commandOptions.memory);
            workflowHDT.setThreads(Math.round(commandOptions.threads));
            workflowHDT.setProvenance(false);

            YamlWriter writer = new YamlWriter(new FileWriter(yamlName));
            writer.write(workflowHDT);
            writer.close();

            // Fix Clazz > Class
            Workflow.fixClazz(yamlName);

            dataTransferOperationsAO.putOperation(new File(yamlName), destFile, null, setTransferControlBlock(dataTransferOperationsAO, destFile, commandOptions.overwrite));

            // Add metadata tag...
            DataObjectAO dataObjectAO = connection.irodsFileSystem.getIRODSAccessObjectFactory().getDataObjectAO(connection.irodsAccount);
            AvuData avuMetaData = new AvuData("cwl", commandOptions.cwl, "waiting");
            dataObjectAO.setAVUMetadata(destFile.getAbsolutePath(), avuMetaData);

            // Start kubernetes?
            // CommandOptionsKubernetes commandOptionsKubernetes = new CommandOptionsKubernetes();

        }
    }
}
