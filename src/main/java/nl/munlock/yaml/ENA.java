package nl.munlock.yaml;

import com.esotericsoftware.yamlbeans.YamlConfig;
import com.esotericsoftware.yamlbeans.YamlWriter;
import nl.munlock.Generic;
import nl.munlock.irods.Connection;
import nl.munlock.irods.Search;
import nl.munlock.kubernetes.Kubernetes;
import nl.munlock.objects.WorkflowSAPP;
import nl.munlock.options.kubernetes.CommandOptionsKubernetes;
import nl.munlock.options.workflow.CommandOptionsSapp;
import org.apache.log4j.Logger;
import org.irods.jargon.core.exception.JargonException;
import org.irods.jargon.core.packinstr.TransferOptions;
import org.irods.jargon.core.pub.DataObjectAO;
import org.irods.jargon.core.pub.DataTransferOperations;
import org.irods.jargon.core.pub.domain.AvuData;
import org.irods.jargon.core.pub.io.IRODSFile;
import org.irods.jargon.core.query.JargonQueryException;
import org.irods.jargon.core.transfer.DefaultTransferControlBlock;
import org.irods.jargon.core.transfer.TransferControlBlock;

import java.io.*;
import java.net.URL;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Scanner;

import static com.esotericsoftware.yamlbeans.YamlConfig.Quote.NONE;

public class ENA {
    private static HashMap<Integer, Integer> taxon2codon = new HashMap<>();
    private static final Logger log = Generic.getLogger(Yaml.class, Yaml.debug);
    private static HashMap<String, Integer> gca2taxon = new HashMap<>();


    public static void generateSAPPWorkflows(CommandOptionsSapp commandOptionsSapp, Connection connection) throws Exception {
        // Upload function
        DataTransferOperations dataTransferOperationsAO = connection.irodsFileSystem.getIRODSAccessObjectFactory().getDataTransferOperations(connection.irodsAccount);

        // Obtain taxon - codon mapping
        getTaxonCodons();

        // Obtain EMBL files
        String path = "/unlock/references/genomes/";
        IRODSFile irodsCollection = connection.fileFactory.instanceIRODSFile(path);
        for (File folder : irodsCollection.listFiles()) {
            if (!folder.getName().startsWith("GCA_")) continue;
            IRODSFile irodsCollectionLevel1 = connection.fileFactory.instanceIRODSFile(folder.getAbsolutePath());
            for (File folder2 : irodsCollectionLevel1.listFiles()) {
                log.info("Processing " + folder2.getName());
                HashSet<String> embls = Search.getAllFiles(connection, folder2.getAbsolutePath() + "%", "GCA_%embl.gz");
                HashSet<String> hdts = Search.getAllFiles(connection, folder2.getAbsolutePath() + "%", "GCA_%hdt.gz");

                for (String embl : embls) {
                    log.info("Processing " + embl);
                    String hdt = embl.replaceAll(".embl.gz", ".hdt.gz");
                    if (hdts.contains(hdt)) {
                        log.info("Already annotated " + hdt);
                        continue;
                    }
                    // Check if hdt exists
                    long size = connection.fileFactory.instanceIRODSFile(embl).length();
                    // Empty downloads
                    if (size < 50) continue;

                    String gca = embl.replaceAll(".[0-9]+.embl.gz", "").replaceAll(".*/","");
                    if (!gca2taxon.containsKey(gca)) {
                        log.info("No entry found for " + gca);
                        continue;
                    }

                    Integer taxon = gca2taxon.get(gca);
                    Integer codon = taxon2codon.get(taxon);

                    if (codon == null || codon != 11 && codon != 4) {
                        log.info("TODO implement lineage check, is this a bacterium?");
                        continue;
                    }

                    // Set workflow file
                    WorkflowSAPP workflow = new WorkflowSAPP();
                    workflow.memory = commandOptionsSapp.memory;
                    workflow.setThreads(commandOptionsSapp.threads);
                    workflow.setProvenance(false);
                    workflow.setEmbl(embl);

                    workflow.setCodon(codon);
                    workflow.setDestination(connection.fileFactory.instanceIRODSFile(embl).getParentFile().getPath());
                    workflow.setIdentifier(connection.fileFactory.instanceIRODSFile(embl).getParentFile().getName());

                    // Upload workflow file
                    YamlConfig config = new YamlConfig();
                    config.writeConfig.setQuoteChar(NONE);
                    String yamlFileName = commandOptionsSapp.wid + ".yaml";
                    YamlWriter writer = new YamlWriter(new FileWriter(yamlFileName), config);
                    writer.write(workflow);
                    writer.close();

                    // Fix Clazz > Class
                    Workflow.fixClazz(yamlFileName);
                    Workflow.fixComments(yamlFileName);
                    Workflow.fixSample(yamlFileName);

                    // Upload
                    String irodsPath = new File(embl).getParentFile() + "/" + commandOptionsSapp.wid + ".yaml";
                    IRODSFile destFile = connection.fileFactory.instanceIRODSFile(irodsPath);

                    TransferOptions transferOptions = dataTransferOperationsAO.buildTransferOptionsBasedOnJargonProperties();
                    transferOptions.setForceOption(TransferOptions.ForceOption.USE_FORCE);
                    TransferControlBlock transferControlBlock = DefaultTransferControlBlock.instance();
                    transferControlBlock.setTransferOptions(transferOptions);

                    dataTransferOperationsAO.putOperation(new File(yamlFileName), destFile, null, transferControlBlock);

                    // Add metadata tag...
                    DataObjectAO dataObjectAO = connection.irodsFileSystem.getIRODSAccessObjectFactory().getDataObjectAO(connection.irodsAccount);
                    AvuData avuMetaData = new AvuData("cwl", "/unlock/infrastructure/cwl/workflows/" + commandOptionsSapp.cwl, "waiting");
                    dataObjectAO.setAVUMetadata(destFile.getAbsolutePath(), avuMetaData);

                    // Kubernetes start
                    Kubernetes.yamls.add(irodsPath);
                    Generic.downloadFile(connection, irodsPath);
                    try {
                        CommandOptionsKubernetes commandOptionsKubernetes = new CommandOptionsKubernetes();
                        if (commandOptionsSapp.node != null)
                            commandOptionsKubernetes.node = commandOptionsSapp.node;
                        commandOptionsKubernetes.limit = commandOptionsSapp.limit;
                        Kubernetes.createJobs(commandOptionsKubernetes, connection);
                    } catch (IOException | JargonQueryException | JargonException | InterruptedException e) {
                        e.printStackTrace();
                    }
                    Kubernetes.yamls.clear();
                }
            }
        }
    }

    private static void getTaxonCodons() throws FileNotFoundException {

        // Tax2 codon lookup
        if (!new File("taxonomy.dat").exists())
            downloadURL("http://ftp.ebi.ac.uk/pub/databases/taxonomy/taxonomy.dat", "taxonomy.dat");

        Scanner scanner = new Scanner(new File("taxonomy.dat"));
        taxon2codon = new HashMap<>();
        Integer id = -1;
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();

            if (line.startsWith("//"))
                id = -1;

            if (line.startsWith("ID         ")) {
                id = Integer.valueOf(line.split(": ")[1]);
            }
            if (line.startsWith("GC ID     ")) {
                Integer table = Integer.valueOf(line.split(": ")[1]);
                taxon2codon.put(id, table);
            }
        }
        scanner.close();
        scanner = new Scanner(new File("taxonLookup.txt"));
        // Skip header
        scanner.nextLine();
        while (scanner.hasNextLine()) {
            String[] line = scanner.nextLine().strip().split("\t");
            int taxid = Integer.parseInt(line[0]);
            String gca = line[1];
            gca2taxon.put(gca, taxid);
        }
    }

    private static void downloadURL(String url, String path) {
        System.err.println(url + " to " + path);
        try (BufferedInputStream inputStream = new BufferedInputStream(new URL(url).openStream());
             FileOutputStream fileOS = new FileOutputStream(path)) {
            byte data[] = new byte[1024];
            int byteContent;
            while ((byteContent = inputStream.read(data, 0, 1024)) != -1) {
                fileOS.write(data, 0, byteContent);
            }
        } catch (IOException e) {
            // handles IO exceptions
            System.err.println(e.getMessage());
        }
    }
}
