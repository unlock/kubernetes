package nl.munlock.yaml;

import com.esotericsoftware.yamlbeans.YamlWriter;
import nl.munlock.Generic;
import nl.munlock.irods.Connection;
import nl.munlock.objects.WorkflowRNASeq;
import nl.munlock.options.workflow.CommandOptionsRNASeq;
import org.apache.log4j.Logger;
import org.irods.jargon.core.pub.DataObjectAO;
import org.irods.jargon.core.pub.DataTransferOperations;
import org.irods.jargon.core.pub.domain.AvuData;
import org.irods.jargon.core.pub.io.IRODSFile;
import org.jermontology.ontology.JERMOntology.domain.Assay;
import org.jermontology.ontology.JERMOntology.domain.Data_sample;

import java.io.File;
import java.io.FileWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RNASeq {
    private static final Logger log = Generic.getLogger(RNASeq.class, Yaml.debug);

    public static void generateRNASeqWorkflow(CommandOptionsRNASeq commandOptions, Connection connection, ArrayList<Assay> assays) throws Exception {
        DataTransferOperations dataTransferOperationsAO = connection.irodsFileSystem.getIRODSAccessObjectFactory().getDataTransferOperations(connection.irodsAccount);

        log.info("Processing: " + assays.size());
        for (Assay assay : assays) {
            // Create 2 workflows 1 for workflow_RNAseq_Spliced.cwl and another for workflow_RNAseq_NonSpliced.cwl

            // Fill object
            WorkflowRNASeq workflow = new WorkflowRNASeq();

            workflow.memory = commandOptions.memory;
            workflow.threads = Math.round(commandOptions.threads);

            log.error("TODO SET REFERENCE");
            // workflow.setReference_db(assay.getReference().getFilePath());

            // The parent of unprocessed
            String destination = assay.getLogicalPath().replaceAll("/Unprocessed.*", "/processed/") + "/" +  commandOptions.wid;

            workflow.setDestination(destination);

            List<? extends Data_sample> data_samples = assay.getAllHasPart();
            ArrayList<String> files = new ArrayList<>();
            for (Data_sample data_sample : data_samples) {
                files.add(new URL(data_sample.getContentUrl()).getPath());
            }

            if (files.size() == 0) {
                throw new Exception("No files found");
            } else if (files.size() == 1) {
                log.error("Single end files not supported");
            } else if (files.size() > 2 && files.size() % 2 != 0) {
                throw new Exception("Too many uneven number of files detected");
            } else if (files.size() == 2 || (files.size() > 2 && files.size() % 2 == 0)) {
                log.info("Paired data detected");

                Collections.sort(files);
                for (int i = 0; i < files.size(); i++) {
                    // Skipp all odd values 1,3,5...
                    if (!(i % 2 == 0)) {
                        continue;
                    }
                    workflow.forward_reads.add(files.get(i));
                    workflow.reverse_reads.add(files.get(i+1));
                }

                // Save to nl.wur.ssb.yaml format
                String yamlName;
                if (commandOptions.wid.length() == 0) {
                    // Generate own nl.wur.ssb.yaml name...
                    yamlName = assay.getIdentifier().replaceAll("\\.", "_") + ".yaml";
                } else {
                    yamlName = commandOptions.wid + ".yaml";
                }

                YamlWriter writer = new YamlWriter(new FileWriter(yamlName));
                writer.write(workflow);
                writer.close();

                // Fix Clazz > Class
                Workflow.fixClazz(yamlName);

                // Save to iRODS
                IRODSFile destFile = connection.fileFactory.instanceIRODSFile(assay.getLogicalPath() + "/" + yamlName);

                if (destFile.exists()) {
                    if (commandOptions.overwrite) {
                        log.info("Deleting remote file as destination folder does not exists and nl.wur.ssb.yaml information might have changed");
                        destFile.delete();
                    } else {
                        log.info("Destination file already exists " + destFile);
                    }
                }

                log.info("Saving nl.wur.ssb.yaml file to " + destFile);

                dataTransferOperationsAO.putOperation(new File(yamlName), destFile, null, null);

                // Add metadata tag...
                DataObjectAO dataObjectAO = connection.irodsFileSystem.getIRODSAccessObjectFactory().getDataObjectAO(connection.irodsAccount);
                AvuData avuMetaData = new AvuData("cwl", commandOptions.cwl, "waiting");
                dataObjectAO.setAVUMetadata(destFile.getAbsolutePath(), avuMetaData);
            }
        }
    }
}
