package nl.munlock.yaml;

import com.esotericsoftware.yamlbeans.YamlWriter;
import nl.munlock.Generic;
import nl.munlock.irods.Connection;
import nl.munlock.objects.WorkflowQuality;
import nl.munlock.options.workflow.CommandOptionsQuality;
import org.apache.log4j.Logger;
import org.irods.jargon.core.pub.DataObjectAO;
import org.irods.jargon.core.pub.DataTransferOperations;
import org.irods.jargon.core.pub.domain.AvuData;
import org.irods.jargon.core.pub.io.IRODSFile;
import org.jermontology.ontology.JERMOntology.domain.Assay;
import org.jermontology.ontology.JERMOntology.domain.Data_sample;

import java.io.File;
import java.io.FileWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Quality {
    private static final Logger log = Generic.getLogger(Quality.class, Yaml.debug);

    public static void generateQualityWorkflow(CommandOptionsQuality commandOptions, Connection connection, ArrayList<Assay> assays) throws Exception {
        DataTransferOperations dataTransferOperationsAO = connection.irodsFileSystem.getIRODSAccessObjectFactory().getDataTransferOperations(connection.irodsAccount);
        // Check filter references
        ArrayList<String> filterPaths = nl.munlock.irods.Generic.prepareFilter(connection, commandOptions.filter_references);

        for (Assay assay : assays) {
            log.info("Parsing " + assay.getClassTypeIri() + " " + assay.getIdentifier());

            // Fill object
            WorkflowQuality workflow = new WorkflowQuality();
            workflow.memory = commandOptions.memory;
            workflow.threads = Math.round(commandOptions.threads);
            workflow.setKraken_database(commandOptions.kraken_database);
            workflow.identifier = commandOptions.wid.toLowerCase() + "_" + assay.getIdentifier();
            workflow.setFilter_references(filterPaths);
            workflow.filter_rrna = commandOptions.rrna;
            workflow.run_filter_references = commandOptions.run_filter_references;
            // The parent of unprocessed
            String destination = assay.getLogicalPath().replaceAll("/Unprocessed.*", "/processed/") + "/" + commandOptions.wid;

            workflow.setDestination(destination);

            List<? extends Data_sample> data_samples = assay.getAllHasPart();
            ArrayList<String> files = new ArrayList<>();
            for (Data_sample data_sample : data_samples) {
                files.add(new URL(data_sample.getContentUrl()).getPath());
            }

            if (files.size() == 0) continue;

            ArrayList<String> forwardReads = new ArrayList<>();
            ArrayList<String> reverseReads = new ArrayList<>();

            // Additional type check
            if (assay.getAllAdditionalType().size() > 1) {
                throw new Exception("More than one additional type detected, work in progress");
            }

            String cwl = null;
            for (String type : assay.getAllAdditionalType()) {
                // Set CWL for nanopore
                if (type.endsWith("Nanopore")) {
                    cwl = "/unlock/infrastructure/cwl/workflows/workflow_nanopore_quality.cwl";
                    for (Data_sample data_sample : assay.getAllHasPart()) {
                        workflow.nanopore_fastq_files.add(new URL(data_sample.getContentUrl()).getPath());
                    }

                } else if (type.endsWith("Illumina")) {

                    cwl = "/unlock/infrastructure/cwl/workflows/workflow_illumina_quality.cwl";
                    Collections.sort(files);
                    for (int i = 0; i < files.size(); i++) {
                        // Skipp all odd values 1,3,5...
                        if (!(i % 2 == 0)) {
                            continue;
                        }
                        forwardReads.add(files.get(i));
                        reverseReads.add(files.get(i+1));
                    }
                    for (String filePath : forwardReads) {
                        workflow.addIllumina_forward_reads(filePath);
                    }

                    for (String filePath : reverseReads) {
                        workflow.addIllumina_reverse_reads(filePath);
                    }
                } else if (type.endsWith("Pacbio")) {
                    cwl = "/unlock/infrastructure/cwl/workflows/workflow_nanopore_quality.cwl";
                    for (Data_sample data_sample : assay.getAllHasPart()) {
                        workflow.pacbio_reads.add(new URL(data_sample.getContentUrl()).getPath());
                    }
                } else {
                    System.err.println("Not done yet " + type);
                }
            }

            // Save to nl.wur.ssb.yaml format
            String yamlName;
            if (commandOptions.wid.length() == 0) {
                // Generate own nl.wur.ssb.yaml name...
                yamlName = assay.getIdentifier().replaceAll("\\.", "_") + ".yaml";
            } else {
                yamlName = commandOptions.wid + ".yaml";
            }

            // Save to nl.wur.ssb.yaml format
            YamlWriter writer = new YamlWriter(new FileWriter(yamlName));
            writer.write(workflow);
            writer.close();

            // Fix Clazz > Class
            Workflow.fixClazz(yamlName);

            // Save to iRODS
            IRODSFile destFile = connection.fileFactory.instanceIRODSFile(assay.getLogicalPath() + "/" + yamlName);

            // Skip empty cwls
            if (cwl == null) {
                continue;
            }

            log.info("Uploading " + destFile);

            if (destFile.exists()) destFile.delete();

            dataTransferOperationsAO.putOperation(new File(yamlName), destFile, null, null);

            // Add metadata tag...
            DataObjectAO dataObjectAO = connection.irodsFileSystem.getIRODSAccessObjectFactory().getDataObjectAO(connection.irodsAccount);
            AvuData avuMetaData = new AvuData("cwl", cwl, "waiting");
            dataObjectAO.setAVUMetadata(destFile.getAbsolutePath(), avuMetaData);
        }
    }
}
