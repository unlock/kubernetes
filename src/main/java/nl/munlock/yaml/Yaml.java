package nl.munlock.yaml;

import nl.munlock.Generic;
import nl.munlock.irods.Connection;
import nl.munlock.irods.Search;
import nl.munlock.options.workflow.*;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.ext.com.google.common.io.Files;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.jermontology.ontology.JERMOntology.domain.Assay;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/*
 * Application to generate YAML files used in iRODS for the CWL runners
 * Currently the following workflows are in place:
 *  workflow_quality.cwl
 *  workflow_ngtax.cwl
 */

public class Yaml {
    public static final boolean debug = false;
    private static final Logger log = Generic.getLogger(Yaml.class, Yaml.debug);

    /**
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        Logger.getLogger("org").setLevel(Level.OFF);

        CommandOptionsYAML commandOptions = new CommandOptionsYAML(args);
        Connection connection = new Connection(commandOptions);

        log.info("Logged in as " + connection.irodsAccount.getUserName());

        // Generate specific workflows
        if (commandOptions.cwl.endsWith("quality.cwl")) {
            File domainDirectory = Files.createTempDir();
            Domain domain = new Domain("file://" + domainDirectory);
            log.info("Running quality analysis");
            ArrayList<Assay> assays = Search.getAssaysFromRDF(commandOptions, connection, domain);
            log.info("Obtained " + assays.size() + " assays");
            CommandOptionsQuality commandOptionsQuality = new CommandOptionsQuality(args);
            Quality.generateQualityWorkflow(commandOptionsQuality, connection, assays);
        } else if (commandOptions.cwl.endsWith("multiqc.cwl")) {
            CommandOptionsMultiQC commandOptionsMultiQC = new CommandOptionsMultiQC(args);
            Reporting.generateMultiQCReport(commandOptionsMultiQC, connection);
        } else if (commandOptions.cwl.matches("workflow_ngtax.cwl") || commandOptions.cwl.matches("workflow_ngtax_picrust2.cwl")) {
            log.info("Running ngtax analysis");
            File domainDirectory = Files.createTempDir();
            Domain domain = new Domain("file://"+domainDirectory);

            ArrayList<Assay> assays = Search.getAssaysFromRDF(commandOptions, connection, domain);
            CommandOptionsNGTAX commandOptionsNGTAX = new CommandOptionsNGTAX(args);
            // Generate MOCK ngtax
            if (commandOptionsNGTAX.mock) {
                NGTax.generateMockWorkflow(commandOptionsNGTAX, connection, domain, 3);
                NGTax.generateMockWorkflow(commandOptionsNGTAX, connection, domain, 4);
            }
            // Execute NGTax workflow
             NGTax.generateNGTAXWorkflow(commandOptionsNGTAX, connection, assays);
        } else if (commandOptions.cwl.matches("workflow_spades.cwl")) {
            log.info("Running spades assembly");
            ArrayList<Domain> domains = Search.getProjectFromRDF(commandOptions, connection);
            CommandOptionsSpades commandOptionsSpades = new CommandOptionsSpades(args);
            for (Domain domain : domains) {
                SPADES.generateSpadesWorkflow(commandOptionsSpades, connection, domain);
            }
        } else if (commandOptions.cwl.matches("workflow_nanopore.cwl") || commandOptions.cwl.matches("workflow_nanopore_assembly.cwl")) {
            log.info("Running nanopore basecalling and assembly");
            ArrayList<Domain> domains = Search.getProjectFromRDF(commandOptions, connection);
            CommandOptionsNanopore commandOptionsNanopore = new CommandOptionsNanopore(args);
            for (Domain domain : domains) {
                Nanopore.generateNanoporeWorkflow(commandOptionsNanopore, connection, domain);
            }
        } else if (commandOptions.cwl.matches("workflow_metagenomics_assembly.cwl") || commandOptions.cwl.matches("workflow_metagenomics.cwl") || commandOptions.cwl.matches("workflow_metagenomics_read_annotation.cwl")) {
            log.info("Running metagenomics workflow");
            ArrayList<Domain> domains = Search.getProjectFromRDF(commandOptions, connection);
            CommandOptionsMetagenomics commandOptionsMetagenomics = new CommandOptionsMetagenomics(args);
            for (Domain domain : domains) {
                MetaGenomics.generateMetaGenomicsWorkflow(commandOptionsMetagenomics, connection, domain);
            }
        } else if (commandOptions.cwl.matches("workflow_RNAseq_NonSpliced.cwl") || commandOptions.cwl.matches("workflow_RNAseq_Spliced.cwl")) {
            log.info("Running rnaseq workflow");
            File domainDirectory = Files.createTempDir();
            Domain domain = new Domain("file://"+domainDirectory);

            ArrayList<Assay> assays = Search.getAssaysFromRDF(commandOptions, connection, domain);
            CommandOptionsRNASeq commandOptionsRNAseq = new CommandOptionsRNASeq(args);
            RNASeq.generateRNASeqWorkflow(commandOptionsRNAseq, connection, assays);
        } else if (commandOptions.cwl.matches("workflow_indexer.cwl")) {
            log.info("Running index workflow");
            CommandOptionsIndexer commandOptionsIndexer = new CommandOptionsIndexer(args);
            Index.generateIndexWorkflow(commandOptionsIndexer, connection);
        } else if (commandOptions.cwl.matches("workflow_sapp_microbes.cwl")) {
            CommandOptionsSapp commandOptionsSapp = new CommandOptionsSapp(args);
            ENA.generateSAPPWorkflows(commandOptionsSapp, connection);
        } else if (commandOptions.cwl.matches("workflow_demultiplexing.cwl")) {
            CommandOptionsDemultiplexing commandOptionsDemultiplexing = new CommandOptionsDemultiplexing(args);
            if (commandOptionsDemultiplexing.investigation == null) {
                throw new Exception("Investigation is obligatory due to demultiplexing complexity");
            }
            log.info("Demultiplexing for " + commandOptionsDemultiplexing.project + " " + commandOptionsDemultiplexing.investigation);
            String folderQuery = "/" + connection.irodsAccount.getZone() + "/landingzone/projects/" + commandOptions.project + "/" + commandOptionsDemultiplexing.investigation;
            ArrayList<Domain> domains = Search.getProjectFromRDF(connection, folderQuery);
            for (Domain domain : domains) {
                NGTax.generateDemultiplexWorkflow(commandOptionsDemultiplexing, connection, domain);
            }
        } else if (commandOptions.cwl.contains("irods_hdt.cwl")) {
            log.info("Generating HDT workflow files");
            CommandOptionsHDT commandOptionsHDT = new CommandOptionsHDT(args);
            Set<String> folders = Search.getOUFromiRODS(connection, commandOptionsHDT);
            // For each folder obtain last modified TTL file and last modified HDT file
            Set<String> analysis = new HashSet<>();
            int count = 0;
            for (String folder : folders) {
                count++;
                if (count > 10) {
                    connection.reconnect();
                }
                boolean check = Search.checkTimeStampsHDT(connection, commandOptionsHDT, folder);
                if (check) {
                    analysis.add(folder);
                    HDT.generateHDTWorkflow(commandOptionsHDT, connection, analysis);
                    analysis.removeAll(analysis);
                }
            }
            log.info("Found " + folders.size() + " observational units");
            // generate the workflows
            HDT.generateHDTWorkflow(commandOptionsHDT, connection, analysis);
        } else if (commandOptions.cwl.contains("workflow_toHDT_compression.cwl")) {
            CommandOptionsToHDT commandOptionsToHDT = new CommandOptionsToHDT(args);
            toHDT.generateHDTWorkflow(commandOptionsToHDT, connection);
        } else {
            throw new Exception("Unknown CWL provided " + commandOptions.cwl);
        }
    }
}
