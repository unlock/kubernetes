package nl.munlock.yaml;

import nl.munlock.Generic;
import nl.munlock.irods.Connection;
import nl.munlock.options.workflow.CommandOptionsKraken;
import org.apache.log4j.Logger;
import org.irods.jargon.core.exception.JargonException;
import org.irods.jargon.core.query.GenQueryBuilderException;
import org.irods.jargon.core.query.JargonQueryException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class Kraken {
    private static final Logger log = Generic.getLogger(Kraken.class, Yaml.debug);


    public static void generateKrakenDatabase(CommandOptionsKraken commandOptions, Connection connection) throws JargonQueryException, JargonException, GenQueryBuilderException, IOException {
        ArrayList<String> references = nl.munlock.irods.Generic.prepareFilter(connection, commandOptions.references);
        System.err.println("DBNAME=KRAKEN_COCULTURE");
        System.err.println("apt install rsync");
        System.err.println("/unlock/infrastructure/binaries/kraken2/kraken2-v2.1.2/kraken2-build --download-taxonomy --db $DBNAME");
        for (String reference : references) {
            System.err.println("iget " + reference);
            System.err.println("/unlock/infrastructure/binaries/kraken2/kraken2-v2.1.2/kraken2-build --add-to-library "+new File(reference).getName()+" --db $DBNAME");
        }
        System.err.println("kraken2-build --build --db $DBNAME");

//        DataTransferOperations dataTransferOperationsAO = connection.irodsFileSystem.getIRODSAccessObjectFactory().getDataTransferOperations(connection.irodsAccount);
//        String cwl = "/unlock/infrastructure/cwl/multiqc/multiqc.cwl";
//
//        // Obtain all files in a study
//        String folderQuery = "/" + connection.irodsAccount.getZone() + "/projects/" + commandOptions.project + "/" + commandOptions.investigation + "%/" + commandOptions.study + "%";
//        HashSet<String> files = getAllFiles(connection, folderQuery, "%");
//
//        // Fill object
//        WorkflowMultiqc workflow = new WorkflowMultiqc();
//        workflow.memory = commandOptions.memory;
//        workflow.threads = Math.round(commandOptions.threads);
//        workflow.identifier = commandOptions.wid.toLowerCase() + "_" + commandOptions.study;
//
//        // The parent of unprocessed
//        String folder = "/" + connection.irodsAccount.getZone() + "/projects/" + commandOptions.project + "/" + commandOptions.investigation + "/" + commandOptions.study;
//        workflow.setFolder(folder);
//        workflow.destination = folder + "/report/";
//
//
//        if (workflow.destination.contains("%"))
//            throw new JargonException("Destination contains %: " + workflow.destination);
//
//        for (String file : files) {
//            if (file.contains("/unprocessed/"))
//                continue;
//            if (file.endsWith(".ttl"))
//                continue;
//            if (file.endsWith(".fq.gz"))
//                continue;
//            if (file.endsWith(".fastq.gz"))
//                continue;
//            System.err.println(file);
//            workflow.addIRODS(file);
////            if (workflow.getIRODS().keySet().size() > 10) {
////                break;
////            }
//        }
//
//        // Save to nl.wur.ssb.yaml format
//        String yamlName = commandOptions.study + "_multiqc.yaml";
//
//        // Save to nl.wur.ssb.yaml format
//        YamlWriter writer = new YamlWriter(new FileWriter(yamlName));
//        writer.write(workflow);
//        writer.close();
//
//        // Fix Clazz > Class
//        Workflow.fixClazz(yamlName);
//
//        // Save to iRODS
//        IRODSFile destFile = connection.fileFactory.instanceIRODSFile(folder + "/" + yamlName);
//
//        log.info("Uploading " + destFile);
//
//        if (destFile.exists()) destFile.delete();
//
//        dataTransferOperationsAO.putOperation(new File(yamlName), destFile, null, null);
//
//        // Add metadata tag...
//        DataObjectAO dataObjectAO = connection.irodsFileSystem.getIRODSAccessObjectFactory().getDataObjectAO(connection.irodsAccount);
//        AvuData avuMetaData = new AvuData("cwl", cwl, "waiting");
//        dataObjectAO.setAVUMetadata(destFile.getAbsolutePath(), avuMetaData);
    }
}
