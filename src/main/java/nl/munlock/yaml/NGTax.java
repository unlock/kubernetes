package nl.munlock.yaml;

import com.esotericsoftware.yamlbeans.YamlConfig;
import com.esotericsoftware.yamlbeans.YamlWriter;
import nl.munlock.Generic;
import nl.munlock.irods.Connection;
import nl.munlock.objects.WorkflowDemultiplex;
import nl.munlock.objects.WorkflowNgtax;
import nl.munlock.ontology.domain.*;
import nl.munlock.options.workflow.CommandOptionsDemultiplexing;
import nl.munlock.options.workflow.CommandOptionsNGTAX;
import nl.wur.ssb.RDFSimpleCon.ResultLine;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.jena.rdf.model.NodeIterator;
import org.apache.log4j.Logger;
import org.irods.jargon.core.exception.JargonException;
import org.irods.jargon.core.pub.CollectionAO;
import org.irods.jargon.core.pub.DataObjectAO;
import org.irods.jargon.core.pub.DataTransferOperations;
import org.irods.jargon.core.pub.IRODSGenQueryExecutor;
import org.irods.jargon.core.pub.domain.AvuData;
import org.irods.jargon.core.pub.io.IRODSFile;
import org.irods.jargon.core.query.*;
import org.jermontology.ontology.JERMOntology.domain.Assay;
import org.jermontology.ontology.JERMOntology.domain.Data_sample;
import org.jermontology.ontology.JERMOntology.domain.Sample;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.net.URL;
import java.util.*;
import java.util.concurrent.TimeUnit;

import static com.esotericsoftware.yamlbeans.YamlConfig.Quote.NONE;
import static nl.munlock.Generic.*;
import static nl.munlock.yaml.Index.setTransferControlBlock;

public class NGTax {
    private static final Logger log = Generic.getLogger(NGTax.class, Yaml.debug);
    public static void generateNGTAXWorkflow(CommandOptionsNGTAX commandOptions, Connection connection, ArrayList<Assay> assays) throws Exception {
        DataTransferOperations dataTransferOperationsAO = connection.irodsFileSystem.getIRODSAccessObjectFactory().getDataTransferOperations(connection.irodsAccount);

        int assayCount = 1;
        for (Assay assay : assays) {
            log.info("Processing: " + assayCount + " of " + assays.size());
            assayCount = assayCount + 1;
            String yamlFileName;

            // Only amplicon types allowed
            boolean pass = false;
            for (String type : assay.getAllAdditionalType()) {
                if (type.contains("Amplicon"))
                    pass = true;
            }

            if (!pass) {
                log.info("Additional type of " + assay.getResource().getURI() + " does not match Amplicon");
                continue;
            } else {
                log.info(assay.getResource().getURI());
                log.info(assay.getIdentifier());
                log.info("Analysing " + assay.getLogicalPath());
            }

            // Fill object
            WorkflowNgtax workflow = new WorkflowNgtax();

            workflow.memory = commandOptions.memory;
            workflow.setThreads(commandOptions.threads);
            workflow.setProvenance(true);
            // workflow.setConda("picrust2");
            workflow.setMetadata(assay.getLogicalPath() + "/" + assay.getIdentifier() + ".ttl");

            // Silva database TODO should this not be obtained from the CWL file?
            if (commandOptions.reference_db.length() > 0) {
                workflow.setReference_db(commandOptions.reference_db);
            }

            // Primer settings
            String forwardPrimer = Generic.getProperty(assay.getResource().getModel(), assay.getResource().getURI(), "http://m-unlock.nl/ontology/forwardPrimer").next().toString();
            String reversePrimer = Generic.getProperty(assay.getResource().getModel(), assay.getResource().getURI(), "http://m-unlock.nl/ontology/reversePrimer").next().toString();

            workflow.setForward_primer(forwardPrimer);
            workflow.setReverse_primer(reversePrimer);

            // Set the read length
            workflow.setFor_read_len(commandOptions.read_len);
            workflow.setRev_read_len(commandOptions.read_len);

            // Set primers removed? default for ngtax demultiplexed files
            workflow.setPrimersRemoved(commandOptions.primersRemoved);

            // Sets minimumThreshold
            workflow.setMinimum_threshold(commandOptions.minimumThreshold);


            String target_subfragment = Generic.getProperty(assay.getResource().getModel(), assay.getResource().getURI(), "http://m-unlock.nl/ontology/target_subfragment").next().toString();
            if (target_subfragment == null) {
                throw new Exception("No target subfragment found for " + assay.getLogicalPath());
            }

            workflow.setFragment(target_subfragment);

            // Identifier
            workflow.setSample(assay.getIdentifier());

            // The parent of unprocessed
            String destination = assay.getLogicalPath().replaceAll("/Unprocessed.*", "/processed/") + "/" + commandOptions.wid + "_" + commandOptions.read_len;

            workflow.setDestination(destination);

            ArrayList<Data_sample> files = new ArrayList<>();
            for (Data_sample data_sample : assay.getAllHasPart()) {
                NodeIterator nodeIterator = Generic.getProperty(assay.getResource().getModel(), data_sample.getResource().getURI(), "http://schema.org/description");
                boolean library = false;
                while (nodeIterator.hasNext()) {
                    String element = nodeIterator.next().asLiteral().getString();
                    if (element.contains("Library")) {
                        library = true;
                    }
                }
                if (!library) {
                    files.add(data_sample);
                }
            }
            
            log.debug("Read length set to " + commandOptions.read_len);

            // Set yaml filename
            if (commandOptions.read_len > 0) {
                yamlFileName = commandOptions.wid + "_" + commandOptions.read_len + ".yaml";
            } else {
                yamlFileName = commandOptions.wid + "_" + workflow.getFor_read_len() + ".yaml";
            }

            if (files.size() == 0) {
                throw new Exception("No files found");
            } else if (files.size() == 1) {
                log.debug("Single end files detected");
                if (files.get(0).getClassTypeIri().endsWith("PairedSequenceDataSet")) {
                    log.error("Paired sequence dataset detected but only 1 file was found");
                    log.error("Paired dataset should not be interleaved");
                    continue;
                }
                // IN PROGRESS... mr..DNA -.-
                workflow.addForward_reads(new URL(files.get(0).getContentUrl()).getPath());
                SequenceDataSet sequenceDataSet = (SequenceDataSet) assay.getAllHasPart().get(0);

                // Evaluate if the set read length matches / smaller than the raw read length
                if (commandOptions.read_len == 0 && sequenceDataSet.getReadLength() > 0) {
                    log.info("Obtaining read length information from RDF file");
                    workflow.setFor_read_len(Math.toIntExact(sequenceDataSet.getReadLength()));
                } else if (commandOptions.read_len > 0) {
                    if (commandOptions.read_len > sequenceDataSet.getReadLength() && sequenceDataSet.getReadLength() > 0) {
                        throw new Exception("Read length of " + commandOptions.read_len + " is larger than " + sequenceDataSet.getReadLength());
                    }
                    log.info("Setting read length to " + commandOptions.read_len + " of " + sequenceDataSet.getReadLength());
                    workflow.setFor_read_len(Math.toIntExact(commandOptions.read_len));
                } else {
                    log.error("RDF file does not contain read length information? Please provide read length information");
                }
            } else if (files.size() == 2) {
                log.debug("Paired data detected");
                workflow.setReverse_primer(reversePrimer);

                workflow.addForward_reads(new URL(files.get(0).getContentUrl()).getPath());
                workflow.addReverse_reads(new URL(files.get(1).getContentUrl()).getPath());

                workflow.irods = workflow.getIRODS();
                workflow.setRev_read_len(Math.toIntExact(commandOptions.read_len));
                workflow.setFor_read_len(Math.toIntExact(commandOptions.read_len));
            } else {
                files.forEach(file -> {
                    System.err.println(file.getName());
                    System.err.println(file.getResource().getURI());
                });
                throw new Exception("More than 2 files detected...");
            }

            log.debug("Yaml filename: " + yamlFileName);

            // Save to iRODS
            IRODSFile destFile = connection.fileFactory.instanceIRODSFile(assay.getLogicalPath() + "/" + yamlFileName);

            YamlConfig config = new YamlConfig();
            config.writeConfig.setQuoteChar(NONE);
            YamlWriter writer = new YamlWriter(new FileWriter(yamlFileName), config);

            writer.write(workflow);
            writer.close();

            if (commandOptions.debug) {
                Scanner scanner = new Scanner(new File(yamlFileName));
                while (scanner.hasNextLine()) {
                    System.err.println(scanner.nextLine());
                }
            }

            // Fix Clazz > Class
            Workflow.fixClazz(yamlFileName);
            Workflow.fixComments(yamlFileName);
            Workflow.fixSample(yamlFileName);

            if (!commandOptions.overwrite && destFile.exists()) {
                continue;
            }

            // Generic part here for both single and paired end data
            log.info("Start uploading...");

            dataTransferOperationsAO.putOperation(new File(yamlFileName), destFile, null, setTransferControlBlock(dataTransferOperationsAO, destFile, commandOptions.overwrite));

            // Add metadata tag...
            DataObjectAO dataObjectAO = connection.irodsFileSystem.getIRODSAccessObjectFactory().getDataObjectAO(connection.irodsAccount);
            AvuData avuMetaData = new AvuData("cwl", "/unlock/infrastructure/cwl/workflows/" + commandOptions.cwl, "waiting");
            dataObjectAO.setAVUMetadata(destFile.getAbsolutePath(), avuMetaData);
        }
    }


    public static void generateDemultiplexWorkflow(CommandOptionsDemultiplexing commandOptionsDemultiplexing, Connection connection, Domain domain) throws Exception {
        DataTransferOperations dataTransferOperationsAO = connection.irodsFileSystem.getIRODSAccessObjectFactory().getDataTransferOperations(connection.irodsAccount);

        Iterator<ResultLine> resultLineIterator = domain.getRDFSimpleCon().runQuery("getAssay.txt", true, "http://m-unlock.nl/ontology/AmpliconLibraryAssay").iterator();

        // For each library
        while (resultLineIterator.hasNext()) {
            ResultLine resultLine = resultLineIterator.next();
            AmpliconLibraryAssay ampliconLibraryAssay = domain.make(AmpliconLibraryAssay.class, resultLine.getIRI("assay"));

            ResultLine assayLine = domain.getRDFSimpleCon().runQuery("getAssayFromAmpliconLibrary.txt", true, ampliconLibraryAssay.getResource().getURI()).iterator().next();
            AmpliconAssay ampliconAssay = domain.make(AmpliconAssay.class, assayLine.getIRI("assay"));

            String mappingFile = generateMappingFile(domain, ampliconLibraryAssay, ampliconAssay, commandOptionsDemultiplexing);

            // Find amplicon library
            String path = "/" + connection.irodsAccount.getZone() + "/landingzone/ampliconlibraries";
            HashSet<String> libraryFwdFile = findFile(connection, ampliconLibraryAssay.getAllHasPart().get(0).getName(), path);
            HashSet<String> libraryRevFile = findFile(connection, ampliconLibraryAssay.getAllHasPart().get(1).getName(), path);

            if (libraryFwdFile.size() == 0)
                continue;

            String demultiplexedDestinationFolder = "/" + commandOptionsDemultiplexing.zone + "/landingzone/projects/" + commandOptionsDemultiplexing.project + "/" + commandOptionsDemultiplexing.investigation + "/demultiplexed/";
            // connection.fileFactory.instanceIRODSFile(demultiplexedDestinationFolder).mkdirs();

            // Making sure the parent folder is present
            String mappingFolderDestination = demultiplexedDestinationFolder + "/jobs/";
            IRODSFile destFolder = connection.fileFactory.instanceIRODSFile(mappingFolderDestination);
            destFolder.mkdirs();

            // Creating the file and if exist delete it...?
            String mappingFileDestination = mappingFolderDestination + mappingFile;
            IRODSFile destFile = connection.fileFactory.instanceIRODSFile(mappingFileDestination);
            while (destFile.exists()) {
                destFile.delete();
            }

            // Uploading file to library location or to project?
            dataTransferOperationsAO.putOperation(new File(mappingFile), destFile, null, null);

            // create yaml file
            String fwdPrimer = ampliconAssay.getForwardPrimer();
            String revPrimer = ampliconAssay.getReversePrimer();

            String libFile1 = libraryFwdFile.iterator().next();
            String libFile2 = libraryRevFile.iterator().next();

            WorkflowDemultiplex workflowDemultiplex = new WorkflowDemultiplex();
            workflowDemultiplex.setForward_primer(fwdPrimer);
            workflowDemultiplex.setReverse_primer(revPrimer);
            workflowDemultiplex.addForward_reads(libFile1);
            workflowDemultiplex.addReverse_reads(libFile2);
            workflowDemultiplex.setMappingFile(mappingFileDestination);
            workflowDemultiplex.setDestination(demultiplexedDestinationFolder + "/" + new File(libFile1).getName() + "/" + fwdPrimer + "-" + revPrimer);


            String yamlFileName = mappingFile + ".yaml";

            YamlConfig config = new YamlConfig();
            config.writeConfig.setQuoteChar(NONE);
            YamlWriter writer = new YamlWriter(new FileWriter(yamlFileName), config);

            writer.write(workflowDemultiplex);
            writer.close();

            // Fix Clazz > Class
            Workflow.fixClazz(yamlFileName);
            Workflow.fixComments(yamlFileName);
            Workflow.fixSample(yamlFileName);

            // Upload
            destFile = connection.fileFactory.instanceIRODSFile(demultiplexedDestinationFolder + "/jobs/" + yamlFileName);
            if (destFile.exists()) {
                destFile.delete();
            }
            dataTransferOperationsAO.putOperation(new File(yamlFileName), destFile, null, setTransferControlBlock(dataTransferOperationsAO, destFile, commandOptionsDemultiplexing.overwrite));

            // Add metadata tag...
            DataObjectAO dataObjectAO = connection.irodsFileSystem.getIRODSAccessObjectFactory().getDataObjectAO(connection.irodsAccount);
            AvuData avuMetaData = new AvuData("cwl", "/unlock/infrastructure/cwl/workflows/" + commandOptionsDemultiplexing.cwl, "waiting");
            dataObjectAO.setAVUMetadata(destFile.getAbsolutePath(), avuMetaData);

            log.info("Created mapping and yaml file for " + destFile);
        }
    }

    private static String generateMappingFile(Domain domain, AmpliconLibraryAssay ampliconLibraryAssay, AmpliconAssay ampliconAssayReference, CommandOptionsDemultiplexing commandOptionsDemultiplexing) throws Exception {
        String mappingFile = "mapping_file_" + commandOptionsDemultiplexing.project + "_" + DigestUtils.md5Hex(ampliconAssayReference.getForwardPrimer() + "_" + ampliconAssayReference.getReversePrimer() + "_" + ampliconLibraryAssay.getIdentifier()).toLowerCase() + ".txt";

//        String jobid = yamlFile;
//        if (!yamlFile.matches("(([A-Za-z0-9][-A-Za-z0-9_.]*)?[A-Za-z0-9])?")) {
//            // Md5 of yaml filename
//            jobid = DigestUtils.md5Hex(yamlFile).toLowerCase();
//        }

        PrintWriter printWriter = new PrintWriter(mappingFile);
        printWriter.println("#sampleID\tforwardBarcodeSequence\treverseBarcodeSequence\tLibraryNumber\tDirection\tLibraryName");

        Iterable<ResultLine> ampliconAssays = domain.getRDFSimpleCon().runQuery("getAssayIDsFromLibrary.txt", true, ampliconLibraryAssay.getResource().getURI());

        for (ResultLine assay : ampliconAssays) {
            AmpliconAssay ampliconAssay = domain.make(AmpliconAssay.class, assay.getIRI("assay"));

            if (!fixPrimer(ampliconAssay.getForwardPrimer()).contains(fixPrimer(ampliconAssayReference.getForwardPrimer()))) {
                log.info("Assay from same library with different primer detected " + ampliconAssay.getIdentifier() + " " + fixPrimer(ampliconAssay.getForwardPrimer()) + "-" + fixPrimer(ampliconAssay.getReversePrimer()) + " vs reference " + ampliconAssayReference.getIdentifier() + " " + fixPrimer(ampliconAssayReference.getForwardPrimer()) + "-" + fixPrimer(ampliconAssayReference.getReversePrimer()));
                continue;
            }

            if (fixPrimer(ampliconAssay.getReversePrimer()) != null && !fixPrimer(ampliconAssay.getReversePrimer()).contains(fixPrimer(ampliconAssayReference.getReversePrimer()))) {
                log.info("Assay from same library with different primer detected " + ampliconAssay.getIdentifier() + " vs reference " + ampliconAssayReference.getIdentifier());
                continue;
            }

            String sampleID = ampliconAssay.getIdentifier();
            String fwdBarcode = ampliconAssay.getForwardBarcode();
            String revBarcode = ampliconAssay.getReverseBarcode();
            String libraryNumber = "1";
            String direction = "p";
            String libraryName = ampliconAssay.getLibrary().getAllHasPart().get(0).getName();
            printWriter.println(sampleID + "\t" + fwdBarcode + "\t" + revBarcode + "\t" + libraryNumber + "\t" + direction + "\t" + libraryName);
        }
        printWriter.close();
        return mappingFile;
    }

    public static HashSet<String> findFile(Connection connection, String fileName, String supposedPath) throws GenQueryBuilderException, JargonException { // throws GenQueryBuilderException, JargonQueryException, JargonException {
        IRODSGenQueryBuilder queryBuilder = new IRODSGenQueryBuilder(true, null);

        // Generalised to landingzone folder, check later if its ENA or Project
        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_DATA_NAME, QueryConditionOperators.EQUAL, fileName);
        // Skip files found in trash
        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_COLL_NAME, QueryConditionOperators.NOT_LIKE, "/" + connection.irodsAccount.getZone() + "/trash/%");
        queryBuilder.addSelectAsGenQueryValue(RodsGenQueryEnum.COL_COLL_NAME);
        queryBuilder.addSelectAsGenQueryValue(RodsGenQueryEnum.COL_DATA_NAME);

        // Set limit?
        IRODSGenQueryFromBuilder query = queryBuilder.exportIRODSQueryFromBuilder(2);

        IRODSGenQueryExecutor irodsGenQueryExecutor = connection.accessObjectFactory.getIRODSGenQueryExecutor(connection.irodsAccount);
        IRODSQueryResultSet irodsQueryResultSet;
        try {
            irodsQueryResultSet = irodsGenQueryExecutor.executeIRODSQuery(query, 0);
            TimeUnit.SECONDS.sleep(0);
        } catch (JargonException | InterruptedException | JargonQueryException e) {
            e.printStackTrace();
            return new HashSet<>();
        }

        List<IRODSQueryResultRow> irodsQueryResultSetResults = irodsQueryResultSet.getResults();

        // If no hits are found
        if (irodsQueryResultSetResults.size() == 0) {
            log.error("This file is not found " + fileName + " in " + supposedPath);
            return new HashSet<>();
        } else {
            // if one hit or more is found collect all paths
            HashSet<String> paths = new HashSet<>();
            for (IRODSQueryResultRow irodsQueryResultSetResult : irodsQueryResultSetResults) {
                String path = irodsQueryResultSetResult.getColumn(0) + "/" + irodsQueryResultSetResult.getColumn(1);
                if (path.startsWith(supposedPath)) {
                    paths.add(path);
                } else {
                    System.err.println(path + " not in " + supposedPath + " but still added for processing");
                    paths.add(path);
                }
            }

            // if more than one hit found
            if (paths.size() > 1) {
                // throw new FileIntegrityException("Multiple libraries found with the same name: " + fileName);
                return new HashSet<>();
            }
            return paths;
        }
    }

    public static String fixPrimer(String primer) {
        return primer.replaceAll("B", "[CGT]")
                .replaceAll("D", "[AGT]")
                .replaceAll("H", "[ACT]")
                .replaceAll("K", "[GT]")
                .replaceAll("M", "[AC]")
                .replaceAll("N", "[ACGT]")
                .replaceAll("R", "[AG]")
                .replaceAll("S", "[GC]")
                .replaceAll("V", "[ACG]")
                .replaceAll("W", "[AT]")
                .replaceAll("Y", "[CT]");
    }

    public static void generateMockWorkflow(CommandOptionsNGTAX commandOptions, Connection connection, Domain domain, Integer mockID) throws Exception {
        // Forward primer
        // Reverse primer
        // Database
        DataTransferOperations dataTransferOperationsAO = connection.irodsFileSystem.getIRODSAccessObjectFactory().getDataTransferOperations(connection.irodsAccount);

        // Perform query per study get path and primer set
        Iterator<ResultLine> resultLines = domain.getRDFSimpleCon().runQuery("getAssayPrimers.txt", true).iterator();
        HashMap<String, HashMap<String, String>> studyPrimers = new HashMap<>();
        while (resultLines.hasNext()) {
            ResultLine resultLine = resultLines.next();
            String path = resultLine.getLitString("path").replaceAll("/O_.*", "");
            String fwd = resultLine.getLitString("fwd");
            String rev = resultLine.getLitString("rev");
            String fragment = resultLine.getLitString("subfragment");
            if (!studyPrimers.containsKey(path)) {
                studyPrimers.put(path, new HashMap<>());
            }
            // Primer pair with fragment name
            studyPrimers.get(path).put(fwd + "===" + rev, fragment);
        }

        // Working with a
        for (String study : studyPrimers.keySet()) {
            ArrayList<String> primerList = new ArrayList<>(studyPrimers.get(study).keySet());
            Collections.sort(primerList);
            for (int i = 0; i < primerList.size(); i++) {
                String primerSet = primerList.get(i);
                int counter = i + 1;
                String primerID = primerSet.replaceAll("\\[","_").replaceAll("\\]","_").replaceAll("_+", "_").replaceAll("=+","-");
                String fragment = studyPrimers.get(study).get(primerSet);
                String cleanFragment = fragment.replaceAll(" +","_");
                String refMock = study + "/O_REFMOCK"+mockID+"/amplicon/A_REFMOCK" + mockID + "_" + cleanFragment + "." + counter;
                String refMockID = "REFMOCK" + mockID + "_" + cleanFragment + "." + counter;
                // Create the folder structure
                connection.fileFactory.instanceIRODSFile(refMock).mkdirs();

                // Amplicon assay object
                Domain tempDomain = new Domain("");

                Sample sample = tempDomain.make(Sample.class, "http://"+connection.irodsAccount.getHost() + "/Sample/" + UUID.randomUUID() + "/REFMOCK_" +primerID);
                sample.setSamplePackage(Mixs.Core);
                sample.setName(refMockID);
                sample.setDescription("Reference mock of " + refMockID);
                sample.setIdentifier(refMockID);

                AmpliconAssay ampliconAssay = tempDomain.make(AmpliconAssay.class, "http://"+connection.irodsAccount.getHost() +"/Amplicon/" + UUID.randomUUID().toString() + "/REFMOCK_" + primerID);
                ampliconAssay.setForwardPrimer(primerSet.split("===")[0]);
                ampliconAssay.setReversePrimer(primerSet.split("===")[1]);
                ampliconAssay.setLogicalPath(refMock);
                ampliconAssay.setTarget_subfragment(studyPrimers.get(study).get(primerSet));
                ampliconAssay.setDescription("Reference mock of " + refMockID);
                ampliconAssay.setIdentifier(refMockID);
                ampliconAssay.setName(refMockID);

                String sha256 = getSHA256(connection, "/unlock/references/databases/ngtax/mock"+mockID+".fa");
                SequenceDataSet dataset = tempDomain.make(SequenceDataSet.class, "ni:///sha-256;" + sha256);
                dataset.setBase64(getBase64(connection,"/unlock/references/databases/ngtax/mock"+mockID+".fa"));
                dataset.setName("mock" + mockID + ".fa");
                dataset.setContentUrl("https://" + connection.irodsAccount.getHost() + "/unlock/references/databases/ngtax/mock"+mockID+".fa");
                dataset.setSha256(sha256);
                dataset.setFileFormat(FileType.format_1929);
                // TODO change to Synthetic or Other?
                dataset.setSeqPlatform(SequencingPlatform.Illumina);

                // Get sequence info
                String mockFile = "/unlock/references/databases/ngtax/mock" + mockID + ".fa";
                downloadFile(connection, mockFile);
                Scanner scanner = new Scanner(new File("." + mockFile));
                int reads = 0;
                int bases = 0;
                int readLength = 0;
                while (scanner.hasNextLine()) {
                    String line = scanner.nextLine().strip();
                    if (line.startsWith(">")) {
                        reads = reads + 1;
                    } else {
                        bases = bases + line.length();
                        if (line.length() > readLength)
                            readLength = line.length();
                    }
                }
                dataset.setReadLength((long) readLength);
                dataset.setBases((long) bases);
                dataset.setReads((long) reads);
                ampliconAssay.addHasPart(dataset);

                sample.addHasPart(ampliconAssay);

                tempDomain.save(refMockID + ".ttl");
                IRODSFile destFile = connection.fileFactory.instanceIRODSFile(refMock + "/" + refMockID + ".ttl");

                if (destFile.exists()) {
                    if (!commandOptions.overwrite) {
                        continue;
                    }
                }

                dataTransferOperationsAO.putOperation(new File( refMockID + ".ttl"), destFile, null, setTransferControlBlock(dataTransferOperationsAO, destFile, commandOptions.overwrite));

                // Yaml
                WorkflowNgtax workflowMock = new WorkflowNgtax();

                workflowMock.memory = commandOptions.memory;
                workflowMock.setThreads(commandOptions.threads);
                workflowMock.setDestination(refMock + "/"+refMockID+"_"+commandOptions.read_len+"/");
                workflowMock.setForward_primer(primerSet.split("===")[0]);
                workflowMock.setReverse_primer(primerSet.split("===")[1]);
                workflowMock.setFor_read_len(commandOptions.read_len);
                workflowMock.setRev_read_len(commandOptions.read_len);
                workflowMock.setMinimum_threshold(commandOptions.minimumThreshold);
                workflowMock.setReference_db(commandOptions.reference_db);
                workflowMock.setProvenance(true);
                workflowMock.setSample(refMockID);
                workflowMock.fragment = fragment;
                workflowMock.setMetadata(ampliconAssay.getLogicalPath() + "/" + ampliconAssay.getIdentifier() + ".ttl");

                if (mockID == 3) {
                    workflowMock.setMock3(refMockID);
                } else if (mockID == 4) {
                    workflowMock.setMock4(refMockID);
                } else {
                    throw new Exception("Unknown " + mockID + " detected");
                }

                String yamlFileName = refMockID+"_"+commandOptions.read_len+".yaml";
                YamlConfig config = new YamlConfig();
                config.writeConfig.setQuoteChar(NONE);
                YamlWriter writer = new YamlWriter(new FileWriter(yamlFileName), config);

                writer.write(workflowMock);
                writer.close();

                // Fix Clazz > Class
                Workflow.fixClazz(yamlFileName);
                Workflow.fixComments(yamlFileName);
                Workflow.fixSample(yamlFileName);

                // Folder creation
                destFile = connection.fileFactory.instanceIRODSFile(refMock);

                if (!destFile.exists()) {
                    destFile.mkdirs();
                }

                IRODSFile irodsFile = connection.fileFactory.instanceIRODSFile(study + "/O_REFMOCK" + mockID);
                CollectionAO collectionAO = connection.irodsFileSystem.getIRODSAccessObjectFactory().getCollectionAO(connection.irodsAccount);
                AvuData avuMetaData = new AvuData("type", "ObservationUnit", "");
                collectionAO.setAVUMetadata(irodsFile.getAbsolutePath(), avuMetaData);

                destFile = connection.fileFactory.instanceIRODSFile(refMock + "/" + yamlFileName);

                if (destFile.exists()) {
                    if (!commandOptions.overwrite) {
                        continue;
                    }
                }

                log.debug("Saving nl.wur.ssb.yaml file to " + destFile);
                dataTransferOperationsAO.putOperation(new File(yamlFileName), destFile, null, setTransferControlBlock(dataTransferOperationsAO, destFile, commandOptions.overwrite));

                // Add metadata tag...
                DataObjectAO dataObjectAO = connection.irodsFileSystem.getIRODSAccessObjectFactory().getDataObjectAO(connection.irodsAccount);
                avuMetaData = new AvuData("cwl", "/unlock/infrastructure/cwl/workflows/workflow_mock_ngtax.cwl", "waiting");
                dataObjectAO.setAVUMetadata(destFile.getAbsolutePath(), avuMetaData);

            }
        }
    }
}

