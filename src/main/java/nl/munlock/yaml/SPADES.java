package nl.munlock.yaml;

import com.esotericsoftware.yamlbeans.YamlWriter;
import nl.munlock.Generic;
import nl.munlock.irods.Connection;
import nl.munlock.ontology.domain.PairedSequenceDataSet;
import nl.munlock.ontology.domain.SequenceDataSet;
import nl.munlock.options.workflow.CommandOptionsSpades;
import nl.munlock.objects.WorkflowSpades;
import nl.wur.ssb.RDFSimpleCon.ResultLine;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.log4j.Logger;
import org.irods.jargon.core.pub.DataObjectAO;
import org.irods.jargon.core.pub.DataTransferOperations;
import org.irods.jargon.core.pub.domain.AvuData;
import org.irods.jargon.core.pub.io.IRODSFile;
import org.jermontology.ontology.JERMOntology.domain.Assay;
import org.jermontology.ontology.JERMOntology.domain.Data_sample;
import org.jermontology.ontology.JERMOntology.domain.Sample;
import org.purl.ppeo.PPEO.owl.domain.observation_unit;

import java.io.File;
import java.io.FileWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;

public class SPADES {
    private static final Logger log = Generic.getLogger(SPADES.class, Yaml.debug);

    public static void generateSpadesWorkflow(CommandOptionsSpades commandOptions, Connection connection, Domain domain) throws Exception {
        DataTransferOperations dataTransferOperationsAO = connection.irodsFileSystem.getIRODSAccessObjectFactory().getDataTransferOperations(connection.irodsAccount);

        // Fill object
        WorkflowSpades workflow = new WorkflowSpades();
        workflow.memory = commandOptions.memory;
        workflow.threads = Math.round(commandOptions.threads);

//        System.err.println(workflow.memory);
//        System.err.println(workflow.threads);

        Iterator<ResultLine> observationUnits = domain.getRDFSimpleCon().runQuery("getObservationUnits.txt", false).iterator();
        ArrayList<String> observationUnitsList = new ArrayList<>();
        observationUnits.forEachRemaining(resultLine -> observationUnitsList.add(resultLine.getIRI("ou")));

        for (String observationUnitURL : observationUnitsList) {
            observation_unit observationUnit = domain.make(observation_unit.class, observationUnitURL);

            // This needs work, where do we put it?
            String destination = observationUnit.getLogicalPath().replaceAll("/Unprocessed.*", "/processed/") +"/"+ commandOptions.wid;

            // Getting the files
            ArrayList<String> filePaths = new ArrayList<>();
            for (Sample sample : observationUnit.getAllHasPart()) {
                for (Assay assay : sample.getAllHasPart()) {
                    if (assay.getClassTypeIri().endsWith("DNASeqAssay")) {
                        for (Data_sample data_sample : assay.getAllHasPart()) {
                            // Parse each sequence dataset, as we might have overlapping identifiers in the future...
                            SequenceDataSet sequenceDataSet = (SequenceDataSet) data_sample;
                            // If file already processed skip...
                            if (filePaths.contains(new URL(sequenceDataSet.getContentUrl()).getPath())) continue;

                            if (sequenceDataSet.getSeqPlatform().getIRI().endsWith("Illumina")) {
                                PairedSequenceDataSet pairedSequenceDataSet = (PairedSequenceDataSet) assay.getAllHasPart().get(0);
                                workflow.addForward_reads(new URL(pairedSequenceDataSet.getContentUrl()).getPath());
                                workflow.addReverse_reads(new URL(pairedSequenceDataSet.getPaired().getContentUrl()).getPath());
                                // Adding to already processed files due to linked paired dataset
                                filePaths.add(new URL(pairedSequenceDataSet.getContentUrl()).getPath());
                                filePaths.add(new URL(pairedSequenceDataSet.getPaired().getContentUrl()).getPath());
                            } else if (sequenceDataSet.getSeqPlatform().getIRI().endsWith("PacBio")) {
                                if (sequenceDataSet.getFileFormat().getIRI().endsWith("FASTQ")) {
                                    workflow.addPacbio(new URL(sequenceDataSet.getContentUrl()).getPath());
                                } else {
                                    log.error("File type "+sequenceDataSet.getFileFormat()+" for " + sequenceDataSet.getName() + " not supported");
                                }
                            } else if (sequenceDataSet.getSeqPlatform().getIRI().endsWith("NanoPore")) {
                                log.error("NANOPORE NOT YET DONE");
                                // workflow.addP(assay.getAllFile().get(0).getFilePath());
                            }
                            filePaths.add(new URL(sequenceDataSet.getContentUrl()).getPath());
                        }
                    }
                }
            }

            workflow.setDestination(destination);

            if (workflow.getForward_reads().size() == 0 || workflow.getReverse_reads().size() == 0) {
                log.debug("No illumina paired reads detected, cannot run SPADES");
                continue;
            }

            // Save to nl.wur.ssb.yaml format
            String yamlName;
            if (commandOptions.wid.length() == 0) {
                // Generate own nl.wur.ssb.yaml name...
                yamlName = observationUnit.getIdentifier().replaceAll("\\.", "_") + ".yaml";
            } else {
                yamlName = commandOptions.wid + ".yaml";
            }

            YamlWriter writer = new YamlWriter(new FileWriter(yamlName));
            writer.write(workflow);
            writer.close();

            // Fix Clazz > Class
            Workflow.fixClazz(yamlName);

            log.info("NOT FINISHED YET");

            // Save to iRODS
            IRODSFile destFile = connection.fileFactory.instanceIRODSFile(observationUnit.getLogicalPath() + "/" + yamlName);

            log.info("Uploading " + new File(yamlName) + " to " + destFile);

            if (destFile.exists()) destFile.delete();

            dataTransferOperationsAO.putOperation(new File(yamlName), destFile, null, null);

            // Add metadata tag...
            DataObjectAO dataObjectAO = connection.irodsFileSystem.getIRODSAccessObjectFactory().getDataObjectAO(connection.irodsAccount);
            AvuData avuMetaData = new AvuData("cwl", commandOptions.cwl, "waiting");
            dataObjectAO.setAVUMetadata(destFile.getAbsolutePath(), avuMetaData);
        }
    }
}
