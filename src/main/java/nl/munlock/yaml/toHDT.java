package nl.munlock.yaml;

import com.esotericsoftware.yamlbeans.YamlWriter;
import nl.munlock.Generic;
import nl.munlock.irods.Connection;
import nl.munlock.irods.Search;
import nl.munlock.objects.WorkflowHDT;
import nl.munlock.objects.WorkflowToHDT;
import nl.munlock.options.workflow.CommandOptionsHDT;
import nl.munlock.options.workflow.CommandOptionsToHDT;
import org.apache.log4j.Logger;
import org.irods.jargon.core.pub.DataObjectAO;
import org.irods.jargon.core.pub.DataTransferOperations;
import org.irods.jargon.core.pub.domain.AvuData;
import org.irods.jargon.core.pub.io.IRODSFile;

import java.io.File;
import java.io.FileWriter;
import java.util.Set;

public class toHDT {
    private static final Logger log = Generic.getLogger(toHDT.class, false);

    public static void generateHDTWorkflow(CommandOptionsToHDT commandOptions, Connection connection) throws Exception {
        DataTransferOperations dataTransferOperationsAO = connection.irodsFileSystem.getIRODSAccessObjectFactory().getDataTransferOperations(connection.irodsAccount);

        File yamlFile = new File(new File(commandOptions.input).getName() + "_toHDT.yaml");

        if (yamlFile.exists())
            yamlFile.delete();

        // Save to iRODS
        IRODSFile destFile = connection.fileFactory.instanceIRODSFile(commandOptions.input + "_toHDT.yaml");

        if (!destFile.exists()) {
            // If not exists start building
        } else if (commandOptions.overwrite && destFile.exists())
            // If exists and overwrite, delete
            destFile.delete();
        else {
            // Else skip this one
            return;
        }

        log.info("Uploading " + yamlFile + " to " + destFile);

        WorkflowToHDT workflowToHDT = new WorkflowToHDT();
        workflowToHDT.setDestination(new File(commandOptions.output).getParentFile().getAbsolutePath());
        workflowToHDT.setMemory(commandOptions.memory);
        workflowToHDT.setThreads(Math.round(commandOptions.threads));
        workflowToHDT.setProvenance(false);
        workflowToHDT.setInput(commandOptions.input);
        workflowToHDT.output = new File(commandOptions.output).getName();

        YamlWriter writer = new YamlWriter(new FileWriter(yamlFile));
        writer.write(workflowToHDT);
        writer.close();

        // Fix Clazz > Class
        Workflow.fixClazz(yamlFile.getAbsolutePath());

        dataTransferOperationsAO.putOperation(yamlFile, destFile, null, null);

        // Add metadata tag...
        DataObjectAO dataObjectAO = connection.irodsFileSystem.getIRODSAccessObjectFactory().getDataObjectAO(connection.irodsAccount);
        AvuData avuMetaData = new AvuData("cwl", commandOptions.cwl, "waiting");
        dataObjectAO.setAVUMetadata(destFile.getAbsolutePath(), avuMetaData);

        if (yamlFile.exists())
            yamlFile.delete();

    }
}
