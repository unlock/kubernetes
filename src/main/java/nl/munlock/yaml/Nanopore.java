package nl.munlock.yaml;

import com.esotericsoftware.yamlbeans.YamlWriter;
import nl.munlock.Generic;
import nl.munlock.irods.Connection;
import nl.munlock.objects.WorkflowNanopore;
import nl.munlock.options.workflow.CommandOptionsNanopore;
import nl.wur.ssb.RDFSimpleCon.ResultLine;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.log4j.Logger;
import org.irods.jargon.core.pub.DataObjectAO;
import org.irods.jargon.core.pub.DataTransferOperations;
import org.irods.jargon.core.pub.domain.AvuData;
import org.irods.jargon.core.pub.io.IRODSFile;
import org.jermontology.ontology.JERMOntology.domain.Assay;
import org.jermontology.ontology.JERMOntology.domain.Data_sample;
import org.jermontology.ontology.JERMOntology.domain.Sample;
import org.purl.ppeo.PPEO.owl.domain.observation_unit;

import java.io.File;
import java.io.FileWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

import static nl.munlock.irods.Search.makePath;

public class Nanopore {
    private static final Logger log = Generic.getLogger(Nanopore.class, Yaml.debug);
    static Map<String, String> prefixMap;
    public static void generateNanoporeWorkflow(CommandOptionsNanopore commandOptions, Connection connection, Domain domain) throws Exception {
        prefixMap = domain.getRDFSimpleCon().getModel().getNsPrefixMap();

        DataTransferOperations dataTransferOperationsAO = connection.irodsFileSystem.getIRODSAccessObjectFactory().getDataTransferOperations(connection.irodsAccount);

        // Check filter references
        ArrayList<String> filterPaths = nl.munlock.irods.Generic.prepareFilter(connection, commandOptions.filter_references);

        // For observation units
        Iterator<ResultLine> observationUnits = domain.getRDFSimpleCon().runQuery("getObservationUnits.txt", true).iterator();
        ArrayList<String> observationUnitsList = new ArrayList<>();
        observationUnits.forEachRemaining(resultLine -> observationUnitsList.add(resultLine.getIRI("ou")));
        Collections.sort(observationUnitsList);

        if (commandOptions.level.equalsIgnoreCase("ou")) {
            for (String observationUnitURL : observationUnitsList) {
                log.info("Observation url " + observationUnitURL);
                // Fill object
                WorkflowNanopore workflow = new WorkflowNanopore();
                workflow.memory = commandOptions.memory;
                workflow.threads = Math.round(commandOptions.threads);

                workflow.setIdentifier(commandOptions.wid);
                workflow.setProvenance(true);
                workflow.setBasecall_model(commandOptions.basecall_model);
                workflow.setKraken_database(commandOptions.kraken_database);
                workflow.setMetagenome(true);
                workflow.setConfiguration_command(commandOptions.configuration_command);
                workflow.binning = false;
                workflow.setFilter_references(filterPaths);
                workflow.setDeduplicate(commandOptions.deduplicate);
                workflow.setUse_reference_mapped_reads(commandOptions.use_reference_mapped);

                if (commandOptions.genome_size.length() > 0)
                    workflow.setGenome_size(commandOptions.genome_size);

                observation_unit observationUnit = domain.make(observation_unit.class, observationUnitURL);

                // This needs work, where do we put it?
                String destination = observationUnit.getLogicalPath().replaceAll("/.nprocessed.*", "/processed/") +"/"+ commandOptions.wid;

                // Map matches with the regex path query?
                String path = makePath(commandOptions.project, commandOptions.investigation, commandOptions.study, commandOptions.observationUnit, commandOptions.sample, commandOptions.assay, connection);
                path = path.replaceAll("%", ".*");

                if (!destination.matches(path)) {
                    log.info("Skipping destination " + destination + " " + path);
                    continue;
                }
                log.info("Processing " + path);

                // Getting the files from an observation unit
                ArrayList<String> filePaths = new ArrayList<>();
                for (Sample sample : observationUnit.getAllHasPart()) {
                    // Similar for either approach
                    prepareAssays(sample, commandOptions, workflow, filePaths);
                }

                workflow.setDestination(destination);
                workflow.irods = workflow.getIRODS();
                workflow.setIrods(workflow.getIRODS());
                workflow.setBam_workers(workflow.getThreads());
                // Save to nl.wur.ssb.yaml format
                String yamlName;
                if (commandOptions.wid.length() == 0) {
                    // Generate own nl.wur.ssb.yaml name...
                    yamlName = observationUnit.getIdentifier().replaceAll("\\.", "_") + ".yaml";
                } else {
                    yamlName = commandOptions.wid + ".yaml";
                }

                YamlWriter writer = new YamlWriter(new FileWriter(yamlName));

                writer.write(workflow);
                writer.close();

                // Fix Clazz > Class
                Workflow.fixClazz(yamlName);

                if (workflow.getNanopore_fastq_files().size() == 0 && workflow.getFast5_files().size() == 0) {
                    String message = "No input files detected for " + yamlName + " " + observationUnitURL;
                    log.error(message);
                    // throw new Exception(message);
                    continue;
                }

                // Save to iRODS
                IRODSFile destFile = connection.fileFactory.instanceIRODSFile(observationUnit.getLogicalPath() + "/" + yamlName);

                log.info("Uploading " + new File(yamlName) + " to " + destFile);

                if (destFile.exists()) destFile.delete();

                dataTransferOperationsAO.putOperation(new File(yamlName), destFile, null, null);

                // Add metadata tag...
                DataObjectAO dataObjectAO = connection.irodsFileSystem.getIRODSAccessObjectFactory().getDataObjectAO(connection.irodsAccount);
                AvuData avuMetaData = new AvuData("cwl", "/unlock/infrastructure/cwl/workflows/"+commandOptions.cwl, "waiting");
                dataObjectAO.setAVUMetadata(destFile.getAbsolutePath(), avuMetaData);
            }
        } else if (commandOptions.level.equalsIgnoreCase("sample")) {
            for (String observationUnitURL : observationUnitsList) {

                observation_unit observationUnit = domain.make(observation_unit.class, observationUnitURL);

                for (Sample sample : observationUnit.getAllHasPart()) {
                    log.info("Sample " + sample.getResource().getURI());
                    // Fill object
                    WorkflowNanopore workflow = new WorkflowNanopore();
                    workflow.memory = commandOptions.memory;
                    workflow.threads = Math.round(commandOptions.threads);

                    workflow.setIdentifier(commandOptions.wid);
                    workflow.setProvenance(true);
                    workflow.setBasecall_model(commandOptions.basecall_model);
                    workflow.setKraken_database(commandOptions.kraken_database);
                    workflow.setMetagenome(true);
                    workflow.setConfiguration_command(commandOptions.configuration_command);
                    workflow.binning = false;
                    workflow.setFilter_references(filterPaths);
                    workflow.setDeduplicate(commandOptions.deduplicate);
                    workflow.setUse_reference_mapped_reads(commandOptions.use_reference_mapped);

                    if (commandOptions.genome_size.length() > 0)
                        workflow.setGenome_size(commandOptions.genome_size);

                    // This needs work, where do we put it?
                    String destination = observationUnit.getLogicalPath() + "/SAM_"+ sample.getIdentifier() + "/";

                    // Map matches with the regex path query?
                    String path = makePath(commandOptions.project, commandOptions.investigation, commandOptions.study, commandOptions.observationUnit, commandOptions.sample, commandOptions.assay, connection);
                    path = path.replaceAll("%", ".*");

                    if (!destination.matches(path)) {
                        log.info("Skipping destination " + destination + " " + path);
                        continue;
                    }
                    log.info("Processing " + path);

                    // Getting the files from a sample
                    ArrayList<String> filePaths = new ArrayList<>();

                    prepareAssays(sample, commandOptions, workflow, filePaths);

                    workflow.setDestination(destination + commandOptions.wid);
                    workflow.irods = workflow.getIRODS();
                    workflow.setIrods(workflow.getIRODS());
                    workflow.setBam_workers(workflow.getThreads());
                    // Save to nl.wur.ssb.yaml format
                    String yamlName;
                    if (commandOptions.wid.length() == 0) {
                        // Generate own nl.wur.ssb.yaml name...
                        yamlName = sample.getIdentifier().replaceAll("\\.", "_") + ".yaml";
                    } else {
                        yamlName = commandOptions.wid + ".yaml";
                    }

                    YamlWriter writer = new YamlWriter(new FileWriter(yamlName));

                    writer.write(workflow);
                    writer.close();

                    // Fix Clazz > Class
                    Workflow.fixClazz(yamlName);

                    if (workflow.getNanopore_fastq_files().size() == 0 && workflow.getFast5_files().size() == 0) {
                        String message = "No input files detected for " + yamlName + " " + observationUnitURL;
                        log.error(message);
                        // throw new Exception(message);
                        continue;
                    }

                    // Save to iRODS
                    IRODSFile destFile = connection.fileFactory.instanceIRODSFile(destination + "/" + yamlName);

                    log.info("Uploading " + new File(yamlName) + " to " + destFile);

                    if (destFile.exists()) destFile.delete();

                    dataTransferOperationsAO.putOperation(new File(yamlName), destFile, null, null);

                    // Add metadata tag...
                    DataObjectAO dataObjectAO = connection.irodsFileSystem.getIRODSAccessObjectFactory().getDataObjectAO(connection.irodsAccount);
                    AvuData avuMetaData = new AvuData("cwl", "/unlock/infrastructure/cwl/workflows/" + commandOptions.cwl, "waiting");
                    dataObjectAO.setAVUMetadata(destFile.getAbsolutePath(), avuMetaData);
                }
            }
        }
    }

    private static void prepareAssays(Sample sample, CommandOptionsNanopore commandOptions, WorkflowNanopore workflow, ArrayList<String> filePaths) throws MalformedURLException {
        for (Assay assay : sample.getAllHasPart()) {
            String platform = Generic.getProperty(assay.getResource().getModel(), assay.getResource().getURI(), prefixMap.get("base") + "Platform").next().toString();

            if (platform.endsWith("OXFORD_NANOPORE")) {
                for (Data_sample data_sample : assay.getAllHasPart()) {
                    // Only for a certain workflow
                    if (commandOptions.cwl.endsWith("workflow_nanopore.cwl") && data_sample.getFileFormat().getIRI().endsWith("Other")) {
                        workflow.addFast5File(new URL(data_sample.getContentUrl()).getPath());
                    } else if (commandOptions.cwl.endsWith("workflow_nanopore_assembly.cwl") && data_sample.getFileFormat().getIRI().endsWith("format_1930")) {
                        workflow.addNanoporeFastqFile(new URL(data_sample.getContentUrl()).getPath());
                    } else {
                        log.error("File type " + data_sample.getFileFormat() + " for " + data_sample.getName() + " not supported");
                    }
                    filePaths.add(new URL(data_sample.getContentUrl()).getPath());
                }
            } else if (platform.endsWith("PACBIO_SMRT")) {
                    for (Data_sample data_sample : assay.getAllHasPart()) {
                        workflow.addPacbio_reads(new URL(data_sample.getContentUrl()).getPath());
                        filePaths.add(new URL(data_sample.getContentUrl()).getPath());
                    }
            } else if (platform.endsWith("ILLUMINA")) {
                // Assay has two parts
                List<? extends Data_sample> data_samples = assay.getAllHasPart();
                if (data_samples.size() != 2) {
                    log.error("ILLUMINA data detected but not in paired format, detected " + data_samples.size() + " file(s)");
                }
                String url1 = new URL(data_samples.get(0).getContentUrl()).getPath();
                String url2 = new URL(data_samples.get(1).getContentUrl()).getPath();
                // Skip if file is already added
                if (workflow.getIllumina_forward_reads().contains(url1) || workflow.getIllumina_reverse_reads().contains(url2)) continue;
                workflow.addForward_illumina_fastq_file(url1);
                workflow.addReverse_illumina_fastq_file(url2);
                // Binning is only possible when illumina data is present
                workflow.binning = commandOptions.binning;
                filePaths.add(new URL(data_samples.get(0).getContentUrl()).getPath());
                filePaths.add(new URL(data_samples.get(1).getContentUrl()).getPath());
            } else {
                log.error("Sequencing platform not supported: " + platform);
            }
        }
    }
}
