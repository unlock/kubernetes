package nl.munlock.yaml;

import nl.munlock.Generic;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;
import java.util.regex.Pattern;

public class Workflow {
    private static final Logger log = Generic.getLogger(Workflow.class, Yaml.debug);
    static Pattern samplePattern = Pattern.compile("^[0-9_]+$");

    /**
     * Transforms the clazz: variable to class: needed for yaml but not allowed by java
     * TODO find a way of doing it directly without additional code
     * @param yaml
     * @throws FileNotFoundException
     */
    public static void fixClazz(String yaml) throws FileNotFoundException {
        Scanner scanner = new Scanner(new File(yaml));
        String content = "";
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            line = line.replaceAll("clazz:","class:");
            content = content + line + "\n";
        }
        scanner.close();
        PrintWriter writer = new PrintWriter(new File(yaml));
        writer.print(content);
        writer.close();
    }

    /**
     * Transforms the class: variable to clazz: needed to read the yaml back into java objects
     * TODO find a way of doing it directly without additional code
     * @param yaml
     * @throws FileNotFoundException
     */
    public static void fixClass(String yaml) throws FileNotFoundException {
        Scanner scanner = new Scanner(new File(yaml));
        String content = "";
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            line = line.replaceAll("class:","clazz:");
            content = content + line + "\n";
        }
        scanner.close();
        PrintWriter writer = new PrintWriter(new File(yaml));
        writer.print(content);
        writer.close();
    }

    /**
     * Removes all the "- !" from the file
     * @param yaml
     * @throws FileNotFoundException
     */
    public static void fixComments(String yaml) throws FileNotFoundException {
        Scanner scanner = new Scanner(new File(yaml));
        String content = "";
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            line = line.replaceAll("^- !.*","");
            content = content + line + "\n";
        }
        scanner.close();
        PrintWriter writer = new PrintWriter(new File(yaml));
        writer.print(content);
        writer.close();
    }

    public static void fixSample(String yaml) throws FileNotFoundException {
        Scanner scanner = new Scanner(new File(yaml));
        String content = "";
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            if (line.startsWith("sample:")) {
                String key = line.split(":")[0].trim();
                String value = line.split(":")[1].trim();
                if (samplePattern.matcher(value).matches()) {
                    log.info("Sample forced quotes for " + value);
                    value = "\"" + value + "\"";
                    line = key + ": " + value;
                }
            }
            content = content + line + "\n";
        }
        scanner.close();
        PrintWriter writer = new PrintWriter(new File(yaml));
        writer.print(content);
        writer.close();
    }
}
