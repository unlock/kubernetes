package nl.munlock.yaml;

import com.esotericsoftware.yamlbeans.YamlWriter;
import nl.munlock.Generic;
import nl.munlock.irods.Connection;
import nl.munlock.objects.WorkflowMetagenomics;
import nl.munlock.ontology.domain.PairedSequenceDataSet;
import nl.munlock.ontology.domain.SequenceDataSet;
import nl.munlock.options.workflow.CommandOptionsMetagenomics;
import nl.wur.ssb.RDFSimpleCon.ResultLine;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.log4j.Logger;
import org.irods.jargon.core.pub.DataObjectAO;
import org.irods.jargon.core.pub.DataTransferOperations;
import org.irods.jargon.core.pub.domain.AvuData;
import org.irods.jargon.core.pub.io.IRODSFile;
import org.jermontology.ontology.JERMOntology.domain.Assay;
import org.jermontology.ontology.JERMOntology.domain.Data_sample;
import org.jermontology.ontology.JERMOntology.domain.Sample;
import org.purl.ppeo.PPEO.owl.domain.observation_unit;

import java.io.File;
import java.io.FileWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

public class MetaGenomics {
    private static final Logger log = Generic.getLogger(MetaGenomics.class, Yaml.debug);

    public static void generateMetaGenomicsWorkflow(CommandOptionsMetagenomics commandOptions, Connection connection, Domain domain) throws Exception {
        DataTransferOperations dataTransferOperationsAO = connection.irodsFileSystem.getIRODSAccessObjectFactory().getDataTransferOperations(connection.irodsAccount);

        Iterator<ResultLine> observationUnits = domain.getRDFSimpleCon().runQuery("getObservationUnits.txt", true).iterator();
        ArrayList<String> observationUnitsList = new ArrayList<>();
        observationUnits.forEachRemaining(resultLine -> observationUnitsList.add(resultLine.getIRI("ou")));

        // Check filter references
        ArrayList<String> filterPaths = nl.munlock.irods.Generic.prepareFilter(connection, commandOptions.filter_references);

        if (filterPaths.size() == 0) {
            log.error("Failed processing filter references");
            return;
        }

        if (observationUnitsList.size() == 0) {
            log.error("No observation units detected");
            domain.save("test.ttl");
            return;
        }

        // Two levels as option, at OU level see code below or at assay level
        if (commandOptions.level.matches("Sample")) {
            for (String observationUnitURL : observationUnitsList) {
                observation_unit observationUnit = domain.make(observation_unit.class, observationUnitURL);
                for (Sample sample : observationUnit.getAllHasPart()) {
                    log.debug("==============================================");
                    log.debug(sample.getResource().getURI());

                    // Fill object
                    WorkflowMetagenomics workflow = new WorkflowMetagenomics();
                    workflow.memory = commandOptions.memory;
                    workflow.threads = Math.round(commandOptions.threads);
                    workflow.identifier = sample.getIdentifier();
                    workflow.binning = true;

                    for (Assay assay : sample.getAllHasPart()) {
                        HashSet<String> filePaths = new HashSet<>();

                        if (assay.getClassTypeIri().endsWith("DNASeqAssay")) {
                            // This needs work, where do we put it?

                            for (Data_sample data_sample : assay.getAllHasPart()) {
                                log.debug(data_sample.getResource().getURI());
                                // Parse each sequence dataset, as we might have overlapping identifiers in the future...
                                SequenceDataSet sequenceDataSet = (SequenceDataSet) data_sample;
                                // If file already processed skip...
                                if (filePaths.contains(new URL(sequenceDataSet.getContentUrl()).getPath())) continue;

                                if (sequenceDataSet.getSeqPlatform().getIRI().endsWith("Illumina")) {
                                    log.debug("illumina sequence data detected");
                                    PairedSequenceDataSet pairedSequenceDataSet = (PairedSequenceDataSet) data_sample;
                                    workflow.addForward_reads(new URL(pairedSequenceDataSet.getContentUrl()).getPath());
                                    workflow.addReverse_reads(new URL(pairedSequenceDataSet.getPaired().getContentUrl()).getPath());
                                    // Adding to already processed files due to linked paired dataset
                                    filePaths.add(new URL(pairedSequenceDataSet.getContentUrl()).getPath());
                                    filePaths.add(new URL(pairedSequenceDataSet.getPaired().getContentUrl()).getPath());
                                } else if (sequenceDataSet.getSeqPlatform().getIRI().endsWith("PacBio")) {
                                    if (sequenceDataSet.getFileFormat().getIRI().endsWith("FASTQ")) {
                                        workflow.addPacbio(new URL(sequenceDataSet.getContentUrl()).getPath());
                                    } else {
                                        log.error("File type "+sequenceDataSet.getFileFormat()+" for " + sequenceDataSet.getName() + " not supported");
                                    }
                                } else if (sequenceDataSet.getSeqPlatform().getIRI().endsWith("Nanopore")) {
                                    workflow.addNanopore(new URL(sequenceDataSet.getContentUrl()).getPath());
                                }
                                filePaths.add(new URL(sequenceDataSet.getContentUrl()).getPath());
                            }
                        }
                    }

                    workflow.setDestination(observationUnit.getLogicalPath() + "/SAM_" + sample.getIdentifier() +"/" + commandOptions.wid);
                    workflow.setKraken_database(commandOptions.kraken_database);
                    workflow.setFilter_references(filterPaths);
                    workflow.irods = workflow.getIRODS();
                    workflow.getIrods().putAll(workflow.getIRODS());

                    if (workflow.getIllumina_forward_reads().size() == 0 || workflow.getIllumina_reverse_reads().size() == 0) {
                        log.debug("No illumina paired reads detected, cannot run SPADES");
                        continue;
                    }

                    // Save to nl.wur.ssb.yaml format
                    String yamlName;
                    if (commandOptions.wid.length() == 0) {
                        // Generate own nl.wur.ssb.yaml name...
                        yamlName = sample.getIdentifier().replaceAll("\\.", "_") + ".yaml";
                    } else {
                        yamlName = commandOptions.wid + ".yaml";
                    }

                    YamlWriter writer = new YamlWriter(new FileWriter(yamlName));
                    writer.write(workflow);
                    writer.close();

                    // Fix Clazz > Class
                    Workflow.fixClazz(yamlName);

                    // Save to iRODS
                    IRODSFile destFile = connection.fileFactory.instanceIRODSFile(observationUnit.getLogicalPath() + "/SAM_" +sample.getIdentifier()+ "/" + yamlName);

                    log.info("Uploading " + new File(yamlName) + " to " + destFile);

                    if (destFile.exists()) destFile.delete();

                    dataTransferOperationsAO.putOperation(new File(yamlName), destFile, null, null);

                    // Add metadata tag...
                    DataObjectAO dataObjectAO = connection.irodsFileSystem.getIRODSAccessObjectFactory().getDataObjectAO(connection.irodsAccount);
                    AvuData avuMetaData = new AvuData("cwl", "/unlock/infrastructure/cwl/workflows/" + commandOptions.cwl, "waiting");
                    dataObjectAO.setAVUMetadata(destFile.getAbsolutePath(), avuMetaData);
                }
            }
        }

        if (commandOptions.level.matches("OU")) {
            // OU level
            for (String observationUnitURL : observationUnitsList) {

                log.debug("==============================================");
                log.debug(observationUnitURL);

                // Fill object
                WorkflowMetagenomics workflow = new WorkflowMetagenomics();
                workflow.memory = commandOptions.memory;
                workflow.threads = Math.round(commandOptions.threads);
                workflow.binning = true;
                workflow.setFilter_references(filterPaths);

                observation_unit observationUnit = domain.make(observation_unit.class, observationUnitURL);

                workflow.identifier = observationUnit.getIdentifier().replaceAll("^OBS_", "");

                // This needs work, where do we put it?
                String destination = observationUnit.getLogicalPath().replaceAll("/Unprocessed.*", "/processed/") + "/" + commandOptions.wid;

                // Getting the files
                HashSet<String> filePaths = new HashSet<>();
                for (Sample sample : observationUnit.getAllHasPart()) {
                    System.err.println(sample.getIdentifier());
                    for (Assay assay : sample.getAllHasPart()) {
                        boolean illumina = false;
                        for (String s : assay.getAllAdditionalType()) {
                            if (s.toLowerCase().contains("illumina")) {
                                illumina = true;
                            }
                        }
                        if (illumina) {
                            for (Data_sample data_sample : assay.getAllHasPart()) {
                                log.error(data_sample.getResource().getURI());
                                // Parse each sequence dataset, as we might have overlapping identifiers in the future...
                                SequenceDataSet sequenceDataSet = (SequenceDataSet) data_sample;
                                // If file already processed skip...
                                if (filePaths.contains(new URL(sequenceDataSet.getContentUrl()).getPath()))
                                    continue;

                                if (sequenceDataSet.getSeqPlatform().getIRI().endsWith("Illumina")) {
                                    log.debug("illumina sequence data detected");
                                    PairedSequenceDataSet pairedSequenceDataSet = (PairedSequenceDataSet) data_sample;
                                    workflow.addForward_reads(new URL(pairedSequenceDataSet.getContentUrl()).getPath());
                                    workflow.addReverse_reads(new URL(pairedSequenceDataSet.getPaired().getContentUrl()).getPath());
                                    // Adding to already processed files due to linked paired dataset
                                    filePaths.add(new URL(pairedSequenceDataSet.getContentUrl()).getPath());
                                    filePaths.add(new URL(pairedSequenceDataSet.getPaired().getContentUrl()).getPath());
                                } else if (sequenceDataSet.getSeqPlatform().getIRI().endsWith("PacBio")) {
                                    System.err.println(sequenceDataSet.getFileFormat());
                                    if (sequenceDataSet.getFileFormat().getIRI().endsWith("FASTQ")) {
                                        workflow.addPacbio(new URL(sequenceDataSet.getContentUrl()).getPath());
                                    } else {
                                        log.error("File type "+sequenceDataSet.getFileFormat()+" for " + sequenceDataSet.getName() + " not supported");
                                    }
                                } else if (sequenceDataSet.getSeqPlatform().getIRI().endsWith("Nanopore")) {
                                    workflow.addNanopore(new URL(sequenceDataSet.getContentUrl()).getPath());
                                }
                                filePaths.add(new URL(sequenceDataSet.getContentUrl()).getPath());
                            }
                        }
                    }
                }

                workflow.setDestination(destination);
                workflow.setKraken_database("/unlock/references/databases/Kraken2/K2_PlusPF_20210517");

                if (workflow.getIllumina_forward_reads().size() == 0 || workflow.getIllumina_reverse_reads().size() == 0) {
                    log.error("No illumina paired reads detected, cannot run SPADES");
                    continue;
                }

                // Save to nl.wur.ssb.yaml format
                String yamlName;
                if (commandOptions.wid.length() == 0) {
                    // Generate own nl.wur.ssb.yaml name...
                    yamlName = observationUnit.getIdentifier().replaceAll("\\.", "_") + ".yaml";
                } else {
                    yamlName = commandOptions.wid + ".yaml";
                }

                YamlWriter writer = new YamlWriter(new FileWriter(yamlName));
                writer.write(workflow);
                writer.close();

                // Fix Clazz > Class
                Workflow.fixClazz(yamlName);

                // Save to iRODS
                IRODSFile destFile = connection.fileFactory.instanceIRODSFile(observationUnit.getLogicalPath() + "/" + yamlName);

                log.info("Uploading " + new File(yamlName) + " to " + destFile);

                if (destFile.exists()) destFile.delete();

                dataTransferOperationsAO.putOperation(new File(yamlName), destFile, null, null);

                // Add metadata tag...
                DataObjectAO dataObjectAO = connection.irodsFileSystem.getIRODSAccessObjectFactory().getDataObjectAO(connection.irodsAccount);
                AvuData avuMetaData = new AvuData("cwl", "/unlock/infrastructure/cwl/workflows/" + commandOptions.cwl, "waiting");
                dataObjectAO.setAVUMetadata(destFile.getAbsolutePath(), avuMetaData);
            }
        }
    }
}
