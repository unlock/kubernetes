package nl.munlock.yaml;

import com.esotericsoftware.yamlbeans.YamlWriter;
import nl.munlock.Generic;
import nl.munlock.irods.Connection;
import nl.munlock.options.workflow.CommandOptionsIndexer;
import nl.munlock.objects.WorkflowIndexer;
import org.apache.log4j.Logger;
import org.irods.jargon.core.exception.JargonException;
import org.irods.jargon.core.packinstr.TransferOptions;
import org.irods.jargon.core.pub.DataObjectAO;
import org.irods.jargon.core.pub.DataTransferOperations;
import org.irods.jargon.core.pub.domain.AvuData;
import org.irods.jargon.core.pub.io.IRODSFile;
import org.irods.jargon.core.transfer.DefaultTransferControlBlock;
import org.irods.jargon.core.transfer.TransferControlBlock;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;

import static org.irods.jargon.core.packinstr.TransferOptions.ForceOption.NO_FORCE;
import static org.irods.jargon.core.packinstr.TransferOptions.ForceOption.USE_FORCE;

public class Index {
    private static final Logger log = Generic.getLogger(Index.class, Yaml.debug);

    public static void generateIndexWorkflow(CommandOptionsIndexer commandOptions, Connection connection) throws Exception {
        DataTransferOperations dataTransferOperationsAO = connection.irodsFileSystem.getIRODSAccessObjectFactory().getDataTransferOperations(connection.irodsAccount);

        // Check if reference exists in iRODS
        IRODSFile reference = connection.fileFactory.instanceIRODSFile(commandOptions.reference);
        if (!reference.exists()) {
            throw new FileNotFoundException("File not found in iRODS using path: " + reference.getAbsolutePath());
        }

        // File exists... Create yaml file next to genome file?
        String yamlFileName;

        // Fill object
        WorkflowIndexer workflow = new WorkflowIndexer();

        workflow.memory = commandOptions.memory;
        workflow.threads = Math.round(commandOptions.threads);

        workflow.setReference_genome("/Data/" + new File(commandOptions.reference).getName());

        // The parent of unprocessed
        String destination = reference.getParent();
        workflow.setDestination(destination);

        // Set star and value
        if (commandOptions.run_star) {
            workflow.setSetGenomeSAindexeNbases(commandOptions.genomeSAindexNbases);
        }

        // Save to nl.wur.ssb.yaml format
        String yamlName;
        if (commandOptions.wid.length() == 0) {
            // Generate own nl.wur.ssb.yaml name...
            yamlName = commandOptions.reference.replaceAll("\\.", "_") + ".yaml";
        } else {
            yamlName = commandOptions.wid + ".yaml";
        }

        YamlWriter writer = new YamlWriter(new FileWriter(yamlName));
        writer.write(workflow);
        writer.close();

        // Fix Clazz > Class
        Workflow.fixClazz(yamlName);

        // Save to iRODS
        IRODSFile destFile = connection.fileFactory.instanceIRODSFile(reference.getParentFile() + "/" + yamlName);


        log.info("Saving nl.wur.ssb.yaml file to " + destFile);
        dataTransferOperationsAO.putOperation(new File(yamlName), destFile, null, setTransferControlBlock(dataTransferOperationsAO, destFile, commandOptions.overwrite));

        // Add metadata tag...
        DataObjectAO dataObjectAO = connection.irodsFileSystem.getIRODSAccessObjectFactory().getDataObjectAO(connection.irodsAccount);
        AvuData avuMetaData = new AvuData("cwl","/unlock/infrastructure/cwl/workflows/" + commandOptions.cwl, "waiting");
        dataObjectAO.setAVUMetadata(destFile.getAbsolutePath(), avuMetaData);
    }

    public static TransferControlBlock setTransferControlBlock(DataTransferOperations dataTransferOperationsAO, IRODSFile destFile, boolean overwrite) throws JargonException {
        TransferOptions transferOptions = dataTransferOperationsAO.buildTransferOptionsBasedOnJargonProperties();
        TransferControlBlock transferControlBlock = DefaultTransferControlBlock.instance();
        if (destFile.exists() && overwrite)
            transferOptions.setForceOption(USE_FORCE);
        else
            transferOptions.setForceOption(NO_FORCE);
        transferControlBlock.setTransferOptions(transferOptions);
        return transferControlBlock;
    }
}
