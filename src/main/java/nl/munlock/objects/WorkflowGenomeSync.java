package nl.munlock.objects;

public class WorkflowGenomeSync extends Workflow {

    public String enaBrowserTools;

    public String taxonomy;
    public String identifier;
    public int codon;
    public boolean bacteria;

    public String getEnaBrowserTools() {
        return enaBrowserTools;
    }

    public void setEnaBrowserTools(String enaBrowserTools) {
        this.enaBrowserTools = enaBrowserTools;
    }

    public String getTaxonomy() {
        return taxonomy;
    }

    public void setTaxonomy(String taxonomy) {
        this.taxonomy = taxonomy;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public boolean isBacteria() {
        return bacteria;
    }

    public void setBacteria(boolean bacteria) {
        this.bacteria = bacteria;
    }

    public int getCodon() {
        return codon;
    }

    public void setCodon(int codon) {
        this.codon = codon;
    }
}
