package nl.munlock.objects;

import nl.munlock.Generic;
import nl.munlock.yaml.Yaml;
import org.apache.log4j.Logger;

public class WorkflowHDT extends Workflow {
    private static final Logger log = Generic.getLogger(WorkflowHDT.class, Yaml.debug);

    public String folder;

    public String getFolder() {
        return folder;
    }

    public void setFolder(String folder) {
        this.folder = folder;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getDestination() {
        return destination;
    }
//    public HashMap<String, String> irods = new HashMap<>();
//    public ArrayList<FileClass> forward_reads = new ArrayList<>();
//    public ArrayList<FileClass> reverse_reads = new ArrayList<>();
//    public ArrayList<FileClass> pacbio_reads = new ArrayList<>();
//    public String kraken_database; //  = new ArrayList<>();
//    public boolean isolate;
//    public boolean metagenomics;
//    public Map<String, String> clazz;

//    public void addIRODS(String irodsPath) {
//        irods.put(irods.size() + "_irods", irodsPath);
//    }

//    public void setDestination(String destination) {
//        this.destination = destination;
//    }
//
//    public String getDestination() {
//        return destination;
//    }

//    public ArrayList<FileClass> getForward_reads() {
//        return forward_reads;
//    }

//    public void addForward_reads(String forward_reads) throws Exception {
//        addIRODS(forward_reads);
//        FileClass fileClass = new FileClass();
//        fileClass.setClazz("File");
//        fileClass.setLocation(forward_reads);
//        this.forward_reads.add(fileClass);
//    }


//    public boolean isIsolate() {
//        return isolate;
//    }

//    public void setIsolate(boolean isolate) {
//        this.isolate = isolate;
//    }

//    public ArrayList<FileClass> getReverse_reads() {
//        return reverse_reads;
//    }

//    public void addReverse_reads(String reverse_reads) throws Exception {
//        addIRODS(reverse_reads);
//        FileClass fileClass = new FileClass();
//        fileClass.setClazz("File");
//        fileClass.setLocation(reverse_reads);
//        this.reverse_reads.add(fileClass);
//    }

//    public ArrayList<FileClass> getPacbio() {
//        return pacbio_reads;
//    }

//    public void addPacbio(String pacbio) throws Exception {
//        addIRODS(pacbio);
//        FileClass fileClass = new FileClass();
//        fileClass.setClazz("File");
//        fileClass.setLocation(pacbio);
//        this.pacbio_reads.add(fileClass);
//    }

//    public boolean isMetagenomics() {
//        return metagenomics;
//    }

//    public void setMetagenomics(boolean metagenomics) {
//        this.metagenomics = metagenomics;
//    }

//    public String getKraken_database() {
//        return kraken_database;
//    }

//    public void setKraken_database(String kraken_database) throws Exception {
//        this.kraken_database = kraken_database;
//        FileClass fileClass = new FileClass();
//        fileClass.setClazz("Directory");
//        fileClass.setLocation(kraken_database);
//        this.kraken_database.add(fileClass);
//    }
}
