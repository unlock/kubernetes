package nl.munlock.objects;

import java.util.ArrayList;
import java.util.HashMap;

import static nl.munlock.yaml.NGTax.fixPrimer;

public class WorkflowNgtax extends Workflow {

    public String reference_db;
    public String forward_primer;
    public String reverse_primer;
    public ArrayList<FileClass> metadata = new ArrayList<>();
    public ArrayList<FileClass> forward_reads = new ArrayList<>();
    public ArrayList<FileClass> reverse_reads = new ArrayList<>();
    // public HashMap<String, String> hashMapTest = new HashMap<>();
    public int rev_read_len;
    public int for_read_len;
    public String sample;
    public double minimum_threshold;

    private String mock3;
    private String mock4;
    public String fragment;
    public boolean primersRemoved;

    public void setReference_db(String reference_db) {
        this.reference_db = reference_db;
    }

    public String getReference_db() {
        return reference_db;
    }

    public void setForward_primer(String forward_primer) {
        this.forward_primer = fixPrimer(forward_primer);
    }

    public String getForward_primer() {
        return forward_primer;
    }

    public void setReverse_primer(String reverse_primer) {
        this.reverse_primer = fixPrimer(reverse_primer);
    }

    public String getReverse_primer() {
        return reverse_primer;
    }

    public void addIRODS(String value) {
        irods.put(this.irods.size() + "_irods", value);
    }

    public HashMap<String, String> getIRODS() {
        return irods;
    }

    public ArrayList<FileClass> getReverse_reads() {
        return reverse_reads;
    }

    public void addReverse_reads(String reverse_reads) throws Exception {
        addIRODS(reverse_reads);
        FileClass fileClass = new FileClass();
        fileClass.setClazz("File");
        fileClass.setLocation(reverse_reads);
        this.reverse_reads.add(fileClass);
    }

    public ArrayList<FileClass> getForward_reads() {
        return forward_reads;
    }

    public void addForward_reads(String forward_reads) throws Exception {
        this.addIRODS(forward_reads);
        FileClass fileClass = new FileClass();
        fileClass.setClazz("File");
        fileClass.setLocation(forward_reads);
        this.forward_reads.add(fileClass);
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getDestination() {
        return destination;
    }

    public void setRev_read_len(int rev_read_len) {
        this.rev_read_len = rev_read_len;
    }

    public int getRev_read_len() {
        return rev_read_len;
    }

    public void setFor_read_len(int for_read_len) {
        this.for_read_len = for_read_len;
    }

    public int getFor_read_len() {
        return for_read_len;
    }

    public void setSample(String sample) {
        this.sample = sample.trim();
    }

    public String getSample() {
        return sample;
    }

    public void setMinimum_threshold(double minimum_threshold) {
        this.minimum_threshold = minimum_threshold;
    }

    public double getMinimum_threshold() {
        return minimum_threshold;
    }

    public void setMock3(String mock3) {
        this.mock3 = mock3;
    }

    public String getMock3() {
        return mock3;
    }

    public void setMock4(String mock4) {
        this.mock4 = mock4;
    }

    public String getMock4() {
        return mock4;
    }

    public void setFragment(String fragment) {
        this.fragment = fragment;
    }

    public String getFragment() {
        return fragment;
    }

    public void setPrimersRemoved(boolean primersRemoved) {
        this.primersRemoved = primersRemoved;
    }

    public boolean getPrimersRemoved() {
        return primersRemoved;
    }

    public void setMetadata(String metadata) throws Exception {
        this.addIRODS(metadata);
        FileClass fileClass = new FileClass();
        fileClass.setClazz("File");
        fileClass.setLocation(metadata);
        this.metadata.add(fileClass);
    }

    public ArrayList<FileClass> getMetadata() {
        return metadata;
    }
}