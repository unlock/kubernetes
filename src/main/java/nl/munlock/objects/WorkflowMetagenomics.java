package nl.munlock.objects;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class WorkflowMetagenomics extends Workflow {

    public ArrayList<String> illumina_forward_reads = new ArrayList<>();
    public ArrayList<String> illumina_reverse_reads = new ArrayList<>();
    public ArrayList<String> pacbio_reads = new ArrayList<>();
    public ArrayList<String> nanopore_reads = new ArrayList<>();
    public String kraken_database;
    public HashSet<String> filter_references = new HashSet<>();
    public boolean isolate;
    public boolean metagenomics;
    public boolean binning;
    public Map<String, String> clazz;


    public void addIRODS(String irodsPath) {
        irods.put(irods.size() + "_irods", irodsPath);
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getDestination() {
        return destination;
    }

    public ArrayList<String> getIllumina_forward_reads() {
        return illumina_forward_reads;
    }

    public void addForward_reads(String forward_reads) throws Exception {
        addIRODS(forward_reads);
        this.illumina_forward_reads.add(forward_reads);
    }


    public boolean isIsolate() {
        return isolate;
    }

    public void setIsolate(boolean isolate) {
        this.isolate = isolate;
    }

    public ArrayList<String> getIllumina_reverse_reads() {
        return illumina_reverse_reads;
    }

    public void addReverse_reads(String reverse_reads) throws Exception {
        addIRODS(reverse_reads);
        this.illumina_reverse_reads.add(reverse_reads);
    }

    public ArrayList<String> getPacbio() {
        return pacbio_reads;
    }

    public void addPacbio(String pacbio) throws Exception {
        addIRODS(pacbio);
        this.pacbio_reads.add(pacbio);
    }

    public boolean isMetagenomics() {
        return metagenomics;
    }

    public void setMetagenomics(boolean metagenomics) {
        this.metagenomics = metagenomics;
    }

    public String getKraken_database() {
        return kraken_database;
    }

    public void setKraken_database(String kraken_database) throws Exception {
        this.kraken_database = kraken_database;
//        FileClass fileClass = new FileClass();
//        fileClass.setClazz("Directory");
//        fileClass.setLocation(kraken_database);
//        this.kraken_database.add(fileClass);
    }

    public HashSet<String> getFilter_references() {
        return filter_references;
    }

    public void setFilter_references(ArrayList<String> filter_references) {
        this.filter_references.addAll(filter_references);
        for (String filter_reference : this.filter_references) {
            addIRODS(filter_reference);
        }
    }

    public ArrayList<String> getNanopore_reads() {
        return nanopore_reads;
    }

    public void addNanopore(String nanopore) throws Exception {
        addIRODS(nanopore);
        this.nanopore_reads.add(nanopore);
    }

    public HashMap<String, String> getIRODS() {
        return irods;
    }

    public boolean isBinning() {
        return binning;
    }

    public void setBinning(boolean binning) {
        this.binning = binning;
    }
}
