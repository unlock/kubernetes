package nl.munlock.objects;

public class WorkflowToHDT extends Workflow {

    public FileClass input;

    public String output;

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public void setInput(String input) throws Exception {
        addIRODS(input);
        FileClass fileClass = new FileClass();
        fileClass.setClazz("File");
        fileClass.setLocation(input);
        this.input = fileClass;
    }

    public String getDestination() {
        return destination;
    }

    public void addIRODS(String irodsPath) {
        irods.put(irods.size() + "_irods", irodsPath);
    }
}
