package nl.munlock.objects;

import java.util.ArrayList;

public class WorkflowDemultiplex extends Workflow {

    public String reference_db; // = new ArrayList<>();
    public String forward_primer;
    public String reverse_primer;
    public ArrayList<FileClass> forward_reads = new ArrayList<>();
    public ArrayList<FileClass> reverse_reads = new ArrayList<>();

    //    public ArrayList<FileClass> files = new ArrayList<>();
    public int rev_read_len;
    public int for_read_len;
    public String sample;
    public double minimum_threshold;
    // public HashMap<String, String> irods = new HashMap<>();
    public ArrayList<FileClass> mapping_file = new ArrayList<>();

    public void setReference_db(String reference_db) throws Exception {
//            FileClass fileClass = new FileClass();
//            fileClass.setClazz("File");
//            fileClass.setLocation(reference_db);
//            this.reference_db.add(fileClass);
        this.reference_db = reference_db;
    }

    public String getReference_db() {
        return reference_db;
    }

    public void setForward_primer(String forward_primer) {
        this.forward_primer = forward_primer;
    }

    public String getForward_primer() {
        return forward_primer;
    }

    public void setReverse_primer(String reverse_primer) {
        this.reverse_primer = reverse_primer;
    }

    public String getReverse_primer() {
        return reverse_primer;
    }

    public void addIRODS(String irodsPath) {
        irods.put(irods.size() + "_irods", irodsPath);
    }

    public ArrayList<FileClass> getReverse_reads() {
        return reverse_reads;
    }

    public void addReverse_reads(String reverse_reads) throws Exception {
        addIRODS(reverse_reads);
        FileClass fileClass = new FileClass();
        fileClass.setClazz("File");
        fileClass.setLocation(reverse_reads);
        this.reverse_reads.add(fileClass);
    }

    public ArrayList<FileClass> getForward_reads() {
        return forward_reads;
    }
    public void setMappingFile(String mappingFile) throws Exception {
        addIRODS(mappingFile);
        FileClass fileClass = new FileClass();
        fileClass.setClazz("File");
        fileClass.setLocation(mappingFile);
        this.mapping_file.add(fileClass);
    }

    public void addForward_reads(String forward_reads) throws Exception {
        addIRODS(forward_reads);
        FileClass fileClass = new FileClass();
        fileClass.setClazz("File");
        fileClass.setLocation(forward_reads);
        this.forward_reads.add(fileClass);
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getDestination() {
        return destination;
    }

    public void setRev_read_len(int rev_read_len) {
        this.rev_read_len = rev_read_len;
    }

    public int getRev_read_len() {
        return rev_read_len;
    }

    public void setFor_read_len(int for_read_len) {
        this.for_read_len = for_read_len;
    }

    public int getFor_read_len() {
        return for_read_len;
    }

    public void setSample(String sample) {
        this.sample = sample.trim();
    }

    public String getSample() {
        return sample;
    }

    public void setMinimum_threshold(double minimum_threshold) {
        this.minimum_threshold = minimum_threshold;
    }

    public double getMinimum_threshold() {
        return minimum_threshold;
    }
}
