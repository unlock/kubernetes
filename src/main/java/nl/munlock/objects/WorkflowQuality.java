package nl.munlock.objects;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class WorkflowQuality extends Workflow {

    public String kraken_database;
    public ArrayList<String> nanopore_fastq_files = new ArrayList<>();
    public ArrayList<String> forward_reads = new ArrayList<>();
    public ArrayList<String> reverse_reads = new ArrayList<>();
    public ArrayList<String> pacbio_reads = new ArrayList<>();
    public boolean run_filter_references;
    private String folder;

    public void setKraken_database(String kraken_database) {
        // To ensure it is available?
        addIRODS(kraken_database);
        this.kraken_database = kraken_database;
    }
    public void setDestination(String destination) {
        this.destination = destination;
    }
    public String getDestination() {
        return destination;
    }

    public void addIRODS(String irodsPath) {
        irods.put(irods.size() + "_irods", irodsPath);
    }

    public boolean filter_rrna;

    public HashSet<String> filter_references = new HashSet<>();
    public HashSet<String> getFilter_references() {
        return filter_references;
    }

    public void setFilter_references(ArrayList<String> filter_references) {
        this.filter_references.addAll(filter_references);
        for (String filter_reference : this.filter_references) {
            addIRODS(filter_reference);
        }
    }

    public HashMap<String, String> getIRODS() {
        return irods;
    }

//    public void addFile(String sequenceFile) throws Exception {
//        addIRODS(sequenceFile);
//        FileClass fileClass = new FileClass();
//        fileClass.setClazz("File");
//        fileClass.setLocation(sequenceFile);
//        this.files.add(fileClass);
//    }

//    public ArrayList<String> getFiles() {
//        return files;
//    }

    public ArrayList<String> getIllumina_forward_reads() {
        return forward_reads;
    }

    public void addIllumina_forward_reads(String forward_reads) {
        addIRODS(forward_reads);
//        FileClass fileClass = new FileClass();
//        fileClass.setClazz("File");
//        fileClass.setLocation(forward_reads);
        this.forward_reads.add(forward_reads);
    }

    public ArrayList<String> getIllumina_reverse_reads() {
        return reverse_reads;
    }

    public void addIllumina_reverse_reads(String reverse_reads) {
        addIRODS(reverse_reads);
//        FileClass fileClass = new FileClass();
//        fileClass.setClazz("File");
//        fileClass.setLocation(reverse_reads);
        this.reverse_reads.add(reverse_reads);
    }

    public void setFolder(String folder) {
        this.folder = folder;
    }

    public String getFolder() {
        return folder;
    }
}
