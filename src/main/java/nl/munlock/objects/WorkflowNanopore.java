package nl.munlock.objects;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class WorkflowNanopore extends Workflow {

    public String configuration_command;
    public String basecall_model;
    public String genome_size;
    public String kraken_database;
    public boolean metagenome;
    public HashMap<String, String> irods = new HashMap<>();
    public ArrayList<String> fast5_files = new ArrayList<>();
    public ArrayList<String> nanopore_fastq_files = new ArrayList<>();
    public ArrayList<String> illumina_forward_reads = new ArrayList<>();
    public ArrayList<String> illumina_reverse_reads = new ArrayList<>();
    public ArrayList<String> pacbio_reads = new ArrayList<>();

    public Map<String, String> clazz;
    private int bam_workers;
    public HashSet<String> filter_references = new HashSet<>();
    public boolean binning;
    public boolean deduplicate;
    public boolean use_reference_mapped_reads;

    public void addIRODS(String irodsPath) {
        irods.put(irods.size() + "_irods", irodsPath);
    }

    public void setConfiguration_command(String configuration_command) {
        this.configuration_command = configuration_command;
    }

    public String getConfiguration_command() {
        return this.configuration_command;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getDestination() {
        return destination;
    }

    public void setKraken_database(String kraken_database) throws Exception {
        this.kraken_database = kraken_database;
    }

    public String getKraken_database() {
        return this.kraken_database;
    }

    public void addFast5File(String fast5_file) {
        addIRODS(fast5_file);
//        FileClass fileClass = new FileClass();
//        fileClass.setClazz("File");
//        fileClass.setLocation(fast5_file);
//        this.fast5_files.add(fileClass);
        this.fast5_files.add(fast5_file);
    }

    public void addNanoporeFastqFile(String fastq_file) {
        addIRODS(fastq_file);
//        FileClass fileClass = new FileClass();
//        fileClass.setClazz("File");
//        fileClass.setLocation(fastq_file);
//        this.nanopore_fastq_reads.add(fileClass);
        this.nanopore_fastq_files.add(fastq_file);
    }

    public ArrayList<String> getNanopore_fastq_files() {
        return nanopore_fastq_files;
    }

    public ArrayList<String> getFast5_files() {
        return fast5_files;
    }

    public String getBasecall_model() {
        return basecall_model;
    }

    public void setBasecall_model(String basecall_model) {
        this.basecall_model = basecall_model;
    }

    public String getGenome_size() {
        return genome_size;
    }

    public void setGenome_size(String genome_size) {
        this.genome_size = genome_size;
    }

    public HashMap<String, String> getIRODS() {
        return irods;
    }

    public void setBam_workers(int bam_workers) {
        this.bam_workers = bam_workers;
    }

    public int getBam_workers() {
        return bam_workers;
    }

    public boolean isMetagenome() {
        return metagenome;
    }

    public void setMetagenome(boolean metagenome) {
        this.metagenome = metagenome;
    }
    public void setDeduplicate(boolean deduplicate) {
        this.deduplicate= deduplicate;
    }
    public void setUse_reference_mapped_reads(boolean use_reference_mapped_reads) {
        this.use_reference_mapped_reads= use_reference_mapped_reads;
    }



    public HashSet<String> getFilter_references() {
        return filter_references;
    }

    public void setFilter_references(ArrayList<String> filter_references) {
        this.filter_references.addAll(filter_references);
        for (String filter_reference : this.filter_references) {
            addIRODS(filter_reference);
        }
    }

    public ArrayList<String> getIllumina_forward_reads() {
        return illumina_forward_reads;
    }
    public ArrayList<String> getIllumina_reverse_reads() {
        return illumina_reverse_reads;
    }

    public void addReverse_illumina_fastq_file(String illumina_fastq_file) {
        addIRODS(illumina_fastq_file);
        this.illumina_forward_reads.add(illumina_fastq_file);
    }
    public void addForward_illumina_fastq_file(String illumina_fastq_file) {
        addIRODS(illumina_fastq_file);
        this.illumina_reverse_reads.add(illumina_fastq_file);
    }

    public ArrayList<String> getPacbio_reads() {
        return pacbio_reads;
    }

    public void addPacbio_reads(String pacbio_reads) {
        addIRODS(pacbio_reads);
        this.pacbio_reads.add(pacbio_reads);
    }
}
