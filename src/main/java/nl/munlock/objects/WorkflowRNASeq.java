package nl.munlock.objects;

import java.util.ArrayList;
import java.util.HashMap;

public class WorkflowRNASeq extends Workflow {
    private ArrayList<String> indexFolder = new ArrayList<String>();
    private ArrayList<String> gtf = new ArrayList<String>();

    public ArrayList<String> forward_reads = new ArrayList<>();
    public ArrayList<String> reverse_reads = new ArrayList<>();

    private String prefix_id;
    private boolean filter_rrna;
    private String destination;
    public HashMap<String, String> irods = new HashMap<>();

    public void setIndexFolder(String reference_db) throws Exception {
            addIRODS(reference_db);
            this.indexFolder.add(reference_db);
    }

    public void setGtf(String gtf) throws Exception {
        addIRODS(gtf);
        this.gtf.add(gtf);
    }

    public ArrayList<String> getIndexFolder() {
        return indexFolder;
    }

    public void addIRODS(String value) {
        irods.put(irods.size() + "_irods", value);
    }

    public ArrayList<String> getReverse_reads() {
        return reverse_reads;
    }

    public void addReverse_reads(String reverse_reads) throws Exception {
        addIRODS(reverse_reads);
        this.reverse_reads.add(reverse_reads);
    }

    public ArrayList<String> getForward_reads() {
        return forward_reads;
    }

    public void addForward_reads(String forward_reads) throws Exception {
        addIRODS(forward_reads);
        this.forward_reads.add(forward_reads);
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getDestination() {
        return destination;
    }

    public String getPrefix_id() {
        return prefix_id;
    }

    public void setPrefix_id(String prefix_id) {
        this.prefix_id = prefix_id;
    }

    public boolean isFilter_rrna() {
        return filter_rrna;
    }

    public void setFilter_rrna(boolean filter_rrna) {
        this.filter_rrna = filter_rrna;
    }

    public ArrayList<String> getGtf() {
        return gtf;
    }

}
