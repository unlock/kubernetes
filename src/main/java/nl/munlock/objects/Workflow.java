package nl.munlock.objects;

import java.util.HashMap;

public class Workflow {
    public String destination;
    public int threads;
    public int memory;
    public boolean provenance;
    public String identifier;
    public String conda;
    public HashMap<String, String> irods = new HashMap<>();


    public void setThreads(int threads) {
        this.threads = threads;
    }

    public void setThreads(float threads) {
        this.threads = Math.round(threads);
    }

    public int getThreads() {
        return threads;
    }

    public void setMemory(int threads) {
        this.memory = threads;
    }

    public int getMemory() {
        return memory;
    }

    public void setProvenance(boolean provenance) {
        this.provenance = provenance;
    }

    public boolean getProvenance() {
        return provenance;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public HashMap<String, String> getIrods() {
        return irods;
    }

    public void setIrods(HashMap<String, String> irods) {
        this.irods = irods;
    }

    public String getConda() {
        return conda;
    }

    public void setConda(String conda) {
        this.conda = conda;
    }
}
