package nl.munlock.objects;

public class WorkflowSAPP extends Workflow {

    public String taxonomy;
    public FileClass embl;
    public int codon;

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getDestination() {
        return destination;
    }

    public String getTaxonomy() {
        return taxonomy;
    }

    public void setTaxonomy(String taxonomy) {
        this.taxonomy = taxonomy;
    }

    public int getCodon() {
        return codon;
    }

    public void setCodon(int codon) {
        this.codon = codon;
    }

    public FileClass getEmbl() { return embl; }

    public void setEmbl(String embl) throws Exception {
        addIRODS(embl);
        FileClass fileClass = new FileClass();
        fileClass.setClazz("File");
        fileClass.setLocation(embl);
        this.embl = fileClass;
    }

    public void addIRODS(String irodsPath) {
        irods.put(irods.size() + "_irods", irodsPath);
    }
}
