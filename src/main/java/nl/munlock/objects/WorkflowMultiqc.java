package nl.munlock.objects;

import java.util.HashMap;

public class WorkflowMultiqc extends Workflow {

    public String kraken_database;
    private String folder;

    public void setDestination(String destination) {
        this.destination = destination;
    }
    public String getDestination() {
        return destination;
    }

    public void addIRODS(String irodsPath) {
        irods.put(irods.size() + "_irods", irodsPath);
    }

    public HashMap<String, String> getIRODS() {
        return irods;
    }

    public void setFolder(String folder) {
        this.folder = folder;
    }

    public String getFolder() {
        return folder;
    }
}
