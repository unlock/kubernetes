package nl.munlock.objects;

import java.util.ArrayList;
import java.util.HashMap;

public class WorkflowIndexer extends Workflow {
    private int setGenomeSAindexeNbases;
    private ArrayList<FileClass> reference_genome = new ArrayList<>();

    private String destination;
    public HashMap<String, String> irods = new HashMap<>();

    public void setReference_genome(String reference_db) throws Exception {
            addIRODS(reference_db);
            FileClass fileClass = new FileClass();
            fileClass.setClazz("File");
            fileClass.setLocation(reference_db);
            this.reference_genome.add(fileClass);
    }

    public ArrayList<FileClass> getReference_genome() {
        return reference_genome;
    }

    public void addIRODS(String value) {
        irods.put(irods.size() + "_irods", value);
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getDestination() {
        return destination;
    }

    public int getSetGenomeSAindexeNbases() {
        return setGenomeSAindexeNbases;
    }

    public void setSetGenomeSAindexeNbases(int setGenomeSAindexeNbases) {
        this.setGenomeSAindexeNbases = setGenomeSAindexeNbases;
    }
}
