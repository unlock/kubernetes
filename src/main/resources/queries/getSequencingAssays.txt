SELECT DISTINCT ?assay
WHERE {
    VALUES ?seqAssay { <http://m-unlock.nl/ontology/AmpliconAssay> <http://m-unlock.nl/ontology/AmpliconLibraryAssay> <http://m-unlock.nl/ontology/DNASeqAssay> <http://m-unlock.nl/ontology/RNASeqAssay> }
	?assay a ?seqAssay .
}