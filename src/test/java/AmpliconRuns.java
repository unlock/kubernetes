import nl.munlock.App;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class AmpliconRuns {
    String referenceDB = "/unlock/references/databases/Silva/SILVA_138.1_SSURef_tax_silva.fasta.gz";
    String project = "PRJ%E2BN";
    String investigation = "INV_%";
    String study = "%";
    String observationUnit = "%";
    String assay = "%";
    String workflow = "workflow_ngtax_picrust2.cwl"; // "workflow_ngtax.cwl",
    String limit = "100";
    String[] ngtaxReadLength = {"100"};
    private String reset = "-reset";
    private String wid = "NGTAX_Silva138.1-picrust";
    private String node = "nibi,sanbi";

    // Performing the NGTAX workflow test
    @Test
    public void testNGTAX() throws InterruptedException {
        while (true) {
            try {
                for (String readLength : ngtaxReadLength) {
                    String[] args = {
                            "-cwl", workflow,
                            "-wid", wid,
                            "-project", project,
                            "-investigation", investigation,
                            "-study", study,
                            "-assay", assay,
                            "-length", readLength,
                            "-referenceDB", referenceDB,
                            "-overwrite",
                            // For Entrez datasets primers might not be removed
                            "-primersRemoved",
                            "-memory", "20000",
                            "-threads", "2",
//                            "-debug",
                    };
//                    App.main(args);
                    runKubernetes();
                }
                return;
            } catch (Exception e) {
                e.printStackTrace();
                TimeUnit.MINUTES.sleep(5);
            }
        }
    }

    @Test
    public void runKubernetes() throws Exception {
            String[] args = {
                    "-kubernetes",
                    // Optional argument to preselect workflows
                    "-yaml", wid +"%.yaml",
                    "-project", project,
                    "-investigation", investigation,
                    "-study", study,
                    "-observationUnit", observationUnit,
                    "-limit", limit,
                    "-node", node,
//                    reset
//                "-priority", "1000"
            };
            // Removing empty elements
            List<String> list = new ArrayList<>(Arrays.asList(args));
            list.removeAll(Arrays.asList("", null));
            App.main(list.toArray(new String[0]));
    }

    // Performing the NGTAX demultiplex test
    @Test
    public void testNGTAXDemultiplexing() throws Exception {
        project = "PRJ_MIB-Amplicon";
        investigation = "INV_%";
        String[] args = {
                "-cwl", "workflow_demultiplexing.cwl",
                "-wid", "Demultiplexing_NGTAX",
                "-project", project,
                "-investigation", investigation,
                "-memory", "6000"
        };
        App.main(args);


        project = "ampliconlibraries";

        args = new String[]{
                "-kubernetes",
                "-project", project,
                "-investigation", investigation,
                "-limit", "100",
                "-reset",
                "-delay", "30",
//                "-debug",
                "-disableProv"
        };
        App.main(args);
    }
}
