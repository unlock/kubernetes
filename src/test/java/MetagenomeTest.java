import nl.munlock.App;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MetagenomeTest {


    private String project = "PRJ_UNLOCK";
    private String investigation = "INV_DHB_UNLOCK";
    private String study = "STU_TCB_vs_MCB%";
    private String observationUnit = "%";
    private String limit = "100";
    private String reset = "-reset";
    private String wid = "KB1_MCB_MeOH_2017_assembly"; //""Iricinus_1_Rhelv_assembly.fast"; // "DK2_assembly";
    private String kraken_database = "/unlock/references/databases/Kraken2/K2_PlusPF_20210517";
    private String threads = "32";
    private String memory = "250000";
    private String node = "nibi";
    private String sample = "%";


    // Performing the Metagenomics workflow test
    @Test
    public void testMetaGenomics() throws Exception {
        String[] args = {
                "-cwl", "workflow_metagenomics_assembly.cwl", // "workflow_metagenomics_read_annotation.cwl", // "workflow_metagenomics.cwl",
                "-wid", "metagenomics",
                "-project", project,
                "-investigation", investigation,
                "-study", study,
                "-observationUnit", observationUnit,
                "-memory", memory,
                "-threads", threads,
                "-filter_references", "GCA_000001405.28", // HUMAN
                "-level", "OU",

                "-host","unlock-icat.irods.surfsara.nl",
                "-port","1247",
                "-username","",
                "-zone","unlock",
                "-password","",
                "-sslpolicy","CS_NEG_REQUIRE",
        };
        App.main(args);
        runKubernetes();
    }

    @Test
    public void runKubernetes() throws Exception {
        String[] args = {
                "-kubernetes",
                "-project", project,
                "-investigation", investigation,
                "-study", study,
                "-observationUnit", observationUnit,
                "-limit", limit,
                reset
//                "-priority", "1000"
        };
        // Removing empty elements
        List<String> list = new ArrayList<>(Arrays.asList(args));
        list.removeAll(Arrays.asList("", null));
        App.main(list.toArray(new String[0]));
    }
}
