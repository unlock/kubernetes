import nl.munlock.App;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class QualityRuns {

    String project = "PRJ_IBISBA";
    String investigation = "INV_%"; // INV_CAMI"; // INV_NeoM_RIVM"; // INV_Ticks_RIVM";
    String study = "STU_%";
    String observationUnit = "%"; // "fast5_basecall" "OBS_Zymo-LOG-BB-SN"; // "OBS_fast5"; // "OBS_Zymo-EVEN-BB-SN"; //"OBS_fast5_tiny";
    // String assay = "%";
    String limit = "1000";
    private String reset = "-reset";
    private String wid = "QUALITY_KRAKEN";
//    private String kraken_database = "/unlock/references/databases/Kraken2/K2_PlusPF_20210517";
    private String kraken_database = "/unlock/projects/PRJ_IBISBA/INV_COCULTURE/references/KRAKEN_COCULTURE";

    private String threads = "20";
    private String memory = "250000";
//    private String node = "ichibi";
    private String sample = "%";
    private String rrna = "-rrna";
    private String filter_references = "-filter_references";

    @Test
    public void testIlluminaQuality() throws Exception {
        String[] args = {
                "-cwl", "workflow_illumina_quality.cwl",
                "-wid", wid,
                "-project", project,
                "-investigation", investigation,
                "-study", study,
                "-observationUnit", observationUnit,
                "-kraken_database", kraken_database,
                "-memory", memory,
                "-threads", threads,
                "-filter_references", "GCA_000001405.28", // HUMAN
                "-level", "OU",
                "-host","unlock-icat.irods.surfsara.nl",
                "-port","1247",
                // rRNA filtering
                rrna,
                filter_references,
        };
        App.main(args);
        runKubernetes();
    }

    @Test
    public void runKubernetes() throws Exception {
        String[] args = {
                "-kubernetes",
                "-project", project,
                "-investigation", investigation,
                "-study", study,
                "-observationUnit", observationUnit,
                "-limit", limit,
                reset
//                "-priority", "1000"
        };
        // Removing empty elements
        List<String> list = new ArrayList<>(Arrays.asList(args));
        list.removeAll(Arrays.asList("", null));
        App.main(list.toArray(new String[0]));
    }
}
