import com.google.gson.JsonSyntaxException;
import io.kubernetes.client.openapi.ApiClient;
import io.kubernetes.client.openapi.ApiException;
import io.kubernetes.client.openapi.apis.BatchV1Api;
import io.kubernetes.client.openapi.apis.CoreV1Api;
import io.kubernetes.client.openapi.models.V1Job;
import io.kubernetes.client.openapi.models.V1Pod;
import io.kubernetes.client.util.Config;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

public class KubernetesTests {

    @Test
    public void kubernetesDebugger() throws IOException, ApiException {
        // Requires the kube config file
        ApiClient client = Config.defaultClient();
        BatchV1Api batchV1Api = new BatchV1Api(client);

        List<V1Job> v1Jobs = batchV1Api.listNamespacedJob("unlock", null, null, null, null, null, null, null, null, null, null).getItems();
        List<V1Job> jobs = batchV1Api.listJobForAllNamespaces(null, null, null, null, null, null, null, null, null, null).getItems();

        System.err.println("Jobs: " + jobs.size());

        CoreV1Api api = new CoreV1Api(client);
        List<V1Pod> v1Pods = api.listPodForAllNamespaces(null, null, null, null, null, null, null, null, null, null).getItems();
        for (V1Pod v1Pod : v1Pods) {
            if (v1Pod.getStatus().getPhase().matches("Failed")) {
                try {
                    V1Pod v1Status = api.deleteNamespacedPod(v1Pod.getMetadata().getName(), v1Pod.getMetadata().getNamespace(), null, null, null, null, null, null);
                    System.err.println(v1Status);
                } catch (com.google.gson.JsonSyntaxException | ApiException e) {
                    if (e.getMessage().contains("Expected a string but was BEGIN_OBJECT")) {
                        // Exception thrown when item is being deleted, api is waiting for new implementation...
                    } else if (e.getMessage().contains("Not Found")) {
                        // Job was already on the way to deletion and we went to fast?
                    } else {
                        throw new JsonSyntaxException(e.getMessage());
                    }
                }
            }
        }
    }
}
