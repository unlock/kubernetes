import nl.munlock.App;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MultiQCRuns {

    String project = "PRJ_IBISBA";
    String investigation = "INV_COCULTURE"; // INV_CAMI"; // INV_NeoM_RIVM"; // INV_Ticks_RIVM";
    String study = "STU_COCULTURE";
    String observationUnit = "%"; // "fast5_basecall" "OBS_Zymo-LOG-BB-SN"; // "OBS_fast5"; // "OBS_Zymo-EVEN-BB-SN"; //"OBS_fast5_tiny";
    // String assay = "%";
    String limit = "1000";
    private String reset = "-reset";
    private String wid = "QUALITY_KRAKEN";
    private String threads = "20";
    private String memory = "250000";
//    private String node = "ichibi";
    private String sample = "%";
    private String node = "ichibi";

    @Test
    public void testMultiQC() throws Exception {
        String[] args = {
                "-cwl", "workflow_multiqc.cwl",
                "-wid", wid,
                "-project", project,
                "-investigation", investigation,
                "-study", study,
                "-observationUnit", observationUnit,
                "-memory", memory,
                "-threads", threads,
                "-level", "Study",
                "-host","unlock-icat.irods.surfsara.nl",
                "-port","1247",
                "-username","",
                "-zone","unlock",
                "-password","",
                "-sslpolicy","CS_NEG_REQUIRE",
        };
        App.main(args);
        runKubernetes();
    }

    @Test
    public void runKubernetes() throws Exception {
        String[] args = {
                "-kubernetes",
                "-project", project,
                "-investigation", investigation,
                "-study", study,
                "-observationUnit", observationUnit,
                "-limit", limit,
                "-node", node,
                reset
//                "-priority", "1000"
        };
        // Removing empty elements
        List<String> list = new ArrayList<>(Arrays.asList(args));
        list.removeAll(Arrays.asList("", null));
        App.main(list.toArray(new String[0]));
    }
}
