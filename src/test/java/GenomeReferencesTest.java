import nl.munlock.App;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;


public class GenomeReferencesTest {

    @Test
    public void runSAPP() {
        String[] args = {
                "-cwl", "workflow_sapp_microbes.cwl",
                "-memory", "10000",
                "-threads", "5",
                "-workflowid", "sapp",
                "-debug",
                "-node", "nibi,sanbi",
                "-limit", "50",
        };
        while (true) {
            try {
                App.main(args);
                break;
            } catch (Exception e) {
                System.err.println(e);
                System.err.println("Failure.. try again");
            }
        }
    }

    @Test
    public void runKubernetes() {
        String project = "references";
        String limit = "50";
        String[] args = {
                "-kubernetes",
                "-project", project,
                "-limit", limit,
                "-timeout", "600",
                "-delay", "2",
                "-sleep", "10",
//                "-reset",
                "-node", "nibi"
                // "-debug"
        };

        // Removing empty elements
        List<String> list = new ArrayList<>(Arrays.asList(args));
        list.removeAll(Arrays.asList("", null));
        boolean status = true;
        while (status) {
            try {
                App.main(list.toArray(new String[0]));
                // status = false;
                System.err.println("...SLEEP...");
                TimeUnit.MINUTES.sleep(1);
            } catch (Exception e) {
                for (StackTraceElement stackTraceElement : e.getStackTrace()) {
                    System.err.println(stackTraceElement.getLineNumber() + " " + stackTraceElement.getClassName());
                }
                System.err.println(e.getMessage());
            }
        }
    }
}
