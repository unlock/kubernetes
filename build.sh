#!/bin/bash
#============================================================================
#title          :IRODS CWL
#description    :IRODS manager installation script
#author         :Jasper Koehorst
#date           :2019
#version        :0.0.1
#============================================================================

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

git -C $DIR pull

# ////////////////////////////////////////////////////////////////////////////////////
# Jargon dependency
# ////////////////////////////////////////////////////////////////////////////////////

wget -nc https://github.com/DICE-UNC/jargon/releases/download/4.3.0.2-RELEASE/jargon-core-4.3.0.2-RELEASE-jar-with-dependencies.jar -O $DIR/jargon-core-4.3.0.2-RELEASE-jar-with-dependencies.jar
mvn install:install-file -Dfile=$DIR/jargon-core-4.3.0.2-RELEASE-jar-with-dependencies.jar -DgroupId=jargon -DartifactId=core -Dversion=4.3.0.2 -Dpackaging=jar

# ////////////////////////////////////////////////////////////////////////////////////
# // UNLOCK API
# ////////////////////////////////////////////////////////////////////////////////////
# wget -nc http://download.systemsbiology.nl/unlock/UnlockOntology.jar -O $DIR/UnlockOntology.jar
# mvn install:install-file -Dfile=$DIR/UnlockOntology.jar -DgroupId=nl.munlock -DartifactId=unlockapi -Dversion=1.0.1 -Dpackaging=jar

# Building mode
if [ "$1" == "test" ]; then
	gradle build -b "$DIR/build.gradle" --info
else
	echo "Skipping tests, run './install.sh test' to perform tests"
	gradle build -b "$DIR/build.gradle" -x test
fi

cp $DIR/build/libs/*jar $DIR/

# java -jar $DIR/IRODSTransfer.jar --help
